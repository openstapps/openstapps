# @openstapps/node-builder

## 3.2.0

### Patch Changes

- 689ac68b: pin alpine version to 3.18 and add healthchecks

## 3.0.0

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6
