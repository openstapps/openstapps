# @openstapps/app-builder-image

## 3.1.0

### Minor Changes

- 066e3744: Update to Capacitor 5.x

## 3.0.0

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6
