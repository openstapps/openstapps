const path = require("path");
// const merge = require("deepmerge");

const additionalDeps = {
  '@openstapps/eslint-config': require('./configuration/eslint-config/package.json'),
  '@openstapps/prettier-config': require('./configuration/prettier-config/package.json'),
}

function readPackage(pkg, context) {
  for (const dep in additionalDeps) {
    if (dep in pkg.devDependencies) {
      Object.assign(pkg.devDependencies, additionalDeps[dep].peerDependencies)
    }
  }

  return pkg
}

module.exports = {
  hooks: {
    readPackage,
  }
}
