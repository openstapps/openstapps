#!/usr/bin/env node
import {promisify} from 'node:util';
import {glob} from 'glob';
import {exec} from 'node:child_process';
import {mergeFiles} from 'junit-report-merger';

const files = await glob('./*/*/coverage/cobertura-coverage.xml');
const args = files.map(it => `${it.split('/', 2)[1]}='${it}'`);

console.log(
  'Collecting coverage...',
  await promisify(exec)(`cobertura-merge -o ./coverage.xml ${args.join(' ')}`),
);

const reportFiles = await glob('./*/*/coverage/report-junit.xml');
console.log('Collecting reports...', reportFiles);
await mergeFiles('./report-junit.xml', reportFiles);
