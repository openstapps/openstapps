#!/usr/bin/env sh

# script returns string with everything after the last colon of $1 input
echo -n $1 | grep -oE '[^:]+$'
