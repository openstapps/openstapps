#!/usr/bin/env sh

if pnpm dlx turbo-ignore "$@"
then
  pnpm config --location=project set recursive-install false
fi
