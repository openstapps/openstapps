{
  description = "A Nix-flake-based development environment for OpenStApps";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
    }:
    let
      aapt2buildToolsVersion = "33.0.2";
    in
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            (final: prev: rec {
              fontMin = prev.python311.withPackages (
                ps:
                with ps;
                [
                  brotli
                  fonttools
                ]
                ++ (with fonttools.optional-dependencies; [ woff ])
              );
              android = prev.androidenv.composeAndroidPackages {
                buildToolsVersions = [
                  "34.0.0"
                  aapt2buildToolsVersion
                ];
                platformVersions = [ "34" ];
              };
              cypress = prev.cypress.overrideAttrs (cyPrev: rec {
                version = "13.2.0";
                src = prev.fetchzip {
                  url = "https://cdn.cypress.io/desktop/${version}/linux-x64/cypress.zip";
                  hash = "sha256-9o0nprGcJhudS1LNm+T7Vf0Dwd1RBauYKI+w1FBQ3ZM=";
                };
              });
              nodejs = prev.nodejs_18;
              corepack = prev.corepack_18;
            })
          ];
          config = {
            allowUnfree = true;
            android_sdk.accept_license = true;
          };
        };
        androidFhs = pkgs.buildFHSUserEnv {
          name = "android-env";
          targetPkgs = pkgs: with pkgs; [ ];
          runScript = "bash";
          profile = ''
            export ALLOW_NINJA_ENV=true
            export USE_CCACHE=1
            export LD_LIBRARY_PATH=/usr/lib:/usr/lib32
          '';
        };
      in
      {
        devShell = pkgs.mkShell rec {
          nativeBuildInputs = [ androidFhs ];
          buildInputs = with pkgs; [
            nodejs
            corepack
            # tools
            curl
            jq
            fontMin
            cypress
            # android
            jdk17
            android.androidsdk
          ];
          ANDROID_JAVA_HOME = "${pkgs.jdk.home}";
          ANDROID_SDK_ROOT = "${pkgs.android.androidsdk}/libexec/android-sdk";
          GRADLE_OPTS = "-Dorg.gradle.project.android.aapt2FromMavenOverride=${ANDROID_SDK_ROOT}/build-tools/${aapt2buildToolsVersion}/aapt2";
          CYPRESS_INSTALL_BINARY = "0";
          CYPRESS_RUN_BINARY = "${pkgs.cypress}/bin/Cypress";
        };
      }
    );
}
