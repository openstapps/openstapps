# Open StApps Monorepo

Refer to the [contribution guide](./CONTRIBUTING.md)

## Projects

### Apps

| Name     | Version                                                                                                                           | License                                                                                                                              | Readme                                             |
| -------- | --------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------- |
| Core     | [![npm](https://img.shields.io/npm/v/@openstapps/app.svg?style=flat-square)](https://npmjs.com/package/@openstapps/app)           | [![license)](https://img.shields.io/npm/l/@openstapps/app.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)      | [`frontend/app`](./frontend/app/README.md)         |
| Backend  | [![npm](https://img.shields.io/npm/v/@openstapps/backend.svg?style=flat-square)](https://npmjs.com/package/@openstapps/backend)   | [![license)](https://img.shields.io/npm/l/@openstapps/backend.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)  | [`backend/backend`](./backend/backend/README.md)   |
| Proxy    | [![npm](https://img.shields.io/npm/v/@openstapps/proxy.svg?style=flat-square)](https://npmjs.com/package/@openstapps/proxy)       | [![license)](https://img.shields.io/npm/l/@openstapps/proxy.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)    | [`backend/proxy`](./backend/proxy/README.md)       |
| Database | [![npm](https://img.shields.io/npm/v/@openstapps/database.svg?style=flat-square)](https://npmjs.com/package/@openstapps/database) | [![license)](https://img.shields.io/npm/l/@openstapps/database.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html) | [`backend/database`](./backend/database/README.md) |

### Libraries

| Name       | Version                                                                                                                               | License                                                                                                                                | Readme                                                   |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------- |
| Core       | [![npm](https://img.shields.io/npm/v/@openstapps/core.svg?style=flat-square)](https://npmjs.com/package/@openstapps/core)             | [![license)](https://img.shields.io/npm/l/@openstapps/core.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)       | [`packages/core`](./packages/core/README.md)             |
| API        | [![npm](https://img.shields.io/npm/v/@openstapps/api.svg?style=flat-square)](https://npmjs.com/package/@openstapps/api)               | [![license)](https://img.shields.io/npm/l/@openstapps/api.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)        | [`packages/api`](./packages/api/README.md)               |
| API Plugin | [![npm](https://img.shields.io/npm/v/@openstapps/api-plugin.svg?style=flat-square)](https://npmjs.com/package/@openstapps/api-plugin) | [![license)](https://img.shields.io/npm/l/@openstapps/api-plugin.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html) | [`packages/api-plugin`](./packages/api-plugin/README.md) |
| Logger     | [![npm](https://img.shields.io/npm/v/@openstapps/logger.svg?style=flat-square)](https://npmjs.com/package/@openstapps/logger)         | [![license)](https://img.shields.io/npm/l/@openstapps/logger.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)     | [`packages/logger`](./packages/logger/README.md)         |
| Core Tools | [![npm](https://img.shields.io/npm/v/@openstapps/core-tools.svg?style=flat-square)](https://npmjs.com/package/@openstapps/core-tools) | [![license)](https://img.shields.io/npm/l/@openstapps/core-tools.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html) | [`packages/core-tools`](./packages/core-tools/README.md) |

## Publishing

After having added all changes using `pnpm changeset`

```shell
pnpm publish-packages
```

Internally, this will run

```shell
pnpm changeset version # bump versions
pnpm syncpack:fix
pnpm format:fix # changes to the package.json can cause issues
pnpm install # update lockfile and rebuild packages
git add .
git commit -m "refactor: update changelog"
pnpm changeset tags
git push --follow-tags
# from here the pipeline will do the publishing
```

## Remote caching

Turbo supports remote caching, which massively speeds up build processes.

### Connecting to the remote cache locally

`.env.local`

```dotenv
TURBO_API=http://example:3000
TURBO_TEAM=openstapps
TURBO_TOKEN=abc123
```

### Connecting to the remote cache in GitLab Pipelines

You will need to define

- `TURBO_API`
- `TURBO_TEAM`
- `TURBO_TOKEN`
  Like you did locally as described in [this](https://turbo.build/repo/docs/ci/gitlabci#remote-caching)
  guide.

### Hosting a cache

Self-hosting via Docker is extremely simple, just follow
[this](http://v2202207178592194230.supersrv.de:6341) guide
or in short:

`.env`

```dotenv
PORT=...
TURBO_TOKEN=...
```

```shell
docker run --env-file=.env -p 3000:3000 fox1t/turborepo-remote-cache
```

## Useful commands

#### Why is this package bloating the app?

```shell
pnpm why --prod -r --filter @openstapps/app PACKAGE
```

#### Licenses

```shell
pnpm licenses --prod --filter PROJECT
```
