/** @type {import('@openstapps/core').SCAppConfigurationMenuCategory[]} */
const menus = [
  {
    icon: 'home',
    items: [
      {
        icon: 'newspaper',
        route: '/news',
        title: 'news',
        translations: {
          de: {
            title: 'Aktuelles',
          },
          en: {
            title: 'news',
          },
        },
      },
      {
        icon: 'search',
        route: '/search',
        title: 'search',
        translations: {
          de: {
            title: 'Suche',
          },
          en: {
            title: 'search',
          },
        },
      },
      {
        icon: 'local_library',
        route: '/hebis-search',
        title: 'library catalog',
        translations: {
          de: {
            title: 'Bibliothekskatalog',
          },
          en: {
            title: 'library catalog',
          },
        },
      },
      {
        icon: 'inventory_2',
        route: '/catalog',
        title: 'course catalog',
        translations: {
          de: {
            title: 'Vorlesungsverzeichnis',
          },
          en: {
            title: 'course catalog',
          },
        },
      },
      {
        icon: 'work',
        route: '/jobs',
        title: 'job postings',
        translations: {
          de: {
            title: 'Jobangebote',
          },
          en: {
            title: 'job postings',
          },
        },
      },
    ],
    title: 'overview',
    route: '/overview',
    translations: {
      de: {
        title: 'Übersicht',
      },
      en: {
        title: 'overview',
      },
    },
  },
  {
    icon: 'local_cafe',
    items: [],
    route: '/canteen',
    title: 'canteen',
    translations: {
      de: {
        title: 'Mensa',
      },
      en: {
        title: 'canteen',
      },
    },
  },
  {
    icon: 'map',
    items: [],
    route: '/map',
    title: 'campus map',
    translations: {
      de: {
        title: 'Campus Karte',
      },
      en: {
        title: 'campus map',
      },
    },
  },
  {
    icon: 'school',
    items: [
      {
        icon: 'grade',
        route: '/favorites',
        title: 'favorites',
        translations: {
          de: {
            title: 'Favoriten',
          },
          en: {
            title: 'favorites',
          },
        },
      },
      {
        icon: 'calendar_today',
        route: '/schedule',
        title: 'schedule',
        translations: {
          de: {
            title: 'Stundenplan',
          },
          en: {
            title: 'schedule',
          },
        },
      },
      {
        authProvider: 'paia',
        icon: 'badge',
        route: '/library-account',
        title: 'library account',
        translations: {
          de: {
            title: 'Bibliothekskonto',
          },
          en: {
            title: 'library account',
          },
        },
      },
      {
        icon: 'settings',
        route: '/settings',
        title: 'settings',
        translations: {
          de: {
            title: 'Einstellungen',
          },
          en: {
            title: 'settings',
          },
        },
      },
      {
        icon: 'rate_review',
        route: '/feedback',
        title: 'feedback',
        translations: {
          de: {
            title: 'Feedback',
          },
          en: {
            title: 'feedback',
          },
        },
      },
      {
        icon: 'info',
        route: '/about',
        title: 'about',
        translations: {
          de: {
            title: 'Über die App',
          },
          en: {
            title: 'About the App',
          },
        },
      },
    ],
    title: 'my app',
    route: '/profile',
    translations: {
      de: {
        title: 'Meine App',
      },
      en: {
        title: 'my app',
      },
    },
  },
];

export default menus;
