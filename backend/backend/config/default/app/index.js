import userGroupSetting from './user-group-setting.js';
import languageSetting from './language-setting.js';
import menus from './menu.js';

/** @type {import('@openstapps/core').SCAppConfiguration} */
const app = {
  aboutPages: {},
  campusPolygon: {
    coordinates: [
      [
        [8.660_432_999_690_723, 50.123_027_017_044_436],
        [8.675_496_285_518_358, 50.123_027_017_044_436],
        [8.675_496_285_518_358, 50.130_661_764_486_42],
        [8.660_432_999_690_723, 50.130_661_764_486_42],
        [8.660_432_999_690_723, 50.123_027_017_044_436],
      ],
    ],
    type: 'Polygon',
  },
  features: {},
  menus,
  name: 'Goethe-Uni',
  privacyPolicyUrl: 'https://mobile.server.uni-frankfurt.de/_static/privacy.md',
  settings: [userGroupSetting, languageSetting],
  versionHistory: [],
};

export default app;
