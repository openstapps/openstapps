import {SCSettingInputType, SCThingOriginType, SCThingType} from '@openstapps/core';

/** @type {import('@openstapps/core').SCLanguageSetting}  */
const languageSetting = {
  categories: ['profile'],
  defaultValue: 'en',
  description: 'The language this app is going to use.',
  inputType: SCSettingInputType.SingleChoice,
  name: 'language',
  order: 0,
  origin: {
    indexed: '2018-09-11T12:30:00Z',
    name: 'SCConfigFile Default Values',
    type: SCThingOriginType.Remote,
  },
  translations: {
    de: {
      description: 'Die Sprache in der die App angezeigt wird.',
      name: 'Sprache',
      values: ['Deutsch', 'English'],
    },
    en: {
      description: 'The language this app is going to use.',
      name: 'Language',
      values: ['Deutsch', 'English'],
    },
  },
  type: SCThingType.Setting,
  uid: 'dc9d6dec-6576-45ef-9e35-3598c0d6a662',
  values: ['de', 'en'],
};

export default languageSetting;
