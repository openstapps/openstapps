import {SCSettingInputType, SCThingOriginType, SCThingType} from '@openstapps/core';

/** @type {import('@openstapps/core').SCUserGroupSetting} */
const userGroupSetting = {
  categories: ['profile'],
  defaultValue: 'students',
  description:
    'The user group the app is going to be used.' +
    'This settings for example is getting used for the predefined price category of mensa meals.',
  inputType: SCSettingInputType.SingleChoice,
  name: 'group',
  order: 1,
  origin: {
    indexed: '2018-09-11T12:30:00Z',
    name: 'SCConfigFile Default Values',
    type: SCThingOriginType.Remote,
  },
  translations: {
    de: {
      description:
        'Mit welcher Benutzergruppe soll die App verwendet werden?' +
        ' Die Einstellung wird beispielsweise für die Vorauswahl der Preiskategorie der Mensa verwendet.',
      name: 'Gruppe',
      values: ['Studierende', 'Angestellte', 'Gäste'],
    },
    en: {
      description:
        'The user group the app is going to be used.' +
        ' This settings for example is getting used for the predefined price category of mensa meals.',
      name: 'Group',
      values: ['students', 'employees', 'guests'],
    },
  },
  type: SCThingType.Setting,
  uid: '2c97aa36-4aa2-43de-bc5d-a2b2cb3a530e',
  values: ['students', 'employees', 'guests'],
};

export default userGroupSetting;
