/**
 * This is the default configuration for elasticsearch (a database)
 *
 * To select your university-specific configuration which is merged from this default file and your university-specific
 * file, you have to supply the `NODE_APP_INSTANCE` environment variable with your license plate
 *
 * To select a different database, you have to supply the `NODE_CONFIG_ENV` environment variable with a database name
 * that is implemented in the backend
 *
 * To get more information about the meaning of specific fields, please use your IDE to read the TSDoc documentation.
 *
 * @type {import('../../src/storage/elasticsearch/types/elasticsearch-config.js').ElasticsearchConfigFile}
 */
const config = {
  internal: {
    database: {
      name: 'elasticsearch',
      version: '8.4.2',
      query: {
        minMatch: '75%',
        queryType: 'dis_max',
        matchBoosting: 1.3,
        fuzziness: 'AUTO',
        cutoffFrequency: 0,
        tieBreaker: 0,
      },
    },
  },
};

export default config;
