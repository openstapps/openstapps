/**
 * Generates a range of numbers that represent consecutive calendar months
 *
 * @param {number} startMonth The month to start with (inclusive)
 * @param {number} endMonth The month to end with (inclusive)
 * @returns {number[]}
 */
export function yearSlice(startMonth, endMonth) {
  let months = [...Array.from({length: 13}).keys()].slice(1);
  months = [...months, ...months];
  if (!months.includes(startMonth) || !months.includes(endMonth)) {
    throw new Error(`Given months not part of a year! Check ${startMonth} or ${endMonth}!`);
  }

  const startIndex = months.indexOf(startMonth);
  const endIndex =
    months.indexOf(endMonth) <= startIndex ? months.lastIndexOf(endMonth) : months.indexOf(endMonth);

  return months.slice(startIndex, endIndex + 1);
}

export const sommerRange = yearSlice(3, 8);
export const winterRange = yearSlice(9, 2);
export const month = new Date().getMonth();
export const year = new Date().getFullYear();
export const winterYearOffset = month < winterRange[0] ? -1 : 0;
export const sommerYear = year + (month <= winterRange[winterRange.length] ? -1 : 0);
export const winterYear = `${year + winterYearOffset}/${(year + 1 + winterYearOffset).toString().slice(-2)}`;

export const wsAcronymShort = `WS ${winterYear}`;
export const ssAcronymShort = `SS ${sommerYear}`;
export const wsAcronymLong = `WiSe ${winterYear}`;
export const ssAcronymLong = `SoSe ${sommerYear}`;
