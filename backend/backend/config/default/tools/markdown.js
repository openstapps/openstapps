import {readFile} from 'fs/promises';
import {SCAboutPageContentType} from '@openstapps/core';

/**
 * Usage:
 *
 * ```js
 * await markdown('./page.md', import.meta.url)
 * ```
 *
 * @param {string} path relative path to the file, omitting the language marker
 * @param {string | URL} base base path, usually import.meta.url
 * @returns {Promise<import('@openstapps/core').SCAboutPageMarkdown>}
 */
export async function markdown(path, base) {
  const de = await readFile(new URL(path.replace(/\.md$/, '.de.md'), base), 'utf8');
  const en = await readFile(new URL(path.replace(/\.md$/, '.en.md'), base), 'utf8');

  return {
    value: de,
    translations: {
      en: {
        value: en,
      },
    },
    type: SCAboutPageContentType.MARKDOWN,
  };
}
