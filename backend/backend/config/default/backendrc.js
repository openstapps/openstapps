import app from './app/index.js';
import {backend, internal} from './backend/index.js';

/**
 * This is the default configuration for app and backend
 *
 * University-specific files can be created with the following naming scheme: default-<university license plate>.ts
 *
 * To select your university-specific configuration which is merged from this default file and your university-specific
 * file, you have to supply the `NODE_APP_INSTANCE` environment variable with your license plate
 *
 * To get more information about the meaning of specific fields, please have a look at `@openstapps/core` or use your
 * IDE to read the TSDoc documentation.
 *
 * @type {import('@openstapps/core').SCConfigFile}
 */
const config = {
  app,
  auth: {},
  backend,
  internal,
  uid: 'f-u',
};

export default config;
