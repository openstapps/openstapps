import {
  month,
  sommerRange,
  ssAcronymLong,
  ssAcronymShort,
  winterRange,
  wsAcronymLong,
  wsAcronymShort,
} from '../tools/semester-acronym.js';
import {SCThingType} from '@openstapps/core';

/** @type {import('@openstapps/core').SCBackendConfigurationSearchBoostingContext} */
const boostings = {
  default: [
    {
      factor: 1,
      fields: {
        'academicTerms.acronym': {
          [ssAcronymShort]: sommerRange.includes(month) ? 1.1 : 1.05,
          [wsAcronymShort]: winterRange.includes(month) ? 1.1 : 1.05,
          [ssAcronymLong]: sommerRange.includes(month) ? 1.1 : 1.05,
          [wsAcronymLong]: winterRange.includes(month) ? 1.1 : 1.05,
        },
      },
      type: SCThingType.AcademicEvent,
    },
    {
      factor: 1,
      fields: {
        categories: {
          'course': 1.08,
          'integrated course': 1.08,
          'introductory class': 1.05,
          'lecture': 1.1,
          'seminar': 1.01,
          'tutorial': 1.05,
        },
      },
      type: SCThingType.AcademicEvent,
    },
    {
      factor: 1.6,
      type: SCThingType.Building,
    },
    {
      factor: 1,
      fields: {
        categories: {
          cafe: 1.1,
          learn: 1.1,
          library: 1.2,
          restaurant: 1.1,
        },
      },
      type: SCThingType.PointOfInterest,
    },
    {
      factor: 1,
      fields: {
        categories: {
          'main dish': 2,
        },
      },
      type: SCThingType.Dish,
    },
  ],
  dining: [
    {
      factor: 1,
      fields: {
        categories: {
          'cafe': 2,
          'canteen': 2,
          'restaurant': 2,
          'restroom': 1.2,
          'student canteen': 2,
        },
      },
      type: SCThingType.Building,
    },
    {
      factor: 2,
      type: SCThingType.Dish,
    },
  ],
  place: [
    {
      factor: 2,
      type: SCThingType.Building,
    },
    {
      factor: 2,
      type: SCThingType.PointOfInterest,
    },
    {
      factor: 2,
      type: SCThingType.Room,
    },
  ],
};

export default boostings;
