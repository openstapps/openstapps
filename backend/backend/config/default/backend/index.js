import {SCThingType} from '@openstapps/core';
import aggregations from './aggregations.js';
import boostings from './boostings.js';
import {readFile} from 'fs/promises';

/** @type {import('@openstapps/core').SCBackendInternalConfiguration} */
export const internal = {
  aggregations,
  boostings,
};

/** @type {import('@openstapps/core').SCBackendConfiguration} */
export const backend = {
  SCVersion: JSON.parse(await readFile('package.json', 'utf8')).version,
  externalRequestTimeout: 5000,
  hiddenTypes: [SCThingType.DateSeries, SCThingType.Diff, SCThingType.Floor],
  mappingIgnoredTags: ['minlength', 'pattern', 'see', 'tjs-format'],
  maxMultiSearchRouteQueries: 5,
  maxRequestBodySize: 2e6,
  name: 'Goethe-Universität Frankfurt am Main',
  namespace: '909a8cbc-8520-456c-b474-ef1525f14209',
  sortableFields: [
    {
      fieldName: 'name',
      sortTypes: ['ducet'],
    },
    {
      fieldName: 'type',
      sortTypes: ['ducet'],
    },
    {
      fieldName: 'categories',
      onlyOnTypes: [
        SCThingType.AcademicEvent,
        SCThingType.Building,
        SCThingType.Catalog,
        SCThingType.Dish,
        SCThingType.PointOfInterest,
        SCThingType.Room,
      ],
      sortTypes: ['ducet'],
    },
    {
      fieldName: 'geo',
      onlyOnTypes: [SCThingType.Building, SCThingType.PointOfInterest, SCThingType.Room],
      sortTypes: ['distance'],
    },
    {
      fieldName: 'geo',
      onlyOnTypes: [SCThingType.Building, SCThingType.PointOfInterest, SCThingType.Room],
      sortTypes: ['distance'],
    },
    {
      fieldName: 'inPlace.geo',
      onlyOnTypes: [
        SCThingType.DateSeries,
        SCThingType.Dish,
        SCThingType.Floor,
        SCThingType.Organization,
        SCThingType.PointOfInterest,
        SCThingType.Room,
        SCThingType.Ticket,
      ],
      sortTypes: ['distance'],
    },
    {
      fieldName: 'offers',
      onlyOnTypes: [SCThingType.Dish],
      sortTypes: ['price'],
    },
  ],
};
