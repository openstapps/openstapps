import {SCThingType} from '@openstapps/core';

/** @type {import('@openstapps/core').SCBackendAggregationConfiguration[]} */
const aggregations = [
  {
    fieldName: 'categories',
    onlyOnTypes: [
      SCThingType.AcademicEvent,
      SCThingType.Article,
      SCThingType.Building,
      SCThingType.Catalog,
      SCThingType.Dish,
      SCThingType.PointOfInterest,
      SCThingType.Room,
    ],
  },
  {
    fieldName: 'inPlace.name',
    onlyOnTypes: [
      SCThingType.DateSeries,
      SCThingType.Dish,
      SCThingType.Floor,
      SCThingType.Organization,
      SCThingType.PointOfInterest,
      SCThingType.Room,
      SCThingType.Ticket,
    ],
  },
  {
    fieldName: 'academicTerms.acronym',
    onlyOnTypes: [SCThingType.AcademicEvent, SCThingType.SportCourse],
  },
  {
    fieldName: 'academicTerm.acronym',
    onlyOnTypes: [SCThingType.Catalog],
  },
  {
    fieldName: 'majors',
    onlyOnTypes: [SCThingType.AcademicEvent],
  },
  {
    fieldName: 'keywords',
    onlyOnTypes: [SCThingType.Article, SCThingType.Book, SCThingType.Message, SCThingType.Video],
  },
  {
    fieldName: 'type',
  },
];

export default aggregations;
