/**
 * This is the database configuration for the technical university of berlin
 *
 * @type {import('@openstapps/logger').RecursivePartial<import('../../src/storage/elasticsearch/types/elasticsearch-config.js').ElasticsearchConfigFile>}
 */
const config = {
  internal: {
    database: {
      name: 'elasticsearch',
      query: {
        minMatch: '60%',
        queryType: 'query_string',
      },
    },
  },
};

export default config;
