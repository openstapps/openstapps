import aboutPages from './about-pages/index.js';
import defaultApp from '../default/app/index.js';
import {backend as defaultBackend, internal as defaultInternal} from '../default/backend/index.js';
import versionHistory from './version-history/index.js';

/**
 * This is the default configuration for the Goethe university of Frankfurt
 *
 * @type {import('@openstapps/core').SCConfigFile}
 */
const config = {
  auth: {
    default: {
      client: {
        clientId: '1cac3f99-33fa-4234-8438-979f07e0cdab',
        scopes: '',
        url: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0',
      },
      endpoints: {
        authorization: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0/authorize',
        endSession: 'https://cas.rz.uni-frankfurt.de/cas/logout',
        mapping: {
          id: '$.id',
          email: '$.attributes.mailPrimaryAddress',
          familyName: '$.attributes.sn',
          givenName: '$.attributes.givenName',
          name: '$.attributes.givenName',
          role: '$.attributes.eduPersonPrimaryAffiliation',
          studentId: '$.attributes.employeeNumber',
        },
        token: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0/accessToken',
        userinfo: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0/profile',
      },
    },
    paia: {
      client: {
        clientId: '',
        scopes: '',
        url: 'https://hds.hebis.de/Shibboleth.sso/UBFFM?target=https://hds.hebis.de/ubffm/paia_login_stub.php',
      },
      endpoints: {
        authorization:
          'https://hds.hebis.de/Shibboleth.sso/UBFFM?target=https://hds.hebis.de/ubffm/paia_login_stub.php',
        endSession: 'https://ubffm.hds.hebis.de/Shibboleth.sso/Logout',
        mapping: {
          id: '$.email',
          name: '$.name',
          role: '$.type',
        },
        token: 'https://hds.hebis.de/paia/auth/login',
        userinfo: 'https://hds.hebis.de/paia/core',
      },
    },
  },
  app: {
    ...defaultApp,
    features: {
      extern: {
        hisometry: {
          authProvider: 'default',
          url: 'https://his-self-service.rz.uni-frankfurt.de',
        },
        daia: {
          url: 'https://daia.hebis.de/DAIA2/UB_Frankfurt',
        },
        hebisProxy: {
          url: 'https://proxy.ub.uni-frankfurt.de/login?qurl=',
        },
        paia: {
          authProvider: 'paia',
          url: 'https://hds.hebis.de/paia/core',
        },
        /** TODO: idCards: {
          authProvider: 'default',
          url: 'TODO',
        } */
      },
    },
    versionHistory,
    aboutPages,
  },
  backend: defaultBackend,
  internal: defaultInternal,
  uid: 'f-u',
};

export default config;
