Open StApps provides students from all participating universities with a
high-quality app for everyday study. Open StApps partners integrate
generalizable student processes into app modules in such a way that they can be
used by other universities. The data of a data source used in the Open StApps app
is prepared in a generalized data model in a way that the data source can be easily
exchanged or switched off while the app continues to function without any problems.
