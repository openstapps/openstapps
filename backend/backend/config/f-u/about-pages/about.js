import {SCAboutPageContentType} from '@openstapps/core';
import {markdown} from '../../default/tools/markdown.js';

/** @type {import('@openstapps/core').SCAboutPage} */
export const about = {
  title: 'Über Open StApps',
  content: [
    {
      title: 'Verbundprojekt mehrerer Hochschulen für eine generische Studierenden-App',
      content: await markdown('./about.md', import.meta.url),
      translations: {
        en: {
          title: 'Collaborative project of multiple universities for a single generic study app',
        },
      },
      type: SCAboutPageContentType.SECTION,
    },
    {
      title: 'Goethe-Uni Kontakt',
      content: {
        rows: [
          [
            {
              value: 'Adresse',
              translations: {
                en: {
                  value: 'Address',
                },
              },
              type: SCAboutPageContentType.MARKDOWN,
            },
            {
              // language=Markdown
              value:
                'Goethe Universität<br>' +
                'Hochschulrechenzentrum (HRZ)<br>' +
                'Norbert-Wollheim-Platz 1<br>' +
                '60629 Frankfurt',
              translations: {},
              type: SCAboutPageContentType.MARKDOWN,
            },
          ],
          [
            {
              value: 'Kontaktinformation',
              translations: {
                en: {
                  value: 'Contact information',
                },
              },
              type: SCAboutPageContentType.MARKDOWN,
            },
            {
              // language=Markdown
              value:
                '[app@rz.uni-frankfurt.de](mailto:app@rz.uni-frankfurt.de)<br>' +
                '[+49 69 798 32936](tel:+496979832936)<br>' +
                '[https://app.rz.uni-frankfurt.de](https://app.rz.uni-frankfurt.de)',
              translations: {},
              type: SCAboutPageContentType.MARKDOWN,
            },
          ],
        ],
        type: SCAboutPageContentType.TABLE,
      },
      translations: {
        en: {
          title: 'Goethe-Uni Contact',
        },
      },
      type: SCAboutPageContentType.SECTION,
    },
    {
      icon: 'newspaper',
      title: 'Neue Funktionen / Gelöste Probleme',
      link: 'changelog',
      translations: {
        en: {
          title: 'New features / Resolved issues',
        },
      },
      type: SCAboutPageContentType.ROUTER_LINK,
    },
    {
      icon: 'description',
      title: 'Impressum',
      link: 'imprint',
      translations: {
        en: {
          title: 'Imprint',
        },
      },
      type: SCAboutPageContentType.ROUTER_LINK,
    },
    {
      icon: 'policy',
      title: 'Datenschutz',
      link: 'privacy',
      translations: {
        en: {
          title: 'Privacy policy',
        },
      },
      type: SCAboutPageContentType.ROUTER_LINK,
    },
    {
      icon: 'copyright',
      title: 'Bibliotheken und Lizenzen',
      link: 'licenses',
      translations: {
        en: {
          title: 'Libraries and licenses',
        },
      },
      type: SCAboutPageContentType.ROUTER_LINK,
    },
  ],
  translations: {
    en: {
      title: 'About Open StApps',
    },
  },
};

export default about;
