import {markdown} from '../../default/tools/markdown.js';

/** @type {import('@openstapps/core').SCAboutPage} */
export const privacy = {
  title: 'Datenschutz',
  content: [await markdown('./privacy.md', import.meta.url)],
  translations: {
    en: {
      title: 'Privacy Policy',
    },
  },
};

export default privacy;
