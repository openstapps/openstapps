# Privacy policy

## Contact details of the person responsible

Responsible in the sense of the General Data Protection Regulation and further regulations on data protection is the:

Johann Wolfgang Goethe-Universität Frankfurt am Main represented by its president<br />
Theodor-W.-Adorno-Platz 1<br />
60323 Frankfurt am Main

Postanschrift:<br />
Goethe-Universität Frankfurt am Main<br />
60629 Frankfurt

Website: http://www.uni-frankfurt.de

## Contact details of the data protection officer at Goethe University

You can reach the data protection officers at Johann Wolfgang Goethe University Frankfurt am Main at:<br />
Mail: <dsb@uni-frankfurt.de><br />
Website: http://www.uni-frankfurt.de/47859992/datenschutzbeauftragte

## Information on the processing of personal data

### <u>1. Scope of the processing of personal data</u>.

According to Article 4 DSGVO, personal data is any information relating to an identified or identifiable natural person.

We process personal data of you as a user inside of the Goethe University App to the extent that this is technically necessary for the provision of a **functional application**.

Furthermore, data processing may be based on your voluntary consent if you wish to use **specific functions**.

We therefore distinguish below between

- Access data when using the app: content of requests, IP addresses, date/time of request, requested URL, error codes, browser identifier, HTTP header.

- Location and navigation: voluntary location information

- User settings: voluntary specification of a) language preferences (currently: German/English), b) status (e.g. guest/student) or c) specific search queries and search results (notifications)

- Calendar function: voluntary use of the calendar function (optional with voluntary use of a sync function: opt-in) or the integrated timetable function. The following data is processed and stored on the users device: appointments and events

- Feedback function and contacting: voluntary use with the provision of contact data and, if applicable, voluntary transmission of log data

- Campus services: voluntary use with processing of grade view, matriculation number, email address, name

- Services of the library: voluntary use with processing of library account data, such as ID number with name, e-mail address, postal address, right of use, order data, fees, reservation, loan data. Full details of processing can be found in the library's privacy policy:<br />
  https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

In some places, the app links to the Goethe University website and to other external websites that are displayed in an in-app browser. When you visit these websites, we ask you to pay attention to the separate data protection notices and declarations that apply there.

### <u>2. Purpose(s) of data processing</u>

**Access to location data**.

For navigation, the Goethe University app requires access to the location of the end device used (location-based services). When a request is made, the app collects the current location via GPS, radio cell data and WLAN databases in order to be able to give you as a user:in information about your immediate surroundings. The location data is only accessed if you allow access to the location data. Data about your location is only used to process location-related requests and to display your location on the map.

**Access to access data**.

Log files are stored and processed to ensure that the Goethe Uni app functions properly for you. In addition, we need the data for reasons of security of our information technology systems. No other evaluation or disclosure takes place in this context.

**Access to language settings**.

Access to the language setting is made in order to display the interface of the app in the language of your choice.

**Access to the status group setting**.

Access to the status group setting is provided to show you the information in the app that applies to your group, e.g. canteen prices.

**Access to personal data when using the feedback function**.

We use the processing of personal data from the feedback function to contact you and troubleshoot problems.

**Access to personal data when synchronizing calendars**.

Appointment data is accessed in order to write it to the device calendar when the calendar function is enabled.

**Access to Campus Services data**.

Access to the Campus Management System is solely for the purpose of displaying personal student management data in the app (e.g., exam grades).

**Access to library-specific personal data**.

Access to data (e.g., ID number, name, mailing address) is for the purpose of carrying out ordering and borrowing procedures for books and other materials from the University Library. Full details of the purposes of processing can be found in the Library's Privacy Policy: https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

### <u>3. Rechtsgrundlage(n) für die Datenverarbeitung</u>

The use of usage/access data ("log files") is based on Article 6(1)(f) DSGVO.

For all specific functions where data processing is based on your voluntary consent as a user:in, explicit consent or active acts of consent ("opt-in") are obtained. The provision of personal data about you to Goethe University is done on a voluntary basis. The legal basis in each of these cases is Article 6 (1) a) DSGVO. You can individually revoke your respective consent or change your settings at any time.

### <u>4. Data deletion and storage duration</u>

The data collected in the log files of the app are automatically deleted or anonymized seven days after the end of the access.

The deletion periods or storage duration of the data collected in the library systems can be found in the library's privacy policy: https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

For all other functions and services, the following applies: deletion takes place here depending on the specifications of the service used. The personal data of the data subject will be deleted or blocked as soon as the purpose of the storage no longer applies.

### <u>5. Data disclosure/data transfer</u>

We will not pass on your personal data to third parties.

On the part of the operator, technical and organizational measures are taken to ensure that third parties do not gain access to the processed data, such as usage data. An order processing relationship according to Art. 28 DSGVO does not exist, as only our own servers are used.

### <u>6. Automated decision-making</u>

Automated decision-making, including profiling, does not take place.

## Rights of the data subject

If personal data is processed by you, you are a data subject within the meaning of the GDPR. The assertion of your data subject rights is free of charge. You can, of course, contact us for this purpose. You are entitled to the following data subject rights vis-à-vis Goethe University:

### <u>1. Right of access</u>

You can request confirmation from us as the controller as to whether and which of your personal data is being processed by us. You have the right to request copies of your personal data from us. Please note the exceptions that may arise due to specific regulations.

### <u>2. Right of rectification</u>

You have the right to request us to rectify and/or complete, if the processed personal data concerning you is not (anymore) accurate or not (anymore) complete.

### <u>3. Right to restriction of processing</u>

Under certain conditions, you can request the restriction of the processing of personal data concerning you, i.e. that your personal data is then not deleted, but marked so that further processing is restricted.

### <u>4. Right to erasure</u>

Under certain conditions, you can demand that we delete the personal data concerning you without delay. This is particularly the case if the personal data is no longer necessary for the purpose for which it was originally collected or processed.

### <u>5. Right to information</u>

If you have asserted the right to rectification, erasure or restriction of processing against us, we are obliged to inform all recipients to whom the personal data concerning you have been disclosed of this rectification or erasure of the data or restriction of processing, unless this proves impossible or involves a disproportionate effort. You are entitled to be informed about these recipients.

### <u>6. Right to data portability</u>

Under certain conditions, you have the right to request that we transfer your personal data directly to another controller or organization. Alternatively, under certain conditions, you have the right to request that we ourselves provide you with the data in a machine-readable format.

### <u>7. Right to object</u>

If we process your personal data because the processing is in the public interest, part of our public duties, or if we process your data on the basis of a legitimate interest, you have the right to object at any time to the processing of data relating to you for reasons arising from your particular situation.

### <u>8. Right to revoke the declaration of consent under data protection law</u>

If we process your personal data because you have given us your consent, you have the right to revoke your declaration of consent at any time.

### <u>9. Right to lodge a complaint with a supervisory authority</u>

You also have the right to lodge a complaint with a supervisory authority. The competent supervisory authority will examine your complaint.

## **Contact details of the supervisory authority in the area of data protection**

If you believe that the processing of your personal data violates data protection regulations, if you have a general inquiry or if you want to complain to a competent supervisory authority, you can contact the Hessian Commissioner for Data Protection and Freedom of Information (HBDI).

**The Hessian Commissioner for Data Protection and Freedom of Information can be reached in different ways:**

<u>**The Hessian Commissioner for Data Protection and Freedom of Information**</u><br />
PO Box 3163<br />
65021 Wiesbaden

Telephone: +49 611 1408 -- 0

For general inquiries you can use a contact form:<br />
<https://datenschutz.hessen.de/kontakt><br />
<br />
A complaint form is also available for complaints:<br />
<https://datenschutz.hessen.de/service/beschwerde-uebermitteln>
