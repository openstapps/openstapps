Open StApps bietet Studierenden aller beteiligten Hochschulen eine qualitativ
hochwertige App für den Studienalltag. Open StApps-Verbundpartner integrieren
generalisierbare Studierendenprozesse so in App-Module, dass diese auch
von anderen Hochschulen verwendet werden können. Die in der Open StApps App
verwendeten Daten einer Datenquelle sind in einem generalisierten Datenmodell
so aufbereitet, dass ein Austausch oder Abschaltung der Datenquelle problemlos möglich
ist und die Open StApps App problemlos weiterhin funktionsfähig bleibt.
