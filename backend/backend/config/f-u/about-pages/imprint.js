import {SCAboutPageContentType} from '@openstapps/core';

/** @type {import('@openstapps/core').SCAboutPage} */
export const imprint = {
  title: 'Impressum',
  content: [
    {
      // language=Markdown
      value: `[Impressum der Johann Wolfgang Goethe-Universität Frankfurt am Main](https://www.uni-frankfurt.de/impressum)`,
      translations: {
        en: {
          // language=Markdown
          value: `[Imprint of the Goethe University Frankfurt](https://www.uni-frankfurt.de/impressum)`,
        },
      },
      type: SCAboutPageContentType.MARKDOWN,
    },
  ],
  translations: {
    en: {
      title: 'Imprint',
    },
  },
};

export default imprint;
