import about from './about.js';
import imprint from './imprint.js';
import privacy from './privacy.js';

/** @type {import('@openstapps/core').SCMap<import('@openstapps/core').SCAboutPage>} */
const aboutPages = {
  'about': about,
  'about/imprint': imprint,
  'about/privacy': privacy,
};

export default aboutPages;
