import {versions} from '../../default/tools/version.js';

/** @type {import('@openstapps/core').SCAppVersionInfo[]} */
const versionHistory = await versions(import.meta.url);

export default versionHistory;
