ES_HOST="elasticsearch"
BACKEND_HOST="localhost"
if [ -z $GITLAB_CI ]; then
    ES_HOST=localhost
fi

( STAPPS_LOG_LEVEL=31 STAPPS_EXIT_LEVEL=8 NODE_CONFIG_ENV=elasticsearch NODE_ENV=integration-test ALLOW_NO_TRANSPORT=true ES_ADDR=http://$ES_HOST:9200 node app.js ) & backend_pid=$!
( openstapps-api e2e http://$BACKEND_HOST:3000 --reportPath coverage/integration-report-junit.xml --waiton tcp:$BACKEND_HOST:3000 --samples node_modules/@openstapps/core/test/resources/indexable ) & api_cli_pid=$!

## Check output codes
# api-cli output defines passing the test
# backend should not exit early

wait $api_cli_pid
api_cli_exit=$?
wait $backend_pid
backend_exit=$?

if [ "$api_cli_exit" -eq "0" ]; then
   echo "FINISHED";
   exit;
fi
