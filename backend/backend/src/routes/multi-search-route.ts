/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCMultiSearchRequest,
  SCMultiSearchResponse,
  SCMultiSearchRoute,
  SCSearchResponse,
  SCTooManyRequestsErrorResponse,
} from '@openstapps/core';
import {isTestEnvironment} from '../common.js';
import {BulkStorage} from '../storage/bulk-storage.js';
import {createRoute} from './route.js';
import {backendConfig} from '../config.js';
/**
 * Contains information for using the multi search route
 */
const multiSearchRouteModel = new SCMultiSearchRoute();

/**
 * Implementation of the multi search route (SCMultiSearchRoute)
 */
export const multiSearchRouter = createRoute<
  SCMultiSearchRequest,
  SCMultiSearchResponse | SCTooManyRequestsErrorResponse
>(multiSearchRouteModel, async (request, app) => {
  const bulkMemory: BulkStorage = app.get('bulk');
  const queryNames = Object.keys(request);

  if (queryNames.length > backendConfig.backend.maxMultiSearchRouteQueries) {
    throw new SCTooManyRequestsErrorResponse(isTestEnvironment);
  }

  // get a map of promises for each query
  const searchRequests = queryNames.map(async queryName => {
    return bulkMemory.database.search(request[queryName]);
  });

  const listOfSearchResponses = await Promise.all(searchRequests);

  const response: {[queryName: string]: SCSearchResponse} = {};
  for (const [index, queryName] of queryNames.entries()) {
    response[queryName] = listOfSearchResponses[index];
  }

  return response;
});
