/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSearchRequest, SCSearchResponse, SCSearchRoute} from '@openstapps/core';
import {BulkStorage} from '../storage/bulk-storage.js';
import {createRoute} from './route.js';

/**
 * Contains information for using the search route
 */
const searchRouteModel = new SCSearchRoute();

/**
 * Implementation of the search route (SCSearchRoute)
 */
export const searchRouter = createRoute<SCSearchRequest, SCSearchResponse>(
  searchRouteModel,
  async (request, app) => {
    const bulkMemory: BulkStorage = app.get('bulk');

    return bulkMemory.database.search(request);
  },
);
