/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCBulkDoneRequest,
  SCBulkDoneResponse,
  SCBulkDoneRoute,
  SCNotFoundErrorResponse,
} from '@openstapps/core';
import {Logger} from '@openstapps/logger';
import {isTestEnvironment} from '../common.js';
import {BulkStorage} from '../storage/bulk-storage.js';
import {createRoute} from './route.js';

/**
 * Contains information for using the route for closing bulks
 */
const bulkDoneRouteModel = new SCBulkDoneRoute();

/**
 * Implementation of the bulk done request route (SCBulkDoneRoute)
 */
export const bulkDoneRouter = createRoute<SCBulkDoneRequest, SCBulkDoneResponse>(
  bulkDoneRouteModel,
  async (_request, app, parameters) => {
    const bulkMemory: BulkStorage = app.get('bulk');
    const bulk = bulkMemory.read(parameters.UID);

    if (bulk === undefined) {
      Logger.warn(`Bulk with ${parameters.UID} not found.`);
      throw new SCNotFoundErrorResponse(isTestEnvironment);
    }

    bulk.state = 'done';
    await bulkMemory.markAsDone(bulk);

    return {};
  },
);
