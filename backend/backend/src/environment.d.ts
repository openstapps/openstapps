declare global {
  namespace NodeJS {
    // eslint-disable-next-line unicorn/prevent-abbreviations
    interface ProcessEnv {
      NODE_APP_INSTANCE: 'default' | 'b-tu' | 'f-u' | 'fb-fh' | 'ks-ug' | string | undefined;
      NODE_CONFIG_ENV: 'elasticsearch' | string | undefined;
      STAPPS_LOG_LEVEL: `${number}`;
      ALLOW_NO_TRANSPORT: `${boolean}`;
      PORT: `${number}`;
      ES_DEBUG: `${boolean}`;
      ES_ADDR: string;
      PROMETHEUS_MIDDLEWARE: `${boolean}`;
    }
  }
}

export {};
