/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {QueryDslDateRangeQuery} from '@elastic/elasticsearch/lib/api/types.js';
import {SCSearchDateRangeFilter} from '@openstapps/core';
import {QueryDslSpecificQueryContainer} from '../../types/util.js';

/**
 * Converts a date range filter to elasticsearch syntax
 * @param filter A search filter for the retrieval of the data
 */
export function buildDateRangeFilter(
  filter: SCSearchDateRangeFilter,
): QueryDslSpecificQueryContainer<'range'> {
  const dateRangeObject: QueryDslDateRangeQuery = {
    format: filter.arguments.format,
    time_zone: filter.arguments.timeZone,
    relation: filter.arguments.relation,
  };
  if (filter.arguments.bounds.lowerBound?.mode === 'exclusive') {
    dateRangeObject.gt = filter.arguments.bounds.lowerBound.limit;
  } else if (filter.arguments.bounds.lowerBound?.mode === 'inclusive') {
    dateRangeObject.gte = filter.arguments.bounds.lowerBound.limit;
  }
  if (filter.arguments.bounds.upperBound?.mode === 'exclusive') {
    dateRangeObject.lt = filter.arguments.bounds.upperBound.limit;
  } else if (filter.arguments.bounds.upperBound?.mode === 'inclusive') {
    dateRangeObject.lte = filter.arguments.bounds.upperBound.limit;
  }

  return {
    range: {
      [filter.arguments.field]: dateRangeObject,
    },
  };
}
