/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSearchAvailabilityFilter} from '@openstapps/core';
import {QueryDslSpecificQueryContainer} from '../../types/util.js';

/**
 * Converts an availability filter to elasticsearch syntax
 * @param filter A search filter for the retrieval of the data
 */
export function buildAvailabilityFilter(
  filter: SCSearchAvailabilityFilter,
): QueryDslSpecificQueryContainer<'range'> {
  const scope = filter.arguments.scope?.charAt(0) ?? 's';
  const time = filter.arguments.time === undefined ? 'now' : `${filter.arguments.time}||`;

  return {
    range: {
      [filter.arguments.field]: {
        gte: `${time}/${scope}`,
        lt: `${time}+1${scope}/${scope}`,
        relation: 'intersects',
      },
    },
  };
}
