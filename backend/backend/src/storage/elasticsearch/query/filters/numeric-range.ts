/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {QueryDslNumberRangeQuery} from '@elastic/elasticsearch/lib/api/types.js';
import {SCSearchNumericRangeFilter} from '@openstapps/core';
import {QueryDslSpecificQueryContainer} from '../../types/util.js';

/**
 * Converts a numeric range filter to elasticsearch syntax
 * @param filter A search filter for the retrieval of the data
 */
export function buildNumericRangeFilter(
  filter: SCSearchNumericRangeFilter,
): QueryDslSpecificQueryContainer<'range'> {
  const numericRangeObject: QueryDslNumberRangeQuery = {
    relation: filter.arguments.relation,
  };
  if (filter.arguments.bounds.lowerBound?.mode === 'exclusive') {
    numericRangeObject.gt = filter.arguments.bounds.lowerBound.limit;
  } else if (filter.arguments.bounds.lowerBound?.mode === 'inclusive') {
    numericRangeObject.gte = filter.arguments.bounds.lowerBound.limit;
  }
  if (filter.arguments.bounds.upperBound?.mode === 'exclusive') {
    numericRangeObject.lt = filter.arguments.bounds.upperBound.limit;
  } else if (filter.arguments.bounds.upperBound?.mode === 'inclusive') {
    numericRangeObject.lte = filter.arguments.bounds.upperBound.limit;
  }

  return {
    range: {
      [filter.arguments.field]: numericRangeObject,
    },
  };
}
