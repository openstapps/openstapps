import {QueryDslQueryContainer} from '@elastic/elasticsearch/lib/api/types.js';

export const query: QueryDslQueryContainer = {
  function_score: {
    functions: [
      {
        filter: {
          term: {
            type: 'academic event',
          },
        },
        weight: 1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'academicTerms.acronym.raw': 'SS 2023',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.05,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'academicTerms.acronym.raw': 'WS 2023/24',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'academicTerms.acronym.raw': 'SoSe 2023',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.05,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'academicTerms.acronym.raw': 'WiSe 2023/24',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.1,
      },
      {
        filter: {
          term: {
            type: 'academic event',
          },
        },
        weight: 1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'categories.raw': 'course',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.08,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'categories.raw': 'integrated course',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.08,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'categories.raw': 'introductory class',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.05,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'categories.raw': 'lecture',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'categories.raw': 'seminar',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.01,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'academic event',
                },
              },
              {
                term: {
                  'categories.raw': 'tutorial',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.05,
      },
      {
        filter: {
          term: {
            type: 'building',
          },
        },
        weight: 1.6,
      },
      {
        filter: {
          term: {
            type: 'point of interest',
          },
        },
        weight: 1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'point of interest',
                },
              },
              {
                term: {
                  'categories.raw': 'cafe',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'point of interest',
                },
              },
              {
                term: {
                  'categories.raw': 'learn',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'point of interest',
                },
              },
              {
                term: {
                  'categories.raw': 'library',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.2,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'point of interest',
                },
              },
              {
                term: {
                  'categories.raw': 'restaurant',
                },
              },
            ],
            should: [],
          },
        },
        weight: 1.1,
      },
      {
        filter: {
          term: {
            type: 'dish',
          },
        },
        weight: 1,
      },
      {
        filter: {
          bool: {
            must: [
              {
                term: {
                  type: 'dish',
                },
              },
              {
                term: {
                  'categories.raw': 'main dish',
                },
              },
            ],
            should: [],
          },
        },
        weight: 2,
      },
    ],
    query: {
      bool: {
        minimum_should_match: 0,
        must: [
          {
            dis_max: {
              boost: 1.2,
              queries: [
                {
                  match: {
                    name: {
                      boost: 1.3,
                      fuzziness: 'AUTO',
                      query: 'mathematics',
                    },
                  },
                },
                {
                  query_string: {
                    default_field: 'name',
                    minimum_should_match: '75%',
                    query: 'mathematics',
                  },
                },
              ],
              tie_breaker: 0,
            },
          },
          {
            term: {
              'type.raw': 'academic event',
            },
          },
        ],
        should: [],
      },
    },
    score_mode: 'multiply',
  },
};
