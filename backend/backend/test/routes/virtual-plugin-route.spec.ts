/* eslint-disable unicorn/consistent-function-scoping,@typescript-eslint/no-explicit-any */
/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {SCInternalServerErrorResponse, SCPluginMetaData, SCValidationErrorResponse} from '@openstapps/core';
import {expect, use} from 'chai';
import chaiAsPromised from 'chai-as-promised';
import {Request} from 'express';
import got, {Options} from 'got';
import nock from 'nock';
import sinon from 'sinon';
import {mockReq} from 'sinon-express-mock';
import {plugins, validator} from '../../src/common.js';
import {virtualPluginRoute} from '../../src/routes/virtual-plugin-route.js';
import {DEFAULT_TEST_TIMEOUT, FooError} from '../common.js';
import {registerAddRequest} from './plugin-register-route.spec.js';
import {testApp} from '../tests-setup.js';

use(chaiAsPromised);

const plugin = registerAddRequest.plugin;

describe('Virtual plugin routes', async function () {
  describe('Middleware', async function () {
    const sandbox = sinon.createSandbox();
    /**
     * Internal method which provides information about the specific error inside of an internal server error
     *
     * @param request Express request
     * @param plugin Plugin information (metadata)
     * @param specificError Class of a specific error
     */
    async function testRejection(request: Request, plugin: SCPluginMetaData, specificError: object) {
      // eslint-disable-next-line unicorn/error-message
      let thrownError: Error = new Error();
      try {
        await virtualPluginRoute(request, plugin);
      } catch (error) {
        thrownError = error;
      }
      // return virtualPluginRoute(req, plugin).should.be.rejectedWith(SCInternalServerErrorResponse); was not working for some reason
      expect(thrownError).to.be.instanceOf(SCInternalServerErrorResponse);
      expect((thrownError as SCInternalServerErrorResponse).additionalData).to.be.instanceOf(specificError);
    }

    afterEach(function () {
      // clean up request mocks (fixes issue with receiving response from mock from previous test case)
      nock.cleanAll();
      // restore everything to default methods (remove spies and stubs)
      sandbox.restore();
    });

    it('should forward body of the request to address and route of the plugin', async function () {
      const request = {
        body: {
          query: 'bar',
        },
      };
      // spy the post method of got
      // @ts-expect-error not assignable
      const gotStub = sandbox.stub(got, 'post').returns({body: {}});
      // @ts-expect-error not assignable
      sandbox.stub(validator, 'validate').returns({errors: []});
      const request_ = mockReq(request);

      await virtualPluginRoute(request_, plugin);

      expect(gotStub.args[0][0]).to.equal(plugin.route.slice(1));
      expect(((gotStub.args[0] as any)[1] as Options).prefixUrl).to.equal(plugin.address);
      expect(((gotStub.args[0] as any)[1] as Options).json).to.equal(request_.body);
    });

    it('should provide data from the plugin when its route is called', async function () {
      const request = {
        body: {
          query: 'bar',
        },
      };
      const response = {
        result: [{foo: 'bar'}, {bar: 'foo'}],
      };
      // mock response of the plugin's address
      nock('http://foo.com:1234').post('/foo').reply(200, response);
      const request_ = mockReq(request);

      expect(await virtualPluginRoute(request_, plugin)).to.eql(response);
    });

    it('should throw the validation error if request is not valid', async function () {
      const request = {
        body: {
          invalid_query_field: 'foo',
        },
      };
      const request_ = mockReq(request);

      await testRejection(request_, plugin, SCValidationErrorResponse);
    });

    it('should throw the validation error if response is not valid', async function () {
      const request = {
        body: {
          query: 'foo',
        },
      };
      // mock response of the plugin service
      nock('http://foo.com:1234')
        .post('/foo')
        .reply(200, {invalid_result: ['foo bar']});
      const request_ = mockReq(request);

      await testRejection(request_, plugin, SCValidationErrorResponse);
    });

    it('should throw error if there is a problem with reaching the address of a plugin', async function () {
      const request = {
        body: {
          query: 'foo',
        },
      };

      // fake that post method of got throws an error
      sandbox.stub(got, 'post').callsFake(() => {
        throw new FooError();
      });
      const request_ = mockReq(request);

      await testRejection(request_, plugin, FooError);
    });
  });

  describe('Routes', async function () {
    // increase timeout for the suite
    this.timeout(DEFAULT_TEST_TIMEOUT);
    const sandbox = sinon.createSandbox();
    // http status code
    const OK = 200;
    const internalServerError = new SCInternalServerErrorResponse();

    afterEach(async function () {
      // remove routes
      plugins.clear();
      // // restore everything to default methods (remove stubs)
      sandbox.restore();
      // cleanup request mocks (fixes issue with receiving response from mock from previous test case)
      nock.cleanAll();
    });

    it('should properly provide the response of a plugin', async function () {
      // let's simulate that the plugin is already registered
      plugins.set(registerAddRequest.plugin.route, registerAddRequest.plugin);
      // mock responses of the plugin, depending on the body sent
      nock('http://foo.com:1234')
        .post('/foo', {query: 'foo'})
        .reply(200, {result: [{foo: 'foo'}, {bar: 'foo'}]});
      nock('http://foo.com:1234')
        .post('/foo', {query: 'bar'})
        .reply(200, {result: [{foo: 'bar'}, {bar: 'bar'}]});

      const fooResponse = await testApp
        .post('/foo')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send({query: 'foo'});
      expect(fooResponse.status).to.be.equal(OK);
      expect(fooResponse.body).to.be.deep.equal({result: [{foo: 'foo'}, {bar: 'foo'}]});

      const barResponse = await testApp
        .post('/foo')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send({query: 'bar'});
      expect(barResponse.status).to.be.equal(OK);
      expect(barResponse.body).to.be.deep.equal({result: [{foo: 'bar'}, {bar: 'bar'}]});
    });

    it('should return error response if plugin address is not responding', async function () {
      // let's simulate that the plugin is already registered
      plugins.set(registerAddRequest.plugin.route, registerAddRequest.plugin);

      class FooError extends Error {}
      // fake that got's post method throws an error
      sandbox.stub(got, 'post').callsFake(() => {
        throw new FooError();
      });

      const {status} = await testApp
        .post('/foo')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send({query: 'foo'});

      expect(status).to.be.equal(internalServerError.statusCode);
    });

    it('should return the validation error response if plugin request is not valid', async function () {
      // lets simulate that the plugin is already registered
      plugins.set(registerAddRequest.plugin.route, registerAddRequest.plugin);

      const {status, body} = await testApp
        .post('/foo')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        // using number for query instead of (in request schema) required text
        .send({query: 123});

      expect(status).to.be.equal(502);
      expect(body.additionalData).to.haveOwnProperty('name', 'ValidationError');
    });

    it('should return the validation error response if plugin response is not valid', async function () {
      // lets simulate that the plugin is already registered
      plugins.set(registerAddRequest.plugin.route, registerAddRequest.plugin);
      // mock response of the plugin address
      nock('http://foo.com:1234')
        .post('/foo')
        .reply(OK, {not_valid_field: ['foo bar']});

      const {status, body} = await testApp
        .post('/foo')
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send({query: 'foo'});

      expect(status).to.be.equal(internalServerError.statusCode);
      expect(body.additionalData).to.haveOwnProperty('name', 'ValidationError');
    });
  });
});
