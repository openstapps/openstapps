/* eslint-disable unicorn/no-null */
import type {ContainerInfo} from 'dockerode';

export const swarmBackendContainerWithExposedPorts: ContainerInfo = {
  Command: 'node ./bin/www',
  Created: 1_524_669_882,
  HostConfig: {
    NetworkMode: 'swarm_default',
  },
  Id: 'e3d3f4d18aceac2780bdb95523845d066ed25c04fc65168a5ddbd37a85671bb7',
  Image: 'registry.gitlab.com/openstapps/backend/b-tu-typescript-refactor-for-new-tslint-config',
  ImageID: 'sha256:ef9f0c8c4b6f99dd208948c7aae1d042590aa18e05ebeae4f586e4b4beebeac9',
  Labels: {
    'com.docker.compose.config-hash': '91c6e0cebad15951824162c93392b6880b69599692f07798ae8de659c1616a03',
    'com.docker.compose.container-number': '1',
    'com.docker.compose.oneoff': 'False',
    'com.docker.stack.namespace': 'deployment',
    'com.docker.swarm.service.name': 'deployment_backend',
    'com.docker.compose.version': '1.21.0',
    'stapps.version': '1.0.0',
  },
  Mounts: [],
  Names: ['/deployment_backend_1'],
  NetworkSettings: {
    Networks: {
      ingress: {
        Aliases: null,
        EndpointID: 'da17549a086ff2c9f622e80de833e6f334afda52c8f07080428640c1716dcd14',
        Gateway: '172.18.0.1',
        GlobalIPv6Address: '',
        GlobalIPv6PrefixLen: 0,
        IPAMConfig: null,
        IPAddress: '172.18.0.3',
        IPPrefixLen: 16,
        IPv6Gateway: '',
        Links: null,
        MacAddress: '03:41:ac:11:00:23',
        NetworkID: '947ea5247cc7429e1fdebd5404fa4d15f7c05e6765f2b93ddb3bdb6aaffd1193',
      },
    },
  },
  Ports: [
    {
      IP: 'delete me',
      PrivatePort: 3000,
      PublicPort: 3000,
      Type: 'tcp',
    },
  ],
  State: 'running',
  Status: 'Up 3 minutes',
};
