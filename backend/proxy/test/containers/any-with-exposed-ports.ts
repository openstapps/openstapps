/* eslint-disable unicorn/no-null */
import type {ContainerInfo} from 'dockerode';

export const anyContainerWithExposedPorts: ContainerInfo = {
  Command: 'sh',
  Created: 1_524_669_882,
  HostConfig: {
    NetworkMode: 'default',
  },
  Id: 'e3d3f4d18aceac2780bdb95523845d066ed25c04fc65168a5ddbd37a85671bb7',
  Image: 'ubuntu:4',
  ImageID: 'sha256:ef9f0c8c4b6f99dd208948c7aae1d042590aa18e05ebeae4f586e4b4beebeac9',
  Labels: {},
  Mounts: [],
  Names: ['/container_name_1'],
  NetworkSettings: {
    Networks: {
      bridge: {
        Aliases: null,
        EndpointID: 'da17549a086ff2c9f622e80de833e6f334afda52c8f07080428640c1716dcd14',
        Gateway: '172.18.0.1',
        GlobalIPv6Address: '',
        GlobalIPv6PrefixLen: 0,
        IPAMConfig: null,
        IPAddress: '172.18.0.3',
        IPPrefixLen: 16,
        IPv6Gateway: '',
        Links: null,
        MacAddress: '03:41:ac:11:00:23',
        NetworkID: '947ea5247cc7429e1fdebd5404fa4d15f7c05e6765f2b93ddb3bdb6aaffd1193',
      },
    },
  },
  Ports: [
    {
      IP: '0.0.0.0',
      PrivatePort: 80,
      PublicPort: 80,
      Type: 'tcp',
    },
  ],
  State: 'running',
  Status: 'Up 3 minutes',
};
