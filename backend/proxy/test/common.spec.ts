/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {mkdirSync, writeFileSync, unlinkSync, rmdirSync} from 'fs';
import path from 'path';
import {isFileType} from '../src/common.js';
import {fileURLToPath} from 'url';

process.on('unhandledRejection', async error => {
  await Logger.error(error);

  process.exit(1);
});

describe('common', function () {
  it('should use ssl certs', function () {
    const testCertDirectory = path.resolve(path.dirname(fileURLToPath(import.meta.url)), 'certs');
    mkdirSync(testCertDirectory);
    const notAnExpectedFileTypeFilePath = path.resolve(testCertDirectory, 'notAnExpectedFileType.txt');
    const anExpectedFileTypeFilePath = path.resolve(testCertDirectory, 'notARealCert.crt');
    writeFileSync(notAnExpectedFileTypeFilePath, 'Test');
    writeFileSync(anExpectedFileTypeFilePath, 'Test');

    expect(isFileType(notAnExpectedFileTypeFilePath, 'crt')).to.equal(false);
    expect(isFileType(anExpectedFileTypeFilePath, 'crt')).to.equal(true);

    unlinkSync(notAnExpectedFileTypeFilePath);
    unlinkSync(anExpectedFileTypeFilePath);
    rmdirSync(testCertDirectory);
  });
});
