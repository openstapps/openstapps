#!/usr/bin/env bash
MAILTO=`cat ../config/RECIPIENTS`
UNIT=$1

EXTRA=""
for e in "${@:2}"; do
  EXTRA+="$e"$'\n'
done

UNITSTATUS=$(systemctl status --lines=50 $UNIT)

ssmtp $MAILTO <<EndOfMessage
To:$MAILTO
Subject:Status mail for unit: $UNIT

Status report for unit: $UNIT
$EXTRA

$UNITSTATUS
EndOfMessage

echo -e "Status mail sent to: $MAILTO for unit: $UNIT"
