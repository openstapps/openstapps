# @openstapps/proxy

## 3.0.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Migrate away from @openstapps/configuration
- 23481d0d: Update to TypeScript 5.1.6
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [23481d0d]
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [64caebaf]
  - @openstapps/logger@3.0.0

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6
- Updated dependencies [23481d0d]
  - @openstapps/logger@3.0.0-next.4

## 3.0.0-next.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Migrate away from @openstapps/configuration
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [64caebaf]
  - @openstapps/logger@3.0.0-next.0

## [1.5.1](https://gitlab.com/openstapps/proxy/compare/v1.5.0...v1.5.1) (2023-01-16)

## [1.5.0](https://gitlab.com/openstapps/proxy/compare/v1.4.1...v1.5.0) (2022-11-29)

### Bug Fixes

- SVGs treated as plain texts ([25e506f](https://gitlab.com/openstapps/proxy/commit/25e506f54d56f49bf5201b76076bc167fafce290)), closes [#14](https://gitlab.com/openstapps/proxy/issues/14)

## [1.4.1](https://gitlab.com/openstapps/proxy/compare/v1.4.0...v1.4.1) (2022-11-09)

### Bug Fixes

- include uri path in json log verbatim ([5f77877](https://gitlab.com/openstapps/proxy/commit/5f77877bb4239a437ff3f2eea1c0dfd51c7d4818))

## [1.4.0](https://gitlab.com/openstapps/proxy/compare/v1.3.0...v1.4.0) (2022-11-09)

### Features

- add support for log aggregators ([8c49c31](https://gitlab.com/openstapps/proxy/commit/8c49c317603b1a2964708ed377a5a7c687f829cd))

## [1.3.0](https://gitlab.com/openstapps/proxy/compare/v1.2.0...v1.3.0) (2022-08-22)

## [1.2.0](https://gitlab.com/openstapps/proxy/compare/v1.1.0...v1.2.0) (2022-06-08)

### Features

- added prometheus metrics support ([5522ac5](https://gitlab.com/openstapps/proxy/commit/5522ac55ac00d4b809d942d0a8c58d15b0432fb8))
- reload nginx on proxyconfig change ([1fcf734](https://gitlab.com/openstapps/proxy/commit/1fcf7340d49bde993b3acc7bdc90e6a637a05321))
- support docker swarm deployments ([4bb46d8](https://gitlab.com/openstapps/proxy/commit/4bb46d8a06ff7829b6908bd03c1cf4240767fcc2))

## [1.1.0](https://gitlab.com/openstapps/proxy/compare/v1.0.1...v1.1.0) (2022-03-10)

## [1.0.1](https://gitlab.com/openstapps/proxy/compare/v1.0.0...v1.0.1) (2022-03-10)

### Bug Fixes

- nginx deleting its own conifg ([edbd739](https://gitlab.com/openstapps/proxy/commit/edbd739db995b72fd3f5b5bec7558442a329f719))

## [1.0.0](https://gitlab.com/openstapps/proxy/compare/v0.4.0...v1.0.0) (2022-03-09)

### Features

- add rate limit allow list ([f10cd6c](https://gitlab.com/openstapps/proxy/commit/f10cd6c4314ee8b5f4bddc54b61231335116376d))

## [0.4.0](https://gitlab.com/openstapps/proxy/compare/fbe1a65cd1cfed81c26af14690c620c4cb4fbdc0...v0.4.0) (2021-09-06)

### Bug Fixes

- deny PUT method requests ([61ad5ab](https://gitlab.com/openstapps/proxy/commit/61ad5abc2bc45b7cb405b59d6bc095ff1e12a5cf))
- fix ci job dependencies and caching ([f59cc58](https://gitlab.com/openstapps/proxy/commit/f59cc5832c0b1cfc4b43889e1cedc360e42850e8))
- handle patch & duplicate versions correctly ([8dffe29](https://gitlab.com/openstapps/proxy/commit/8dffe29146f00b8da98b505b41ab374daf093af2))
- increase nginx transport security ([8fe6a27](https://gitlab.com/openstapps/proxy/commit/8fe6a2795f0c6f9107662ef9e5a43c6a3c8c066c))
- remove reasons of linting errors ([b624ed3](https://gitlab.com/openstapps/proxy/commit/b624ed342657f6a574059734f0399ba8539d6c82))
- remove superfluous visible routes ([96741f3](https://gitlab.com/openstapps/proxy/commit/96741f3789d4a0785124a95475a2ad05b41c9dad)), closes [#10](https://gitlab.com/openstapps/proxy/issues/10)

### Features

- accept PUT method ([59db3b5](https://gitlab.com/openstapps/proxy/commit/59db3b5c50be42ef2d2a4acbad8da2e7d813efd6))
- add issues templates ([9637574](https://gitlab.com/openstapps/proxy/commit/96375740719191850238dca975a153a37fe7d4e6)), closes [#4](https://gitlab.com/openstapps/proxy/issues/4)
- add proxy ([fbe1a65](https://gitlab.com/openstapps/proxy/commit/fbe1a65cd1cfed81c26af14690c620c4cb4fbdc0))
- add systemd service examples ([c54aa25](https://gitlab.com/openstapps/proxy/commit/c54aa25f3460e0d08ac4f66621e639a2b46ccc6d))
