# Database docker image

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/database.svg?style=flat-square)](https://gitlab.com/openstapps/database/commits/main)
[![license)](https://img.shields.io/badge/license-GPL--3.0--only-lightgrey.svg)](https://www.gnu.org/licenses/gpl-3.0.en.html)

The used database is Elasticsearch 8.4.x. This images is based on
[elasticsearch:8.4](https://hub.docker.com/r/library/elasticsearch/) from the
[offical Docker Library project](https://docs.docker.com/docker-hub/official_repos/) and uses openjdk:8-jre-alpine as a
base image.

Additionally the [ICU Analysis Plugin](https://www.elastic.co/guide/en/elasticsearch/plugins/8.4/analysis-icu.html) is
installed to provide unicode collation which is used for sorting.

## Usage for testing purposes:

Run it with this command: `docker run -d -p 9200:9200 registry.gitlab.com/openstapps/database:master`

Currently there is an issue with elasticsearch and java on linux machines that temporarly can be solved by running:
`sudo sysctl -w vm.max_map_count=262144` on the host machine. On restart of the machine this parameter will reset.
To set it permanently, you will have to add it to `/etc/sysctl.conf` on machines that run systemd as a init deamon.

## For production use:

Please set `vm.max_map_count=262144` permanently. It is explained [here](https://www.elastic.co/guide/en/elasticsearch/reference/8.4/docker.html#docker-prod-prerequisites)
