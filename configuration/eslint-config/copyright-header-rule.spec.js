"use strict"

const rule = require('./copyright-header-rule')

const RuleTester = require('eslint').RuleTester
const ruleTester = new RuleTester()

const options = [{
  fixedDate: '2023',
  author: 'Me',
  license: `*\n * Copyright {{year}} {{author}}\n *\n * contents\n `,
}]

ruleTester.run('copyright-header', rule, {
  valid: [
    {
      code: `/**\n * Copyright 2023 Me\n *\n * contents\n */`,
      options,
    },
    {
      code: `// @ts-check\n\n\n/**\n * Copyright 2023 Me\n *\n * contents\n */`,
      options,
    }
  ],
  invalid: [
    {
      code: `// bla\n/**\n * Copyright 2022 Me\n *\n * contents\n */\n\nvar s = "abc"`,
      options,
      errors: [
        {
          messageId: 'invalid',
          suggestions: [
            {
              messageId: 'tryThisHeader',
              output: '// bla\n/**\n * Copyright 2023 Me\n *\n * contents\n */\n\nvar s = "abc"'
            }
          ]
        }
      ],
    },
    {
      code: 'var s = "abc"',
      options,
      errors: [
        {
          messageId: 'missing',
          suggestions: [
            {
              messageId: 'tryThisHeader',
              output: '/**\n * Copyright 2023 Me\n *\n * contents\n */\nvar s = "abc"'
            }
          ]
        }
      ]
    }
  ]
})
