/** @type {import('eslint').Linter.Config} */
const config = {
  root: true,
  ignorePatterns: ['projects/**/*'],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2022,
  },
  overrides: [
    {
      files: ['*.ts'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaVersion: 2022,
        sourceType: 'module',
        project: ['tsconfig.json'],
        createDefaultProgram: true,
      },
      extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:jsdoc/recommended',
        'plugin:unicorn/recommended',
        'prettier',
      ],
      plugins: ['eslint-plugin-unicorn', 'eslint-plugin-jsdoc'],
      settings: {
        jsdoc: {
          mode: 'typescript',
        },
      },
      rules: {
        'no-console': 1,
        'unicorn/filename-case': 'error',
        'unicorn/no-array-callback-reference': 'off',
        'unicorn/no-useless-undefined': 'off',
        'unicorn/prefer-node-protocol': 'off',
        'unicorn/no-process-exit': 'off',
        'unicorn/no-array-reduce': 'off',
        'unicorn/prevent-abbreviations': [
          'error',
          {
            replacements: {
              ref: false,
              i: false,
              e2e: false,
            },
          },
        ],
        'unicorn/no-nested-ternary': 'off',
        'unicorn/better-regex': 'off',
        'unicorn/no-non-null-assertion': 'off',

        'jsdoc/no-types': 'error',
        'jsdoc/require-param': 'off',
        'jsdoc/require-param-description': 'error',
        'jsdoc/check-param-names': 'error',
        'jsdoc/require-returns': 'off',
        'jsdoc/require-param-type': 'off',
        'jsdoc/require-returns-type': 'off',
        'jsdoc/check-tag-names': [
          'error',
          {
            definedTags: [
              'internal',
              'aggregatable',
              'float',
              'indexable',
              'integer',
              'keyword',
              'sortable',
              'text',
              'date',
              'validatable',
              'filterable',
              'inheritTags',
            ],
          },
        ],

        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/no-unused-vars': [
          'error',
          {
            args: 'after-used',
            argsIgnorePattern: '^_',
          },
        ],
        '@typescript-eslint/lines-between-class-members': ['error', 'always'],
        '@typescript-eslint/no-explicit-any': 'error',
        '@typescript-eslint/no-non-null-assertion': 'off',
      },
    },
  ],
};

module.exports = config;
