# @openstapps/tsconfig

## 3.3.0

### Minor Changes

- 688bc5f2: v3.3.0 changes

## 3.0.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import "./lib/app.js";
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

### Patch Changes

- 98546a97: Migrate away from @openstapps/configuration
- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import "./lib/app.js";
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

### Patch Changes

- 98546a97: Migrate away from @openstapps/configuration
