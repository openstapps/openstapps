/*
 * Copyright (C) 2018-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Api, Project} from '@openstapps/gitlab-api';
import {Logger} from '@openstapps/logger';
import {mapAsyncLimit} from '@openstapps/collection-utils';
import {CONCURRENCY} from './configuration.js';

/**
 * Get projects for a list of groups
 * @param api GitLab API to make requests with
 * @param groups List of groups
 */
export async function getProjects(api: Api, groups: number[]): Promise<Project[]> {
  Logger.info(`Fetching all projects for specified groups (${groups.length})...`);

  const projects = await mapAsyncLimit(groups, api.getProjectsForGroup, CONCURRENCY).then(it => it.flat());

  Logger.log(`Fetched ${projects.length} project(s).`);

  return projects;
}
