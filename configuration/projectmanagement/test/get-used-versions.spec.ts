import * as chai from 'chai';
import {expect} from 'chai';
import chaiAsPromised from 'chai-as-promised';
import path from 'path';
import {getUsedVersion, getUsedVersionMajorMinor} from '../src/tasks/get-used-version.js';

chai.use(chaiAsPromised);
chai.should();

describe('Verify Versions', function () {
  it('should not depend on core', async function () {
    await getUsedVersion(process.cwd(), '@openstapps/core').should.eventually.be.rejected;
  });

  it('should not be a Node.js project', async function () {
    await getUsedVersion(path.resolve(process.cwd(), '..'), '@openstapps/core').should.eventually.be.rejected;
  });

  it('should get used version', async function () {
    expect(await getUsedVersion(process.cwd(), 'mustache')).to.be.equal('4.2.0');
  });

  it('should get used version major minor', async function () {
    expect(await getUsedVersionMajorMinor(process.cwd(), 'mustache')).to.be.equal('4.2');
  });
});
