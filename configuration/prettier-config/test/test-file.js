function test(one) {
  console.log('this is a somewhat long message to push the line limit to 110 characters.....right here....');
}

var suchobject = {
  foo: 'bar',
  baz: 42,
  foobar: true,
};

var muchobject = {
  '1foo': 'bar',
  '2baz': 42,
  'foobar': true,
};

var bracketSpacing = {foo: 'bar'};
