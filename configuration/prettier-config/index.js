/** @type {import('prettier').Config} */
const config = {
  tabWidth: 2,
  printWidth: 110,
  useTabs: false,
  semi: true,
  singleQuote: true,
  quoteProps: 'consistent',
  trailingComma: 'all',
  bracketSpacing: false,
  arrowParens: 'avoid',
  endOfLine: 'lf'
}

export default config;
