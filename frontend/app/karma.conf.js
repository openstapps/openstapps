/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

// Karma configuration file, see the link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html
var isDocker = require('is-docker');

/**
 * @param config {import('karma').Config}
 */
module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-coverage'),
      require('karma-mocha-reporter'),
      require('karma-junit-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
    ],
    client: {
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
    },
    coverageReporter: {
      dir: 'coverage/',
      reporters: [{type: 'cobertura', subdir: '.'}],
      // reports: ['html', 'lcovonly', 'text-summary', 'cobertura'],
      fixWebpackSourcePaths: true,
    },
    junitReporter: {
      outputDir: 'coverage/',
      outputFile: 'report-junit.xml',
      useBrowserName: false,
    },
    reporters: ['mocha', 'junit', 'coverage'],
    singleRun: true,
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ['ChromeNoSandbox'],
    customLaunchers: {
      ChromeNoSandbox: {
        base: 'ChromeHeadless',
        // We must disable the Chrome sandbox when running Chrome inside Docker,
        // see https://hackernoon.com/running-karma-tests-with-headless-chrome-inside-docker-ae4aceb06ed3
        flags: isDocker() ? ['--no-sandbox'] : [],
      },
    },
  });
};
