/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
// JIT compiler for dependency mocking
import '@angular/compiler';
import './commands';

beforeEach(function () {
  cy.clearAllSettings();
  cy.setLocalConfig({});
  cy.getAllSettings().should('be.empty');
  cy.setSettings({profile: {language: 'de'}});
  cy.interceptConfig();
  cy.clock(new Date('2020-01-01T12:00:00.000Z'), ['Date']);
});

Cypress.on('window:before:load', window => {
  cy.spy(window.console, 'error').as('consoleError');
});

afterEach(function () {
  // TODO: See if we can enable this again at some point
  // as of now, this causes flakiness because of random network errors
  // and other thing that are not related to the actual test
  // cy.get('@consoleError').should('not.have.been.called');
});

Cypress.on('uncaught:exception', error => {
  return !error.message.includes('ResizeObserver loop limit exceeded');
});
