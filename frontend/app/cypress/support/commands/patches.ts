/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import type {SearchPageComponent} from '../../../src/app/modules/data/list/search-page.component';

/**
 * Sets the due time to zero, higher values can lead to issues in cypress
 */
export function patchSearchPage(dueTime = 0) {
  return cy
    .get('stapps-search-page')
    .component<SearchPageComponent>()
    .then(component => {
      component.searchQueryDueTime = dueTime;
      // component.ngOnDestroy();
      component.ngOnInit();
    });
}
