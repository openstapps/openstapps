/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */
import type {Component, NgZone} from '@angular/core';

/**
 *
 */
export function ng(): Cypress.Chainable<any> {
  return cy.window().its('ng');
}

/**
 * Get the Angular zone (Change Detection!)
 */
export function zone(): Cypress.Chainable<NgZone> {
  return cy.get('app-root').component().its('zone');
}

/**
 * Runs a callback inside Angular so change detection can happen
 */
export function runInsideAngular<T, U>(subject: T, zoneAwareTask: (subject: T) => U): Cypress.Chainable<U> {
  return cy.zone().then(zone => cy.wrap(zone.run(zoneAwareTask, undefined, [subject])));
}

/**
 *
 */
export function component<T = Component>($element: Cypress.JQueryWithSelector): Cypress.Chainable<T> {
  return cy.ng().then(ng => ng.getComponent($element[0]));
}
