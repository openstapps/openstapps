/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
describe('catalog', function () {
  it('should have path', function () {
    cy.interceptGet({uid: 'catalog', fixture: 'catalog/catalog', alias: 'catalog'});
    cy.interceptSearch({extends: 'catalog/children', fixture: 'catalog/children', alias: 'catalog/children'});
    cy.visit('/data-detail/catalog');
    cy.wait('@catalog');

    cy.get('stapps-data-path').within(() => {
      cy.get('ion-breadcrumb').first().should('contain', 'FB 1 - Rechtswissenschaft');
      cy.get('ion-breadcrumb').last().should('contain', 'Studium der Pflichtfächer (1. bis 5. Semester)');
    });
  });
});
