/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('canteen', function () {
  const dishUid = '86464b64-da1e-5578-a5c4-eec23457f596';
  beforeEach(function () {
    cy.interceptSearch({
      extends: 'canteen/all',
      fixture: 'canteen/all',
      alias: 'search',
    });
    cy.interceptMultiSearch({
      extends: 'canteen/dishes',
      fixture: 'canteen/dishes',
      alias: 'dishes',
    });
    cy.interceptGet({
      uid: dishUid,
      fixture: 'canteen/canteen',
      alias: 'detail',
    });
  });

  it('should not utilize the default price', function () {
    cy.visit(`/data-detail/${dishUid}`);
    cy.wait('@detail');
    cy.contains('4,40 €').should('not.exist');
  });

  it('should have a student price', function () {
    cy.setSettings({profile: {group: 'student'}});
    cy.visit(`/data-detail/${dishUid}`);
    cy.wait('@detail');
    cy.contains('3,30 €').should('exist');
  });

  it('should have an employee price', function () {
    cy.setSettings({profile: {group: 'employee'}});
    cy.visit(`/data-detail/${dishUid}`);
    cy.wait('@detail');
    cy.contains('2,20 €').should('exist');
  });

  it('should have a guest price', function () {
    cy.setSettings({profile: {group: 'guest'}});
    cy.visit(`/data-detail/${dishUid}`);
    cy.wait('@detail');
    cy.contains('1,10 €').should('exist');
  });
});
