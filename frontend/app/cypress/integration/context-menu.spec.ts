/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('context menu', function () {
  beforeEach(function () {
    cy.interceptSearch({
      extends: {query: 'a'},
      fixture: 'search/generic',
      alias: 'search',
    });
    cy.interceptMultiSearch({
      extends: 'search/event-chips',
      fixture: 'search/event-chips',
      alias: 'eventChips',
    });
    cy.visit('/search');
    cy.patchSearchPage();
    cy.get('ion-searchbar').type('a');
    cy.wait('@search');
    cy.get('ion-searchbar > ion-menu-button').click();
  });

  it('should sort', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', 'Name').click();
      cy.wait('@search');
    });
  });

  it('should filter', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', '(17) Akademische Veranstaltung').click();
      cy.wait('@search');
    });
  });

  it('should have a working delete button', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', '(17) Akademische Veranstaltung').click();

      cy.get('.checkbox-checked').should('have.length', 1);
      cy.contains('ion-list-header', 'Filter').find('ion-button').click();
      cy.wait('@search');
      cy.get('.checkbox-checked').should('have.length', 0);
    });
  });

  it('should truncate long category items', function () {
    cy.contains('ion-list', 'Akademische Veranstaltung / Kategorien').within(() => {
      cy.contains('ion-item', '(1) Tutorium').should('not.exist');
      cy.get('div > ion-button').click();
      cy.contains('ion-item', '(1) Tutorium').should('exist');
    });
  });
});
