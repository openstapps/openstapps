/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
describe('Settings Page', () => {
  it('should have a proper title', () => {
    cy.visit('/settings');

    cy.get('ion-title').contains('Einstellungen');
  });

  it('should change language', () => {
    cy.visit('/settings');
    cy.contains('ion-select', 'Deutsch').should('be.visible').click({force: true});
    cy.get('ion-popover').contains('ion-item', 'English').click();
    cy.get('ion-popover').should('not.exist');
    cy.get('ion-title').contains('Settings');
    cy.contains('ion-select', 'English').click();
    cy.get('ion-popover').contains('ion-item', 'Deutsch').click();
    cy.get('ion-title').contains('Einstellungen');
  });
});
