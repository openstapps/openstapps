/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('search', function () {
  beforeEach(function () {
    cy.interceptSearch({
      extends: {query: 'test'},
      fixture: 'search/generic',
      alias: 'search',
    });
    cy.interceptMultiSearch({
      extends: 'search/event-chips',
      fixture: 'search/event-chips',
      alias: 'eventChips',
    });
    cy.visit('/search');
    cy.patchSearchPage();
  });

  it('should have search results', function () {
    cy.get('ion-searchbar').type('test');
    cy.wait('@search');
    cy.get('stapps-data-list-item').should('have.length.greaterThan', 1);
  });

  it('should display an error message when no results are found', function () {
    const query = 'a';
    cy.interceptSearch({
      extends: {query},
    });
    cy.get('ion-searchbar').type(query);
    cy.wait('@search');
    cy.get('stapps-data-list-item').should('have.length', 0);
    cy.get('stapps-data-list').contains('Keine Ergebnisse');
  });

  it('should have a working clear button', function () {
    cy.get('ion-searchbar').type('test');
    cy.get('ion-searchbar').should('have.value', 'test');
    cy.wait('@search');
    cy.get('stapps-data-list-item').should('have.length.greaterThan', 1);
    cy.get('.searchbar-clear-button').click();
    cy.get('ion-searchbar').should('have.value', '');
  });
});
