/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Something akin to a.b.c a.b.C but never a... or a.AbC
 */
const probablyBadTranslationPattern = /^\s*([a-z_]+|[A-Z_]+)\.(([a-z_]+|[A-Z_]+)\.)([a-z_]+|[A-Z_]+)$/;

// TODO: Reenable and stabilize tests
//describe('translations', function () {
//  for (const path of [
//    'settings',
//    'news',
//    [
//      'search',
//      () => {
//        cy.visit('/search');
//        cy.get('ion-searchbar').type('test');
//        cy.get('stapps-data-list-item').should('have.length.greaterThan', 1);
//      },
//    ],
//    [
//      'context-menu',
//      () => {
//        cy.visit('/search');
//        cy.get('ion-searchbar').type('test');
//        cy.get('stapps-data-list-item').should('have.length.greaterThan', 1);
//        cy.get('ion-menu-button[menu=context]').click();
//        cy.get('stapps-context');
//      },
//    ],
//    'map',
//    'feedback',
//    'about',
//    'canteen',
//    'catalog',
//    'schedule',
//    'dashboard',
//    [
//      'schedule add modal',
//      () => {
//        cy.visit('/schedule');
//        cy.get('ion-fab').click();
//        cy.get('ion-modal');
//      },
//    ],
//    'profile',
//    'favorites',
//  ] as [string, () => void][]) {
//    const name = Array.isArray(path) ? path[0] : path;
//    const method = Array.isArray(path) ? path[1] : undefined;
//
//    describe(name, function () {
//      it('should not contain failed translation paths', function () {
//        if (method) {
//          method();
//        } else {
//          cy.visit(`/${path}`);
//        }
//
//        cy.wait(500);
//
//        cy.get('ion-app *').each($element => {
//          const text = $element.text();
//          if (text) {
//            expect(text).not.to.match(probablyBadTranslationPattern);
//          }
//        });
//      });
//    });
//  }
//});
