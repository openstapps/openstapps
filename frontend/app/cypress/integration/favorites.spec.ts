/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('favorites', function () {
  beforeEach(() => {
    cy.interceptSearch({
      extends: {query: 'test'},
      fixture: 'search/generic',
      alias: 'search',
    });
    cy.interceptMultiSearch({
      extends: 'search/event-chips',
      fixture: 'search/event-chips',
    });
  });

  it('should add a favorite', function () {
    cy.visit('/search');
    cy.patchSearchPage();
    cy.get('ion-searchbar').type('test');
    let text!: string;
    cy.get('stapps-data-list-item')
      .first()
      .within(() => {
        cy.get('.title')
          .invoke('text')
          .then(it => {
            text = it;
          });
        cy.get('stapps-favorite-button').click();
        cy.get('stapps-favorite-button > ion-button > ion-icon')
          .invoke('attr', 'ng-reflect-fill')
          .should('eq', 'true');
      });
    cy.visit('/favorites');
    cy.get('stapps-data-list-item').within(() => {
      cy.get('.title').should('contain', text);
      cy.get('stapps-favorite-button').click();
    });
    cy.get('stapps-data-list').contains('Keine Ergebnisse').should('be.visible');
  });
});
