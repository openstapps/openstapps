/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
describe('ratings', function () {
  const dishUid = '86464b64-da1e-5578-a5c4-eec23457f596';
  beforeEach(() => {
    cy.intercept('https://mobile.server.uni-frankfurt.de/rating', {
      body: {},
    }).as('rating');
    cy.interceptMultiSearch({
      extends: 'canteen/dishes',
      fixture: 'canteen/dishes',
      alias: 'dishes',
    });
    cy.interceptGet({
      uid: dishUid,
      fixture: 'canteen/canteen',
      alias: 'detail',
    });
  });

  it('should open ratings', function () {
    cy.visit(`/data-detail/${dishUid}`);
    cy.get('.rating-stars').should('not.exist');
    cy.get('stapps-rating').first().click({scrollBehavior: 'center'});
    cy.get('.rating-stars').should('exist');
  });

  it('should submit ratings', function () {
    cy.visit(`/data-detail/${dishUid}`);
    cy.get('stapps-rating').first().click({scrollBehavior: 'center'});
    cy.get('.rating-stars > ion-icon').first().click({scrollBehavior: 'center'});
    cy.wait('@rating').its('request.body.rating').should('eq', 5);
  });

  it('should not be possible to rate twice', function () {
    cy.visit(`/data-detail/${dishUid}`);
    cy.get('stapps-rating').first().click({scrollBehavior: 'center'});
    cy.get('.rating-stars > ion-icon').first().click({scrollBehavior: 'center'});
    cy.wait('@rating');
    cy.get('stapps-rating ion-button').should('have.class', 'button-disabled');
    cy.visit('/data-detail/86464b64-da1e-5578-a5c4-eec23457f596');
    cy.get('stapps-rating ion-button').should('have.class', 'button-disabled');
  });

  it('should display a thank you message', function () {
    cy.visit(`/data-detail/${dishUid}`);
    cy.get('stapps-rating').first().click({scrollBehavior: 'center'});
    cy.get('.rating-stars > ion-icon').first().click({scrollBehavior: 'center'});
    cy.wait('@rating');
    cy.get('.thank-you').should('be.visible');
  });

  it('should be dismissible', function () {
    cy.visit(`/data-detail/${dishUid}`);
    cy.get('stapps-rating').first().click({scrollBehavior: 'center'});
    cy.get('.rating-stars').should('be.visible');
    cy.get('body').click(0, 0);
    cy.get('.rating-stars').should('not.exist');
  });
});
