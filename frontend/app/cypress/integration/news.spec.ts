/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('news', function () {
  beforeEach(function () {
    cy.interceptSearch({
      extends: 'news/all',
      fixture: 'news/all',
      alias: 'search',
    });
    cy.interceptGet({
      uid: 'message',
      fixture: 'news/message',
      alias: 'detail',
    });
  });

  it('should show all articles by default', function () {
    cy.visit('/news');
    cy.get('stapps-news-item').should('have.length', 6);
  });

  it('should reload on filter change', function () {
    cy.visit('/news');
    cy.get('stapps-news-item').should('have.length', 6);
    cy.get('stapps-news-settings-filter').first().click({force: true});
    cy.wait('@search');
  });

  it('should have an external link indicator on detail pages', function () {
    cy.visit('/data-detail/message');
    cy.wait('@detail');
    cy.contains('ion-card', 'Ursprünglicher Link').find('ion-icon[name="open_in_browser"]').should('exist');
  });
});
