describe('opening hours', () => {
  beforeEach(function () {
    cy.interceptSearch({
      extends: 'canteen/all',
      fixture: 'canteen/all',
      alias: 'search',
    });
    cy.interceptMultiSearch({
      extends: 'canteen/dishes',
      fixture: 'canteen/dishes',
      alias: 'dishes',
    });
    cy.clock().invoke('restore');
  });

  it('should specify relative closing time', () => {
    cy.clock(new Date(2023, 9, 16, 15, 29), ['Date']);
    cy.visit('/canteen');

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt heute um 22:00');
  });

  it('should specify relative opening time', () => {
    cy.clock(new Date(2023, 9, 16, 6, 29), ['Date']);
    cy.visit('/canteen');

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geschlossen')
      .should('contain', 'Öffnet heute um 08:30');
  });

  it('should specify soon opening time', () => {
    cy.clock(new Date(2023, 9, 16, 8, 0), ['Date']);
    cy.visit('/canteen');

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geschlossen')
      .should('contain', 'Öffnet in 30 Minuten');
  });

  it('should specify soon closing time', () => {
    cy.clock(new Date(2023, 9, 16, 21, 30), ['Date']);
    cy.visit('/canteen');

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt in 30 Minuten');
  });

  it('should update the soon closing time every minute', () => {
    cy.clock(new Date(2023, 9, 16, 21, 30));
    cy.visit('/canteen');
    cy.tick(500);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt in 30 Minuten');

    cy.tick(60_000);
    cy.tick(50);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt in 29 Minuten');
  });

  it('should update the status when it changes', () => {
    cy.clock(new Date(2023, 9, 16, 21, 59));
    cy.visit('/canteen');
    cy.tick(500);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt in 1 Minute');

    cy.tick(60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geschlossen')
      .should('contain', 'Öffnet morgen um 08:30');
  });

  // This one takes long to execute!
  it('should update as expected', () => {
    cy.clock(new Date(2023, 9, 16, 20, 59));
    cy.visit('/canteen');
    cy.tick(500);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt heute um 22:00');

    cy.tick(60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt in 60 Minuten');

    cy.tick(30 * 60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt in 30 Minuten');

    cy.tick(30 * 60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geschlossen')
      .should('contain', 'Öffnet morgen um 08:30');

    cy.tick(9.5 * 60 * 60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geschlossen')
      .should('contain', 'Öffnet in 60 Minuten');

    cy.tick(30 * 60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geschlossen')
      .should('contain', 'Öffnet in 30 Minuten');

    cy.tick(30 * 60_000);

    cy.get('stapps-opening-hours')
      .first()
      .should('contain', 'Geöffnet')
      .should('contain', 'Schließt heute um 22:00');

    // Long tick warps will cause network requests to time out
    cy.get('@consoleError').invoke('resetHistory');
  });
});
