/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('schedule', function () {
  beforeEach(function () {
    cy.interceptSearch({
      extends: {query: 'a'},
      fixture: 'schedule/events',
      alias: 'search',
    });
    cy.interceptMultiSearch({
      extends: 'schedule/events',
      fixture: 'schedule/events',
      alias: 'chips',
    });
    cy.interceptSearch({
      extends: 'schedule/date-series',
      fixture: 'schedule/date-series',
      alias: 'dateSeries',
    });
  });

  it('should respect the url', function () {
    cy.visit('/schedule/calendar/2022-01-19');
    cy.get('#date-select-button0').should('contain', '19.01.22');
  });

  it('should navigate a full page', function () {
    cy.visit('/schedule/calendar/2022-01-19');

    cy.get('.swiper-slide-active').should('contain', 'Mi');

    cy.get('.left-button').click();
    cy.get('.schedule-wrapper .swiper').its('0.swiper.animating').should('eq', false);
    cy.get('.swiper-slide-active').should('contain', 'So');

    cy.get('.right-button').click();
    cy.get('.schedule-wrapper .swiper').its('0.swiper.animating').should('eq', false);
    cy.get('.swiper-slide-active').should('contain', 'Mi');

    cy.get('.right-button').click();
    cy.get('.schedule-wrapper .swiper').its('0.swiper.animating').should('eq', false);
    cy.get('.swiper-slide-active').should('contain', 'Sa');
  });

  for (const [width, count] of [
    [760, 3],
    [770, 3],
    [1700, 7],
  ]) {
    const slideMultiplier = 3;
    it(`should have ${count} slides for ${width}px`, function () {
      cy.visit('/schedule/calendar/2022-01-59');
      cy.viewport(width, 550);
      cy.get('.schedule-wrapper > .infinite-swiper-wrapper')
        .find('.swiper-slide')
        .should('have.length', slideMultiplier * count)
        .first()
        .invoke('outerWidth')
        .should('be.gt', 140);
    });
  }

  it('should navigate to a specific date', function () {
    cy.visit('/schedule/calendar/2059-01-19');
    cy.contains('#date-select-button0', '19.01.59').click();

    cy.get('ion-datetime').should('be.visible').and('have.class', 'datetime-ready');
    cy.get('input.aux-input').should('have.attr', 'value', '2059-01-19');
    cy.get('.calendar-header', {includeShadowDom: true}).should('contain', 'January 2059');

    cy.get('button[data-day=1][data-month=1][data-year=2059]', {
      includeShadowDom: true,
    }).click();

    cy.get('input.aux-input').should('have.attr', 'value', '2059-01-01');
    cy.get('ion-datetime').should('not.exist');
    cy.get('#date-select-button0').should('contain', '01.01.59');
  });

  it('should add events', function () {
    cy.visit('/schedule/calendar/2059-01-19');
    cy.get('stapps-schedule-card').should('not.exist');

    cy.get('ion-fab-button').click();

    cy.patchSearchPage();
    cy.get('ion-searchbar').click().type('a');
    cy.wait('@search');
    cy.wait('@chips');
    cy.wait(200);
    cy.contains('ion-item', 'UNIcert (Test)')
      .contains('stapps-add-event-action-chip', 'Termine Auswählen')
      .click();

    cy.contains('ion-item', /19\. Januar 2059, \d+:00 - \d+:00/).click();
    cy.contains('ion-button', 'Bestätigen').click();

    cy.wait('@dateSeries');
    cy.contains('ion-item', 'UNIcert (Test)')
      .contains('stapps-add-event-action-chip', 'Hinzugefügt')
      .should('exist');
    cy.get('ion-back-button').last().click();

    cy.get('stapps-schedule-card').should('exist');
  });
});
