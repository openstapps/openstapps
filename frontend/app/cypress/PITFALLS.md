# A list of (possibly) common pitfalls

## The component is not updating

### Calling Cypress invoke will not trigger change detection

Rewrite

```typescript
cy.get('component-selector').component().invoke('someFunction');
```

Into

```typescript
cy.get('component-selector')
  .component()
  .runInsideAngular(component => component.someFunction());
```

## The backend is unreachable

See `BACKEND.md`, direct calls to backend are prohibited.

## The search page doesn't work

Run `cy.patchSearchPage()` after visiting the search page.
It set the due time to zero so that the component
won't wait for someone to type more.
