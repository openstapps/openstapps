# Backend Mocking

The backend _must_ be mocked using fixtures or other methods.

You can use

```typescript
cy.interceptMultiSearch(...)
cy.interceptSearch({extend: 'fixture', fixture: 'fixture', alias: 'alias'})
cy.interceptGet({uid})
```

For mocking the responses based on a request.

Paths in the commands will be relative to those, so
`cy.interceptSearch({extend: 'request', fixture: 'response'})`
will actually pull the fixtures from `request.search.req.json`
and `response.search.res.json` respectively.

If `fixture` is omitted, an empty response will be returned.
