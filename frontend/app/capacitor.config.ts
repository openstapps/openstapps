import {CapacitorConfig} from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'de.anyschool.app',
  appName: 'StApps',
  webDir: 'www',
  cordova: {
    preferences: {
      'AndroidXEnabled': 'true',
      'android-minSdkVersion': '22',
      'BackupWebStorage': 'none',
    },
  },
  plugins: {
    SplashScreen: {
      launchAutoHide: false,
      backgroundColor: '#3880ff',
      showSpinner: false,
    },
    LocalNotifications: {
      // TODO
    },
    CapacitorHttp: {
      enabled: false,
    },
  },
};

export default config;
