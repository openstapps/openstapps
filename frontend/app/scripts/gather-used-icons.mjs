/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {glob} from 'glob';
import {readFileSync} from 'fs';
import {
  matchPropertyAccess,
  matchPropertyContent,
  matchTagProperties,
} from '../src/app/util/ion-icon/icon-match.mjs';

/**
 * @returns {Promise<Record<string, string[]>>}
 */
export async function getUsedIconsHtml(pattern = 'src/**/*.html') {
  return Object.fromEntries(
    (await glob(pattern))
      .map(file => [
        file,
        readFileSync(file, 'utf8')
          .match(matchTagProperties('ion-icon'))
          ?.flatMap(match => {
            return match.match(matchPropertyContent(['name', 'md', 'ios']));
          })
          .filter(it => !!it) || [],
      ])
      .filter(([, values]) => values && values.length > 0),
  );
}

/**
 * @returns {Promise<Record<string, string[]>>}
 */
export async function getUsedIconsTS(pattern = 'src/**/*.ts') {
  const regex = matchPropertyAccess('SCIcon');
  return Object.fromEntries(
    (await glob(pattern))
      .map(file => [file, readFileSync(file, 'utf8').match(regex) || []])
      .filter(([, values]) => values && values.length > 0),
  );
}
