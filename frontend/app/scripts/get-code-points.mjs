const url =
  'https://raw.githubusercontent.com/google/material-design-icons/master/' +
  'variablefont/MaterialSymbolsRounded%5BFILL%2CGRAD%2Copsz%2Cwght%5D.codepoints';

export async function fetchCodePointMap() {
  const icons = await fetch(url)
    .then(it => it.text())
    .then(it => new Map(it.split('\n').map(it => /** @type {[string, string]} */ (it.split(' ')))));
  if (icons.size < 100) throw new Error(`Code point map is very small, is the URL incorrect? ${url}`);
  return icons;
}

/**
 * @param {string[]} icons
 */
export async function getCodePoints(icons) {
  const codePoints = await fetchCodePointMap();
  return icons.map(icon => {
    const code = codePoints.get(icon);
    if (!code) throw new Error(`Code point for icon ${icon} not found`);
    return code;
  });
}
