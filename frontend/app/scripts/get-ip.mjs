import {networkInterfaces} from 'os';

console.log(
  Object.entries(networkInterfaces())
    .map(([, info]) => info)
    .flat()
    .find(info => info && !info.internal && info.family === 'IPv4')?.address,
);
