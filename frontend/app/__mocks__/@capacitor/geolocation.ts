/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

export interface Position {
  timestamp: number;
  coords: {
    latitude: number;
    longitude: number;
    accuracy: number;
    altitudeAccuracy: number | null | undefined;
    altitude: number | null;
    speed: number | null;
    heading: number | null;
  };
}

const samplePosition: Position = {
  coords: {
    heading: 123,
    latitude: 34.12,
    longitude: 12.34,
    accuracy: 1,
    altitude: 123,
    altitudeAccuracy: 1,
    speed: 1,
  },
  timestamp: 1_565_275_805_901,
} as Position;

export class GeolocationMock {
  // @ts-ignore
  checkPermissions(): Promise<PermissionStatus> {
    // @ts-ignore
    return Promise.resolve({});
  }

  clearWatch(_options: any): Promise<void> {
    return Promise.resolve(undefined);
  }

  getCurrentPosition(_options?: any): Promise<Position> {
    return Promise.resolve(samplePosition);
  }

  // @ts-ignore
  requestPermissions(permissions?: any): Promise<PermissionStatus> {
    // @ts-ignore
    return Promise.resolve(undefined);
  }

  watchPosition(
    _options: PositionOptions,
    callback: (position: Position, error?: any) => void,
  ): Promise<string> {
    callback(samplePosition);
    return Promise.resolve('');
  }
}

export interface PermissionStatus {
  location: PermissionState;
  coarseLocation: PermissionState;
}

export const Geolocation = new GeolocationMock();
