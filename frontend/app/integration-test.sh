#!/usr/bin/env bash

rm coverage/integration-report-junit-*.xml

ng e2e --watch=false --headless=true --browser="$BROWSER"
exit_code=$?

jrm coverage/integration-report-junit.xml coverage/integration-report-junit-*.xml
rm coverage/integration-report-junit-*.xml

exit $exit_code
