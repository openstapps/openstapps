import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'addWordBreakOpportunities',
  standalone: true,
})
export class AddWordBreakOpportunitiesPipe implements PipeTransform {
  transform(value: string): string {
    return value.replaceAll('/', '/​').replaceAll(/([a-z])([A-Z])/g, '$1​$2');
  }
}
