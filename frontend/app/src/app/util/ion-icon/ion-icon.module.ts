/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {NgModule} from '@angular/core';
import {IconComponent} from './icon.component';
import {IonIconDirective} from './ion-icon.directive';
import {IonBackButtonDirective} from './ion-back-button.directive';
import {IonSearchbarDirective} from './ion-searchbar.directive';
import {IonBreadcrumbDirective} from './ion-breadcrumb.directive';
import {IonReorderDirective} from './ion-reorder.directive';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {CommonModule, TitleCasePipe} from '@angular/common';

@NgModule({
  imports: [TranslateModule, CommonModule],
  declarations: [
    IconComponent,
    IonIconDirective,
    IonBackButtonDirective,
    IonSearchbarDirective,
    IonBreadcrumbDirective,
    IonReorderDirective,
  ],
  exports: [
    IonIconDirective,
    IonReorderDirective,
    IonBackButtonDirective,
    IonSearchbarDirective,
    IonBreadcrumbDirective,
  ],
  providers: [TranslateService, TitleCasePipe],
})
export class IonIconModule {}
