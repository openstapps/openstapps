export function matchTagProperties(tag: string): RegExp;

export function matchPropertyContent(properties: string[]): RegExp;

export function matchPropertyAccess(objectName: string): RegExp;
