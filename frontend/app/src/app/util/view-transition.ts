/**
 * Performs a view transition
 *
 * This is a progressive enhancement for (as of right now) Chromium-based browsers
 * - Firefox position: [positive](https://mozilla.github.io/standards-positions/#view-transitions)
 * - WebKit position: [support](https://github.com/WebKit/standards-positions/issues/48#issuecomment-1679760489)
 */
export function startViewTransition(runner: () => Promise<void>) {
  if ('startViewTransition' in document) {
    document.startViewTransition(runner);
  } else {
    void runner();
  }
}
