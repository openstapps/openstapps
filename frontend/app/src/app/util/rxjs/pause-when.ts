import {filter, MonoTypeOperatorFunction, Observable, repeat, takeUntil} from 'rxjs';

/**
 * Pause the observable if the notifier emits true, and resume when it emits false
 */
export function pauseWhen<T>(notifier: Observable<boolean>): MonoTypeOperatorFunction<T> {
  return value =>
    value.pipe(
      takeUntil(notifier.pipe(filter(it => it))),
      repeat({delay: () => notifier.pipe(filter(it => !it))}),
    );
}
