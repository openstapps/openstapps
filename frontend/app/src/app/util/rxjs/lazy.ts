import {Observable} from 'rxjs';

/**
 * Lazy-load something
 * @param construct the constructing function
 * @example lazy(() => import('module').then(it => it.default))
 */
export function lazy<T>(construct: () => Promise<T>): Observable<T> {
  let value: Promise<T>;
  return new Observable<T>(subscriber => {
    value ??= construct();
    value.then(it => {
      subscriber.next(it);
      subscriber.complete();
    });
  });
}
