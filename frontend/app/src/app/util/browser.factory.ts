/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Platform} from '@ionic/angular';
import {Browser as BrowserPlugin} from '@capacitor/browser';

export abstract class SimpleBrowser {
  abstract open(url: string): Promise<void> | void;
}

class CapacitorBrowser {
  open(url: string) {
    return BrowserPlugin.open({url});
  }
}

class WebBrowser {
  open(url: string) {
    window.open(url, '_blank');
  }
}

export const browserFactory = (platform: Platform) => {
  return platform.is('capacitor') ? new CapacitorBrowser() : new WebBrowser();
};
