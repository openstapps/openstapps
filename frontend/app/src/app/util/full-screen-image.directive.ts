import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: 'img[fullScreenImage]',
  standalone: true,
})
export class FullScreenImageDirective {
  constructor(private host: ElementRef) {}

  @HostListener('click')
  async onClick() {
    if (document.fullscreenElement) {
      await document.exitFullscreen();
    } else {
      await this.host.nativeElement.requestFullscreen();
      if (
        Math.sign(screen.width - screen.height) ===
          Math.sign(this.host.nativeElement.width - this.host.nativeElement.height) &&
        'lock' in screen.orientation
      ) {
        await (screen.orientation.lock as (orientation: string) => Promise<void>)('landscape');
      }
    }
  }
}
