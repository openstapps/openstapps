import {PipeTransform} from '@angular/core';
import {Pipe} from '@angular/core';
import {Observable, fromEvent, map, startWith} from 'rxjs';

@Pipe({
  name: 'mediaQuery',
  pure: true,
  standalone: true,
})
export class MediaQueryPipe implements PipeTransform {
  transform(query: string): Observable<boolean> {
    const match = window.matchMedia(query);
    return fromEvent<MediaQueryListEvent>(match, 'change').pipe(
      map(event => event.matches),
      startWith(match.matches),
    );
  }
}
