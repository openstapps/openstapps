/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {animate, style, transition, trigger} from '@angular/animations';

export const chipTransition = trigger('chipTransition', [
  transition(':enter', [
    style({
      'opacity': 0,
      'transform': 'scaleX(80%)',
      'transform-origin': 'left',
    }),
    animate('200ms ease', style({opacity: 1, transform: 'scaleX(100%)'})),
  ]),
]);

export const chipSkeletonTransition = trigger('chipSkeletonTransition', [
  transition(':leave', [
    style({
      'opacity': 1,
      'transform': 'scaleX(100%)',
      'transform-origin': 'left',
    }),
    animate('200ms ease', style({opacity: 0, transform: 'scaleX(120%)'})),
  ]),
]);
