/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input, OnInit} from '@angular/core';
import {HoursRange} from '../schema/schema';
import moment from 'moment';
import {getScheduleCursorOffset} from './schedule-cursor-offset';

/**
 * Component that displays the schedule
 */
@Component({
  selector: 'stapps-schedule-cursor',
  templateUrl: 'schedule-cursor.html',
  styleUrls: ['schedule-cursor.scss'],
})
export class ScheduleCursorComponent implements OnInit {
  /**
   * Cursor update
   */
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore unused
  private cursorInterval: NodeJS.Timeout;

  /**
   * Range of hours to display
   */
  @Input() readonly hoursRange: HoursRange;

  /**
   * Cursor
   */
  now = ScheduleCursorComponent.getCursorTime();

  /**
   * Vertical scale of the schedule (distance between hour lines)
   */
  @Input() readonly scale: number;

  getScheduleCursorOffset = getScheduleCursorOffset;

  /**
   * Get a floating point time 0..24
   */
  static getCursorTime(): number {
    const mnt = moment(moment.now());

    const hh = mnt.hours();
    const mm = mnt.minutes();

    // tslint:disable-next-line:no-magic-numbers
    return hh + mm / 60;
  }

  /**
   * Initialize
   */
  ngOnInit() {
    this.cursorInterval = setInterval(async () => {
      this.now = ScheduleCursorComponent.getCursorTime();
      // tslint:disable-next-line:no-magic-numbers
    }, 1000 * 60 /*1 Minute*/);
  }
}
