/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCDateSeries, SCISO8601Duration} from '@openstapps/core';
import {unitOfTime} from 'moment';

interface DateRange {
  duration: SCISO8601Duration;
  start: number;
  startAsString?: string;
  endAsString?: string;
}

export interface Range<T> {
  from: T;
  to: T;
}

/**
 * Minimal interface to provide information about a custom event
 */
export interface ScheduleEvent {
  /**
   * UUIDs of things related to the event
   */
  dateSeries: SCDateSeries;

  /**
   * How long the event goes
   */
  time: DateRange;
}

/**
 * Range of hours
 */
export interface HoursRange {
  /**
   * Start hour
   */
  from: number;

  /**
   * End hour
   */
  to: number;
}

export interface ScheduleResponsiveBreakpoint {
  /**
   * Number of days to display
   */
  days: number;

  /**
   * When the first day should start
   */
  startOf: unitOfTime.StartOf;

  /**
   * Width until next breakpoint is hit
   */
  until: number;
}
