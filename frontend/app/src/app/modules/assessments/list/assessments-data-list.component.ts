/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SCThings} from '@openstapps/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'assessments-data-list',
  templateUrl: './assessments-data-list.html',
  styleUrls: ['./assessments-data-list.scss'],
})
export class AssessmentsDataListComponent {
  /**
   * All SCThings to display
   */
  @Input() items?: SCThings[];

  /**
   * Output binding to trigger pagination fetch
   */
  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('loadmore') loadMore = new EventEmitter<void>();

  /**
   * Emits when scroll view should reset to top
   */
  @Input() resetToTop?: Observable<void>;

  /**
   * Indicates whether the list is to display SCThings of a single type
   */
  @Input() singleType = false;

  /**
   * Signalizes that the data is being loaded
   */
  @Input() loading = true;
}
