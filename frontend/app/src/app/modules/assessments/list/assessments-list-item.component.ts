/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, Input} from '@angular/core';
import {SCThings} from '@openstapps/core';

@Component({
  selector: 'assessments-list-item',
  templateUrl: 'assessments-list-item.html',
  styleUrls: ['assessments-list-item.scss'],
})
export class AssessmentsListItemComponent {
  /**
   * Whether the list item should show a thumbnail
   */
  @Input() hideThumbnail = false;

  /**
   * An item to show
   */
  @Input() item: SCThings;
}
