/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSettingCategories, SCThingsField} from '@openstapps/core';
/**
 * Category of settings to use for news filter
 */
export const newsFilterSettingsCategory: SCSettingCategories = 'profile';
/**
 * Settings to use for news filter
 */
export type NewsFilterSettingsNames = 'language' | 'group';
/**
 * The mapping between settings and corresponding data fields for building a value filter
 */
export const newsFilterSettingsFieldsMapping: {
  [key in NewsFilterSettingsNames]: SCThingsField;
} = {
  language: 'inLanguage',
  group: 'audiences',
};
