/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MomentModule} from 'ngx-moment';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {DataModule} from '../data/data.module';
import {SettingsProvider} from '../settings/settings.provider';
import {NewsItemComponent} from './item/news-item.component';
import {NewsPageComponent} from './page/news-page.component';
import {SkeletonNewsItemComponent} from './item/skeleton-news-item.component';
import {ChipFilterComponent} from '../data/chips/filter/chip-filter.component';
import {SettingsModule} from '../settings/settings.module';
import {NewsSettingsFilterComponent} from './elements/news-filter-settings/news-settings-filter.component';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

const newsRoutes: Routes = [{path: 'news', component: NewsPageComponent}];

/**
 * News Module
 */
@NgModule({
  declarations: [
    NewsPageComponent,
    SkeletonNewsItemComponent,
    NewsItemComponent,
    ChipFilterComponent,
    NewsSettingsFilterComponent,
  ],
  imports: [
    IonicModule.forRoot(),
    ThingTranslateModule.forChild(),
    TranslateModule.forChild(),
    RouterModule.forChild(newsRoutes),
    IonIconModule,
    CommonModule,
    MomentModule,
    DataModule,
    ThingTranslateModule,
    SettingsModule,
    UtilModule,
  ],
  providers: [SettingsProvider],
  exports: [NewsItemComponent],
})
export class NewsModule {}
