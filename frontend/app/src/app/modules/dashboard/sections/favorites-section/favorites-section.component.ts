/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ChangeDetectionStrategy, Component} from '@angular/core';
import {filter, map} from 'rxjs/operators';
import {FavoritesService} from '../../../favorites/favorites.service';
import {fadeAnimation} from '../../fade.animation';
import {isMensaThing} from '../../mensa-filters';

/**
 * Shows a section with meals of the chosen mensa
 */
@Component({
  selector: 'stapps-favorites-section',
  templateUrl: 'favorites-section.component.html',
  styleUrls: ['favorites-section.component.scss'],
  animations: [fadeAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoritesSectionComponent {
  items = this.favoritesService.favoriteThings$.pipe(
    map(favorites => favorites.filter(it => !isMensaThing(it))),
    filter(favorites => favorites.length > 0),
  );

  constructor(private favoritesService: FavoritesService) {}
}
