/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {AnimationController} from '@ionic/angular';
import {homePageSearchTransition} from './search-route-transition';

/**
 * Shows a search input field
 */
@Component({
  selector: 'stapps-search-section',
  templateUrl: 'search-section.component.html',
  styleUrls: ['search-section.component.scss'],
})
export class SearchSectionComponent {
  routeTransition = homePageSearchTransition(this.animationController);

  constructor(private animationController: AnimationController) {}
}
