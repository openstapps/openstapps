/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ChangeDetectionStrategy, Component} from '@angular/core';
import {NewsProvider} from '../../../news/news.provider';
import {fadeAnimation} from '../../fade.animation';

/**
 * Shows a section with news
 */
@Component({
  selector: 'stapps-news-section',
  templateUrl: 'news-section.component.html',
  styleUrls: ['news-section.component.scss'],
  animations: [fadeAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsSectionComponent {
  news = this.newsProvider
    .getCurrentFilters()
    // eslint-disable-next-line unicorn/prefer-top-level-await,unicorn/consistent-function-scoping
    .then(filters => this.newsProvider.getList(5, 0, filters));

  constructor(readonly newsProvider: NewsProvider) {}
}
