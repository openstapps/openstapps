/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import {SCSearchResult, SCThingType} from '@openstapps/core';
import {DataProvider} from 'src/app/modules/data/data.provider';
import {fadeAnimation} from '../../fade.animation';

/**
 * Shows a section with meals of the chosen mensa
 */
@Component({
  selector: 'stapps-job-section',
  templateUrl: 'job-section.component.html',
  styleUrls: ['job-section.component.scss'],
  animations: [fadeAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobSectionComponent {
  jobs = inject(DataProvider)
    .search({
      filter: {type: 'value', arguments: {field: 'type', value: SCThingType.JobPosting}},
      size: 5,
      from: 0,
    })
    .then((result: SCSearchResult) => result.data);
}
