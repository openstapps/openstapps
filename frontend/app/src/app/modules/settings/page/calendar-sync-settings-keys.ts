/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {StorageProvider} from '../../storage/storage.provider';

export const CALENDAR_SYNC_SETTINGS_KEY = 'calendarSettings';
export const CALENDAR_SYNC_ENABLED_KEY = 'sync';
export const CALENDAR_NOTIFICATIONS_ENABLED_KEY = 'notifications';
export type CALENDAR_SYNC_KEYS = typeof CALENDAR_SYNC_ENABLED_KEY | typeof CALENDAR_NOTIFICATIONS_ENABLED_KEY;

/**
 *
 */
export async function getCalendarSetting(
  storageProvider: StorageProvider,
  key: CALENDAR_SYNC_KEYS,
  defaultValue = false,
): Promise<boolean> {
  try {
    return await storageProvider.get<boolean>(calendarSettingStorageKey(key));
  } catch {
    return defaultValue;
  }
}

/**
 *
 */
export function calendarSettingStorageKey(key: string): string {
  return `${CALENDAR_SYNC_SETTINGS_KEY}.${key}`;
}
