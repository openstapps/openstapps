import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {SCAppVersionInfo} from '@openstapps/core';
import {MarkdownModule} from 'ngx-markdown';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {IonicModule, ModalController} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {UtilModule} from '../../util/util.module';

@Component({
  selector: 'stapps-release-notes',
  templateUrl: 'release-notes.html',
  styleUrls: ['release-notes.scss', 'release-notes-markdown.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [UtilModule, MarkdownModule, ThingTranslateModule, IonicModule, TranslateModule],
})
export class ReleaseNotesComponent {
  @Input() versionInfos: SCAppVersionInfo[];

  constructor(readonly modalController: ModalController) {}
}
