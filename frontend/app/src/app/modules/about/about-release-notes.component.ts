/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import {ConfigProvider} from '../config/config.provider';

@Component({
  selector: 'about-changelog',
  templateUrl: 'about-release-notes.html',
  styleUrls: ['about-release-notes.scss', './about-page/about-page.scss', 'release-notes-markdown.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutReleaseNotesComponent {
  config = inject(ConfigProvider);
}
