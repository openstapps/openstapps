/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {AboutLicenseModalComponent} from './about-license-modal.component';
import licensesFile from 'src/assets/about/licenses.json';

export interface License {
  name: string;
  licenses: string;
  repository: string;
  authors?: string;
  publisher?: string;
  email?: string;
  url?: string;
  licenseText?: string;
}

@Component({
  selector: 'about-changelog',
  templateUrl: 'about-licenses.html',
  styleUrls: ['about-licenses.scss', './about-page/about-page.scss'],
})
export class AboutLicensesComponent implements OnInit {
  licenses: License[];

  constructor(private modalController: ModalController) {}

  ngOnInit() {
    this.licenses = this.loadLicenses();
  }

  async viewLicense(license: License) {
    const modal = await this.modalController.create({
      component: AboutLicenseModalComponent,
      componentProps: {
        license: license,
        dismissAction: () => {
          modal.dismiss();
        },
      },
      canDismiss: true,
    });
    return await modal.present();
  }

  loadLicenses(): License[] {
    return licensesFile as License[];
  }
}
