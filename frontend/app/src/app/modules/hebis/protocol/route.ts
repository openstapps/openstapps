import {
  SCAbstractRoute,
  SCRouteHttpVerbs,
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '@openstapps/core';

/**
 * Route for searching things
 */
export class SCHebisSearchRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCHebisSearchRequest';
    this.responseBodyName = 'SCHebisSearchResponse';
    this.statusCodeSuccess = 200;
    this.urlPath = '/hebissearch';
  }
}

/**
 * Route for availability
 */
export class SCDaiaRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.GET;
    this.requestBodyName = 'SCDaiaAvailabilityRequest';
    this.responseBodyName = 'SCDaiaAvailabilityResponse';
    this.statusCodeSuccess = 200;
    this.urlPath = '/UB_Frankfurt';
  }
}
