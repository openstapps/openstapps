/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCArticle, SCBook, SCPeriodical, SCSearchResultPagination} from '@openstapps/core';

export interface HebisSearchResponse {
  /**
   * Response Array of SCBook type or Object
   */
  data: Array<SCBook | SCArticle | SCPeriodical>;

  /**
   * Pagination information
   */
  pagination: SCSearchResultPagination;
}

export interface DaiaAvailabilityResponse {
  document: Array<{
    item: Array<{
      id: string;
      label: string;
      chronology?: {
        about?: string;
      };
      department: DaiaSimpleContent;
      available: DaiaService[];
      unavailable: DaiaService[];
      debugInfo: string;
      about?: string;
      storage?: DaiaSimpleContent;
    }>;
  }>;
  institution: DaiaSimpleContent;
  timestamp: string;
}

export interface DaiaSimpleContent {
  id: string;
  content: string;
  href?: string;
}

export interface DaiaService {
  delay?: string;
  href?: string;
  service: string;
  expected?: string;
  limitations?: DaiaSimpleContent[];
}

export interface DaiaHolding {
  id: string;
  department: DaiaSimpleContent;
  signature: string;
  storage?: DaiaSimpleContent;
  available?: DaiaService;
  unavailable?: DaiaService;
  about?: string;
  online: boolean;
  open?: boolean;
  dueDate?: string;
  holdings?: string;
  status?: 'checked_out' | 'not_yet_available' | 'not_available' | 'library_only' | 'available' | 'unknown';
}
