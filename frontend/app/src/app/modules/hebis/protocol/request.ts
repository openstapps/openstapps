import {SCLicensePlate} from '@openstapps/core';

export interface SCHebisSearchRequest {
  /**
   * HDS2 will supply results for the speficied insitute / university if available (Defaults to f-u)
   */
  institute?: SCLicensePlate;
  /**
   * Simple pagination support (Defaults to 0)
   */
  page?: number;
  /**
   * Search query for HDS
   */
  query: string;
}

export interface SCDaiaAvailabilityRequest {
  id: string;
}
