/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, inject, OnInit} from '@angular/core';
import {SCUuid} from '@openstapps/core';
import {HebisDataProvider} from '../hebis-data.provider';
import {DataDetailComponent} from '../../data/detail/data-detail.component';
import {DaiaHolding} from '../protocol/response';

/**
 * A Component to display an SCThing detailed
 */
@Component({
  selector: 'stapps-hebis-detail',
  styleUrls: ['hebis-detail.scss'],
  templateUrl: 'hebis-detail.html',
})
export class HebisDetailComponent extends DataDetailComponent implements OnInit {
  holdings: DaiaHolding[];

  private hebisDataProvider = inject(HebisDataProvider);

  /**
   * @override
   */
  async ngOnInit() {
    const uid = this.route.snapshot.paramMap.get('uid') || '';
    await this.getItem(uid ?? '', false);
  }

  /**
   * Provides data item with given UID
   * @param uid Unique identifier of a thing
   * @param _forceReload Ignore any cached data
   */
  async getItem(uid: SCUuid, _forceReload: boolean) {
    this.item = await (this.inputItem ??
      this.hebisDataProvider.hebisSearch({query: uid, page: 0}).then(
        result =>
          // eslint-disable-next-line unicorn/no-null
          (result.data && result.data[0]) || null,
      ));
  }
}
