/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {HebisDataProvider} from '../hebis-data.provider';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';
import {DataProvider} from '../../data/data.provider';
import {DataRoutingService} from '../../data/data-routing.service';
import {Router} from '@angular/router';

/**
 * HebisSearchPageComponent queries things and shows list of things as search results and filter as context menu
 */
@Component({
  selector: 'stapps-hebissearch-page',
  templateUrl: 'hebis-search-page.html',
  styleUrls: ['../../data/list/search-page.scss'],
  providers: [{provide: DataProvider, useClass: HebisDataProvider}],
})
export class HebisSearchPageComponent {
  constructor(dataRoutingService: DataRoutingService, router: Router) {
    dataRoutingService
      .itemSelectListener()
      .pipe(takeUntilDestroyed())
      .subscribe(async item => {
        void router.navigate(
          ['hebis-detail', (item.origin && 'originalId' in item.origin && item.origin['originalId']) || ''],
          {state: {item}},
        );
      });
  }
}
