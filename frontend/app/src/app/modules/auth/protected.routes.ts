/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {ActivatedRouteSnapshot, Data, Route} from '@angular/router';
import {SCAuthorizationProviderType} from '@openstapps/core';

export interface ProtectedRoute extends Route {
  data: {
    authProvider: SCAuthorizationProviderType;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
}

export class ActivatedProtectedRouteSnapshot extends ActivatedRouteSnapshot {
  data: Data & {authProvider: ProtectedRoute['data']['authProvider']};
}

export type ProtectedRoutes = ProtectedRoute[];
