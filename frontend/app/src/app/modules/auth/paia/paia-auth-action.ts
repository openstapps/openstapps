/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {PAIATokenResponse} from './paia-token-response';
import {AuthActionBuilder, IAuthAction} from 'ionic-appauth';

export interface IPAIAAuthAction extends IAuthAction {
  tokenResponse?: PAIATokenResponse;
}

export class PAIAAuthActionBuilder extends AuthActionBuilder {
  public static Init(): IPAIAAuthAction {
    return AuthActionBuilder.Init() as IPAIAAuthAction;
  }

  public static SignOutSuccess(): IPAIAAuthAction {
    return AuthActionBuilder.SignOutSuccess() as IPAIAAuthAction;
  }

  public static SignOutFailed(error: Error): IPAIAAuthAction {
    return AuthActionBuilder.SignOutFailed(error) as IPAIAAuthAction;
  }

  public static RefreshSuccess(tokenResponse: PAIATokenResponse): IPAIAAuthAction {
    return AuthActionBuilder.RefreshSuccess(tokenResponse) as IPAIAAuthAction;
  }

  public static RefreshFailed(error: Error): IPAIAAuthAction {
    return AuthActionBuilder.RefreshFailed(error) as IPAIAAuthAction;
  }

  public static SignInSuccess(tokenResponse: PAIATokenResponse): IPAIAAuthAction {
    return AuthActionBuilder.SignInSuccess(tokenResponse) as IPAIAAuthAction;
  }

  public static SignInFailed(error: Error): IPAIAAuthAction {
    return AuthActionBuilder.SignInFailed(error) as IPAIAAuthAction;
  }

  public static LoadTokenFromStorageSuccess(tokenResponse: PAIATokenResponse): IPAIAAuthAction {
    return AuthActionBuilder.LoadTokenFromStorageSuccess(tokenResponse) as IPAIAAuthAction;
  }

  public static LoadTokenFromStorageFailed(error: Error): IPAIAAuthAction {
    return AuthActionBuilder.LoadTokenFromStorageFailed(error) as IPAIAAuthAction;
  }

  public static RevokeTokensSuccess(): IPAIAAuthAction {
    return AuthActionBuilder.RevokeTokensSuccess() as IPAIAAuthAction;
  }

  public static RevokeTokensFailed(error: Error): IPAIAAuthAction {
    return AuthActionBuilder.RevokeTokensFailed(error) as IPAIAAuthAction;
  }

  public static LoadUserInfoSuccess(user: Error): IPAIAAuthAction {
    return AuthActionBuilder.LoadUserInfoSuccess(user) as IPAIAAuthAction;
  }

  public static LoadUserInfoFailed(error: Error): IPAIAAuthAction {
    return AuthActionBuilder.LoadUserInfoFailed(error) as IPAIAAuthAction;
  }
}
