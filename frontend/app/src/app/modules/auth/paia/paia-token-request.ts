/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StringMap} from '@openid/appauth';

// TODO: add documentation
export interface PAIATokenRequestJson {
  code: string;
  patron: string;
  extras?: StringMap;
}

export class PAIATokenRequest {
  code: string;

  patron: string;

  extras?: StringMap;

  constructor(request: PAIATokenRequestJson) {
    this.code = request.code;
    this.patron = request.patron;
    this.extras = request.extras;
  }

  /**
   * Serializes a TokenRequest to a JavaScript object.
   */
  toJson(): PAIATokenRequestJson {
    return {
      code: this.code,
      patron: this.patron,
      extras: this.extras,
    };
  }

  toStringMap(): StringMap {
    const map: StringMap = {
      patron: this.patron,
      code: this.code,
    };

    // copy over extras
    if (this.extras) {
      for (const extra in this.extras) {
        if (this.extras.hasOwnProperty(extra) && !map.hasOwnProperty(extra)) {
          // check before inserting to requestMap
          map[extra] = this.extras[extra];
        }
      }
    }
    return map;
  }
}
