/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {PAIAAuthorizationListener} from './paia-authorization-listener';
import {AuthorizationError, AuthorizationRequest} from '@openid/appauth';
import {PAIAAuthorizationResponse} from './paia-authorization-response';

export class PAIAAuthorizationNotifier {
  // eslint-disable-next-line unicorn/no-null
  private listener: PAIAAuthorizationListener | null = null;

  setAuthorizationListener(listener: PAIAAuthorizationListener) {
    this.listener = listener;
  }

  /**
   * The authorization complete callback.
   */
  onAuthorizationComplete(
    request: AuthorizationRequest,
    response: PAIAAuthorizationResponse | null,
    error: AuthorizationError | null,
  ): void {
    if (this.listener) {
      // complete authorization request
      this.listener(request, response, error);
    }
  }
}
