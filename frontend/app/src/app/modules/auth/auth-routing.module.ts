/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {authPaths} from './auth-paths';
import {AuthCallbackPageComponent} from './auth-callback/page/auth-callback-page.component';
import {DefaultAuthService} from './default-auth.service';
import {PAIAAuthService} from './paia/paia-auth.service';

const authRoutes: Routes = [
  {
    path: authPaths.default.redirect_path,
    component: AuthCallbackPageComponent,
  },
  {
    path: authPaths.paia.redirect_path,
    component: AuthCallbackPageComponent,
    providers: [{provide: DefaultAuthService, useExisting: PAIAAuthService}],
  },
];

/**
 * Module defining routes for auth module
 */
@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(authRoutes)],
})
export class AuthRoutingModule {}
