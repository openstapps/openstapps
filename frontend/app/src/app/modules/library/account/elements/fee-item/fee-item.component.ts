/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, Input} from '@angular/core';
import {PAIAFee} from '../../../types';
import {SCArticle, SCBook, SCPeriodical} from '@openstapps/core';
import {LibraryAccountService} from '../../library-account.service';

@Component({
  selector: 'stapps-fee-item',
  templateUrl: './fee-item.html',
  styleUrls: ['./fee-item.scss'],
})
export class FeeItemComponent {
  _fee: PAIAFee;

  book?: SCBook | SCPeriodical | SCArticle;

  hasEdition = false;

  @Input()
  set fee(fee: PAIAFee) {
    this.hasEdition = !fee.edition?.includes('null');
    this._fee = fee;
    if (this.hasEdition) {
      this.libraryAccountService.getDocumentFromHDS(fee.edition as string).then(book => {
        this.book = book;
      });
    }
  }

  get fee() {
    return this._fee;
  }

  propertiesToShow: (keyof PAIAFee)[] = ['about', 'date', 'amount'];

  constructor(private readonly libraryAccountService: LibraryAccountService) {}
}
