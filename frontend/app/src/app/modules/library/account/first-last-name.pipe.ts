import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'firstLastName',
})
export class FirstLastNamePipe implements PipeTransform {
  transform(value: string): unknown {
    return value.split(', ').reverse().join(' ');
  }
}
