/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ChangeDetectionStrategy, Component, HostBinding, Input, inject} from '@angular/core';
import {RouterLink} from '@angular/router';
import {ControlComponent, MapComponent, MarkerComponent} from '@maplibre/ngx-maplibre-gl';
import {AnimationController, IonicModule} from '@ionic/angular';
import {GeoNavigationDirective} from './geo-navigation.directive';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {MapAuto3dDirective} from './map-auto-3d.directive';
import {MediaQueryPipe} from 'src/app/util/media-query.pipe';
import {MapStyleDirective} from './map-style.directive';
import {MapSizeFixDirective} from './map-size-fix.directive';
import {SCThings} from '@openstapps/core';
import {ThingPolygonFeatureCollectionPipe, ThingPoiFeatureCollectionPipe} from './feature-collection.pipe';
import {PoiMarkersComponent} from './elements/poi-markers.component';
import {BuildingMarkersComponent} from './elements/building-markers.component';
import {ThingBoundsPipe} from './thing-bounds.pipe';
import {AttributionComponent} from './controls/attribution.component';
import {mapMaximizeAnimation} from './map-maximize-animation';

/**
 * The map widget (needs a container with explicit size)
 */
@Component({
  selector: 'stapps-map-widget',
  styleUrls: ['./map-widget.scss'],
  templateUrl: './map-widget.html',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    AttributionComponent,
    BuildingMarkersComponent,
    CommonModule,
    ControlComponent,
    GeoNavigationDirective,
    IonIconModule,
    IonicModule,
    MapAuto3dDirective,
    MapComponent,
    MapSizeFixDirective,
    MapStyleDirective,
    MarkerComponent,
    MediaQueryPipe,
    PoiMarkersComponent,
    RouterLink,
    ThingBoundsPipe,
    ThingPoiFeatureCollectionPipe,
    ThingPolygonFeatureCollectionPipe,
    TranslateModule,
  ],
})
export class MapWidgetComponent {
  @HostBinding('class.expand-when-space') expandWhenSpace = true;

  /**
   * A place to show on the map
   */
  @Input({required: true}) place: Extract<SCThings, {geo: object}>;

  maximizeAnimation = mapMaximizeAnimation(inject(AnimationController));
}
