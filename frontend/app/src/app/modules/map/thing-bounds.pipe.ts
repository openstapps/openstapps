import {Pipe, PipeTransform} from '@angular/core';
import {SCGeoInformation} from '@openstapps/core';

@Pipe({
  name: 'thingBounds',
  standalone: true,
  pure: true,
})
export class ThingBoundsPipe implements PipeTransform {
  transform(geo: SCGeoInformation): [[number, number], [number, number]] {
    if (geo.polygon) {
      const lngs = geo.polygon.coordinates[0].map(it => it[0]);
      const lats = geo.polygon.coordinates[0].map(it => it[1]);

      return [
        [Math.max(...lngs), Math.max(...lats)],
        [Math.min(...lngs), Math.min(...lats)],
      ];
    } else {
      return [geo.point.coordinates as [number, number], geo.point.coordinates as [number, number]];
    }
  }
}
