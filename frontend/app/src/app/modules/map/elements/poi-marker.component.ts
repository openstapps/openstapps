import {ChangeDetectionStrategy, Component, HostBinding, Input, OnInit, Optional} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {IonIconModule} from 'src/app/util/ion-icon/ion-icon.module';
import {MapIconDirective} from '../map-icon.directive';
import {Feature, Point} from 'geojson';
import {SCFeatureProperties} from '../feature-collection.pipe';
import {MapDataProvider} from '../map-data.provider';
import {DataRoutingService} from '../../data/data-routing.service';
import {AddWordBreakOpportunitiesPipe} from '../../../util/word-break-opportunities.pipe';

@Component({
  selector: 'stapps-poi-marker',
  templateUrl: './poi-marker.html',
  styleUrl: './poi-marker.scss',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [IonicModule, IonIconModule, MapIconDirective, AddWordBreakOpportunitiesPipe],
})
export class PoiMarkerComponent implements OnInit {
  @Input({required: true}) feature: Feature<Point, SCFeatureProperties>;

  @HostBinding('disabled') disabled = this.dataProvider === null;

  fontSize = 0;

  constructor(
    @Optional() readonly dataProvider: MapDataProvider | null,
    readonly dataRoutingService: DataRoutingService,
  ) {}

  async featureClick() {
    if (this.dataProvider === null) return;

    const item = this.dataProvider.current.value?.data.find(it => it.uid === this.feature.properties.uid);
    if (item === undefined) return;

    this.dataRoutingService.emitChildEvent(item);
  }

  ngOnInit() {
    this.fontSize = Math.max(10, 12 - Math.max(0, this.feature.properties.name.length - 16));
  }
}
