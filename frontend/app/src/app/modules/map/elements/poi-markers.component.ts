import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {MapIconDirective} from '../map-icon.directive';
import {FeatureCollection, Point} from 'geojson';
import {SCFeatureProperties} from '../feature-collection.pipe';
import {animate, style, transition, trigger} from '@angular/animations';
import {MglClusterLeavesPipe} from '../cluster-leaves.pipe';
import {
  ClusterPointDirective,
  GeoJSONSourceComponent,
  MarkersForClustersComponent,
  PointDirective,
} from '@maplibre/ngx-maplibre-gl';
import {AsyncPipe} from '@angular/common';
import {PoiMarkerComponent} from './poi-marker.component';

@Component({
  selector: 'stapps-poi-markers',
  templateUrl: './poi-markers.html',
  styleUrl: './poi-markers.scss',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MapIconDirective,
    MglClusterLeavesPipe,
    GeoJSONSourceComponent,
    MarkersForClustersComponent,
    AsyncPipe,
    ClusterPointDirective,
    PointDirective,
    PoiMarkerComponent,
  ],
  animations: [
    trigger('fade', [
      transition(':enter', [style({opacity: 0}), animate('200ms', style({opacity: 1}))]),
      transition(':leave', [style({opacity: 1}), animate('200ms', style({opacity: 0}))]),
    ]),
  ],
})
export class PoiMarkersComponent {
  @Input({required: true}) data: FeatureCollection<Point, SCFeatureProperties>;

  @Input() clusterPreviewCount = 3;
}
