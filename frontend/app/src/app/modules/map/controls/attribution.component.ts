import {animate, style, transition, trigger} from '@angular/animations';
import {AsyncPipe} from '@angular/common';
import {ChangeDetectionStrategy, Component, Input, inject} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {MapService} from '@maplibre/ngx-maplibre-gl';
import {map, delay, Subject, race, mergeWith} from 'rxjs';
import {IonIconModule} from 'src/app/util/ion-icon/ion-icon.module';

@Component({
  selector: 'stapps-map-attribution',
  templateUrl: './attribution.html',
  styleUrl: './attribution.scss',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [IonicModule, IonIconModule, AsyncPipe],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({opacity: 0, scale: '0.8 1'}),
        animate('0.2s ease', style({opacity: 1, scale: 1})),
      ]),
      transition(':leave', [
        style({opacity: 1, scale: 1}),
        animate('0.2s ease', style({opacity: 0, scale: '0.8 1'})),
      ]),
    ]),
  ],
})
export class AttributionComponent {
  @Input() direction: 'left' | 'right' = 'right';

  manualVisible = new Subject<void>();

  hideAttribution = race(
    this.manualVisible,
    inject(MapService).mapCreated$.pipe(delay(5000), mergeWith(this.manualVisible)),
  ).pipe(map((_, i) => i % 2 === 0));
}
