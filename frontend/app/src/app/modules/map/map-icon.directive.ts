import {SCThings, SCPlace} from '@openstapps/core';
import {SCIcon} from '../../util/ion-icon/icon';
import {Pipe, PipeTransform} from '@angular/core';
import {MaterialSymbol} from 'material-symbols';

const mapIcons: Record<Extract<SCThings, SCPlace>['categories'][number], MaterialSymbol> = {
  'cafe': SCIcon.local_cafe,
  'learn': SCIcon.school,
  'canteen': SCIcon.restaurant,
  'computer': SCIcon.computer,
  'education': SCIcon.school,
  'laboratory': SCIcon.science,
  'library': SCIcon.local_library,
  'lounge': SCIcon.weekend,
  'office': SCIcon.meeting_room,
  'restaurant': SCIcon.restaurant,
  'restroom': SCIcon.wc,
  'student canteen': SCIcon.restaurant,
  'student union': SCIcon.groups,
  'validator': SCIcon.badge,
  'card charger': SCIcon.credit_card,
  'printer': SCIcon.print,
  'disabled access': SCIcon.accessible,
};

const defaultIcon = SCIcon.not_listed_location;

@Pipe({
  name: 'stappsMapIcon',
  standalone: true,
  pure: true,
})
export class MapIconDirective implements PipeTransform {
  transform(value: keyof typeof mapIcons | string | undefined): MaterialSymbol {
    return mapIcons[value as keyof typeof mapIcons] ?? defaultIcon;
  }
}
