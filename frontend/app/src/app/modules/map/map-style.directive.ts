import {Directive, Input, Host} from '@angular/core';
import {MapComponent} from '@maplibre/ngx-maplibre-gl';
import {environment} from '../../../environments/environment';

@Directive({
  selector: 'mgl-map[styleName]',
  standalone: true,
})
export class MapStyleDirective {
  constructor(@Host() readonly map: MapComponent) {}

  @Input()
  set styleName(name: string) {
    const style = `${environment.backend_url}/_static/map/styles/${name}/style.json`;
    if (this.map.style) {
      this.map.mapInstance.setStyle(style);
    } else {
      this.map.style = style;
    }
  }
}
