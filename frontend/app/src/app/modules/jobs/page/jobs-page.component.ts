import {Component} from '@angular/core';
import {SCSearchFilter, SCThingType} from '@openstapps/core';

@Component({
  selector: 'stapps-jobs-page',
  templateUrl: 'jobs-page.html',
  styleUrls: ['jobs-page.scss'],
})
export class JobsPageComponent {
  forcedFilter: SCSearchFilter = {
    type: 'value',
    arguments: {
      field: 'type',
      value: SCThingType.JobPosting,
    },
  };
}
