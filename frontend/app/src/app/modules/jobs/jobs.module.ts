import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MomentModule} from 'ngx-moment';
import {DataModule} from '../data/data.module';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {RouterModule, Routes} from '@angular/router';
import {JobsPageComponent} from './page/jobs-page.component';

const jobsRoutes: Routes = [{path: 'jobs', component: JobsPageComponent}];

@NgModule({
  declarations: [JobsPageComponent],
  imports: [
    IonicModule.forRoot(),
    ThingTranslateModule.forChild(),
    TranslateModule.forChild(),
    RouterModule.forChild(jobsRoutes),
    IonIconModule,
    CommonModule,
    MomentModule,
    DataModule,
    UtilModule,
  ],
})
export class JobModule {}
