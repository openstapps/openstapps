import {Component, Input} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'stapps-share-button',
  templateUrl: 'share-button.html',
  styleUrls: ['share-button.scss'],
})
export class ShareButtonComponent {
  canShare = false;

  @Input({required: true}) title: string;

  @Input() url: string;

  constructor(readonly toastController: ToastController) {}

  share(): boolean {
    const url = this.url ?? new URL(window.location.pathname, `https://${environment.app_host}`);

    if (navigator.share) {
      void navigator.share({
        url: url.toString(),
        text: this.title,
      });
      return false;
    } else {
      void navigator.clipboard.writeText(`${this.title}\n${url}`);
      return true;
    }
  }
}
