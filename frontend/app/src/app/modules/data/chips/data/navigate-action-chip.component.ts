/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCPlaceWithoutReferences, SCThings} from '@openstapps/core';

@Component({
  selector: 'stapps-navigate-action-chip',
  templateUrl: 'navigate-action-chip.html',
  styleUrls: ['navigate-action-chip.scss'],
})
export class NavigateActionChipComponent {
  place: SCPlaceWithoutReferences;

  @Input({required: true}) set item(value: SCThings) {
    if ('geo' in value) {
      this.place = value;
    } else if ('inPlace' in value && value.inPlace && 'geo' in value.inPlace) {
      this.place = value.inPlace;
    } else {
      console.error('Invalid place', value);
    }
  }
}
