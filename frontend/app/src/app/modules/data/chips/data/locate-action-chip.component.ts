/* eslint-disable class-methods-use-this */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCThing} from '@openstapps/core';

/**
 * Shows a horizontal list of action chips
 */
@Component({
  selector: 'stapps-locate-action-chip',
  templateUrl: 'locate-action-chip.html',
})
export class LocateActionChipComponent {
  /**
   * Item
   */
  @Input() item: SCThing;

  /**
   * Click
   */
  onClick(/*event: MouseEvent*/) {
    // TODO
  }
}
