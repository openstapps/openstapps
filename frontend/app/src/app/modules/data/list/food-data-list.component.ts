/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {PositionService} from '../../map/position.service';
import {Geolocation} from '@capacitor/geolocation';
import {BehaviorSubject, catchError} from 'rxjs';
import {pauseWhen} from '../../../util/rxjs/pause-when';
import {SCSearchFilter} from '@openstapps/core';
import {ContextMenuService} from '../../menu/context/context-menu.service';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';

/**
 * Presents a list of places for eating/drinking
 */
@Component({
  templateUrl: 'food-data-list.html',
})
export class FoodDataListComponent {
  isNotInView$ = new BehaviorSubject(true);

  forcedFilter: SCSearchFilter = {
    arguments: {
      filters: [
        {
          arguments: {
            field: 'categories',
            value: 'canteen',
          },
          type: 'value',
        },
        {
          arguments: {
            field: 'categories',
            value: 'student canteen',
          },
          type: 'value',
        },
        {
          arguments: {
            field: 'categories',
            value: 'cafe',
          },
          type: 'value',
        },
        {
          arguments: {
            field: 'categories',
            value: 'restaurant',
          },
          type: 'value',
        },
      ],
      operation: 'or',
    },
    type: 'boolean',
  };

  constructor(positionService: PositionService, contextMenuService: ContextMenuService) {
    positionService
      .watchCurrentLocation({enableHighAccuracy: false, maximumAge: 1000})
      .pipe(
        pauseWhen(this.isNotInView$),
        takeUntilDestroyed(),
        catchError(async _error => {
          await Geolocation.checkPermissions();
        }),
      )
      .subscribe({
        next(position) {
          if (!position) return;
          positionService.position = position;
          contextMenuService.sortQuery.next([
            {
              type: 'distance',
              order: 'asc',
              arguments: {
                field: 'geo',
                position: [position.longitude, position.latitude],
              },
            },
          ]);
        },
        async error() {
          positionService.position = undefined;
          await Geolocation.checkPermissions();
        },
      });
  }

  ionViewWillEnter() {
    this.isNotInView$.next(false);
  }

  ionViewWillLeave() {
    this.isNotInView$.next(true);
  }
}
