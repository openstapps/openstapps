/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Directive, forwardRef, Input, Output, ViewChild} from '@angular/core';
import {CdkVirtualForOf, VIRTUAL_SCROLL_STRATEGY} from '@angular/cdk/scrolling';
import {ScThingListItemVirtualScrollStrategy} from './sc-thing-list-item-virtual-scroll-strategy';

/**
 *
 */
function factory(directive: SCThingListItemVirtualScrollStrategyDirective) {
  return directive.scrollStrategy;
}

@Directive({
  selector: 'cdk-virtual-scroll-viewport[scThingListItemVirtualScrollStrategy]',
  providers: [
    {
      provide: VIRTUAL_SCROLL_STRATEGY,
      useFactory: factory,
      deps: [forwardRef(() => SCThingListItemVirtualScrollStrategyDirective)],
    },
  ],
})
export class SCThingListItemVirtualScrollStrategyDirective {
  scrollStrategy = new ScThingListItemVirtualScrollStrategy();

  @ViewChild(CdkVirtualForOf) virtualForOf: CdkVirtualForOf<unknown>;

  @Output() readonly loadMore = this.scrollStrategy.loadMore;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() set trackGroupBy(value: (item: any) => unknown) {
    this.scrollStrategy.trackGroupBy = value;
  }

  @Input() set minimumHeight(value: number) {
    this.scrollStrategy.approximateItemHeight = value;
  }

  @Input() set buffer(value: number) {
    this.scrollStrategy.buffer = value;
  }

  @Input() set gap(value: number) {
    this.scrollStrategy.gap = value;
  }

  @Input() set itemRenderTimeout(value: number) {
    this.scrollStrategy.itemRenderTimeout = value;
  }
}
