import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DataListComponent} from './data-list.component';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ConfigProvider} from '../../config/config.provider';

describe('DataListComponent', () => {
  let component: DataListComponent;
  let fixture: ComponentFixture<DataListComponent>;
  let configProviderMock: jasmine.SpyObj<ConfigProvider>;

  beforeEach(() => {
    configProviderMock = jasmine.createSpyObj('ConfigProvider', {
      getValue: () => {
        return {lat: 123, lng: 123};
      },
    });
    TestBed.configureTestingModule({
      declarations: [DataListComponent],
      imports: [TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: ConfigProvider, useValue: configProviderMock}],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
