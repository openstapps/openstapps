/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingType} from '@openstapps/core';
import {SCIcon} from '../../util/ion-icon/icon';
import {MaterialSymbol} from 'material-symbols';

export const DataIcons = {
  'academic event': SCIcon.school,
  'assessment': SCIcon.fact_check,
  'article': SCIcon.article,
  'book': SCIcon.book,
  'building': SCIcon.location_city,
  'certification': SCIcon.contract,
  'catalog': SCIcon.inventory_2,
  'contact point': SCIcon.contact_page,
  'course of study': SCIcon.school,
  'date series': SCIcon.event,
  'dish': SCIcon.lunch_dining,
  'favorite': SCIcon.favorite,
  'floor': SCIcon.foundation,
  'id card': SCIcon.badge,
  'message': SCIcon.newspaper,
  'organization': SCIcon.business_center,
  'periodical': SCIcon.feed,
  'person': SCIcon.person,
  'point of interest': SCIcon.pin_drop,
  'publication event': SCIcon.campaign,
  'room': SCIcon.meeting_room,
  'semester': SCIcon.date_range,
  'setting': SCIcon.settings,
  'sport course': SCIcon.sports_soccer,
  'study module': SCIcon.view_module,
  'ticket': SCIcon.confirmation_number,
  'todo': SCIcon.task,
  'tour': SCIcon.tour,
  'video': SCIcon.movie,
  'diff': SCIcon.difference,
  'job posting': SCIcon.work,
} satisfies Record<SCThingType, MaterialSymbol>;
