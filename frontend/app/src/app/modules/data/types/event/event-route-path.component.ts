/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCThingWithoutReferences} from '@openstapps/core';
import {Observable} from 'rxjs';

/**
 * This was originally intended to be used in more than one place.
 * I kept this in place to make it easier to adapt it in the future, if needed.
 */
@Component({
  selector: 'event-route-path',
  templateUrl: 'event-route-path.html',
  styleUrls: ['event-route-path.scss'],
})
export class EventRoutePathComponent {
  @Input() maxItems?: number;

  @Input() itemsAfterCollapse?: number;

  @Input() itemsBeforeCollapse?: number;

  @Input() showSelfInPopover = false;

  @Input() items: Array<SCThingWithoutReferences | undefined> = [];

  @Input() more?: Observable<SCThingWithoutReferences[]>;

  @Input() moreAnchor: 'start' | 'end' = 'start';
}
