/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input, OnInit} from '@angular/core';
import {SCCatalog, SCThings} from '@openstapps/core';
import {DataProvider} from '../../data.provider';

@Component({
  selector: 'stapps-catalog-detail-content',
  templateUrl: 'catalog-detail-content.html',
  styleUrls: ['catalog-detail-content.scss'],
})
export class CatalogDetailContentComponent implements OnInit {
  /**
   * SCCatalog to display
   */
  @Input() item: SCCatalog;

  items: Promise<SCThings[]>;

  constructor(private dataProvider: DataProvider) {}

  async ngOnInit() {
    this.items = this.dataProvider
      .search({
        size: 100,
        sort: [
          {
            arguments: {field: 'type'},
            order: 'desc',
            type: 'ducet',
          },
          {
            arguments: {field: 'name'},
            order: 'asc',
            type: 'ducet',
          },
        ],
        filter: {
          arguments: {
            filters: [
              {
                arguments: {
                  operation: 'and',
                  filters: [
                    {
                      type: 'value',
                      arguments: {
                        field: 'type',
                        value: 'catalog',
                      },
                    },
                    {
                      type: 'value',
                      arguments: {
                        field: 'superCatalog.uid',
                        value: this.item.uid,
                      },
                    },
                  ],
                },
                type: 'boolean',
              },
              {
                arguments: {
                  operation: 'and',
                  filters: [
                    {
                      type: 'value',
                      arguments: {
                        field: 'type',
                        value: 'academic event',
                      },
                    },
                    {
                      type: 'value',
                      arguments: {
                        field: 'catalogs.uid',
                        value: this.item.uid,
                      },
                    },
                  ],
                },
                type: 'boolean',
              },
            ],
            operation: 'or',
          },
          type: 'boolean',
        },
      })
      .then(({data}) => data);
  }
}
