/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {IonicModule, Platform} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MarkdownModule} from 'ngx-markdown';
import {MomentModule} from 'ngx-moment';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {SimpleBrowser, browserFactory} from '../../util/browser.factory';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {RoutingStackService} from '../../util/routing-stack.service';
import {UtilModule} from '../../util/util.module';
import {CalendarService} from '../calendar/calendar.service';
import {ScheduleProvider} from '../calendar/schedule.provider';
import {GeoNavigationDirective} from '../map/geo-navigation.directive';
import {MapWidgetComponent} from '../map/map-widget.component';
import {MenuModule} from '../menu/menu.module';
import {SettingsProvider} from '../settings/settings.provider';
import {StorageModule} from '../storage/storage.module';
import {ActionChipListComponent} from './chips/action-chip-list.component';
import {AddEventActionChipComponent} from './chips/data/add-event-action-chip.component';
import {LocateActionChipComponent} from './chips/data/locate-action-chip.component';
import {NavigateActionChipComponent} from './chips/data/navigate-action-chip.component';
import {EditEventSelectionComponent} from './chips/edit-event-selection.component';
import {CoordinatedSearchProvider} from './coordinated-search.provider';
import {DataFacetsProvider} from './data-facets.provider';
import {DataIconPipe} from './data-icon.pipe';
import {DataRoutingModule} from './data-routing.module';
import {DataProvider} from './data.provider';
import {DataDetailContentComponent} from './detail/data-detail-content.component';
import {DataDetailComponent} from './detail/data-detail.component';
import {DataPathComponent} from './detail/data-path.component';
import {AddressDetailComponent} from './elements/address-detail.component';
import {CertificationsInDetailComponent} from './elements/certifications-in-detail.component';
import {ExternalLinkComponent} from './elements/external-link.component';
import {FavoriteButtonComponent} from './elements/favorite-button.component';
import {LongInlineTextComponent} from './elements/long-inline-text.component';
import {OffersDetailComponent} from './elements/offers-detail.component';
import {OffersInListComponent} from './elements/offers-in-list.component';
import {OriginDetailComponent} from './elements/origin-detail.component';
import {OriginInListComponent} from './elements/origin-in-list.component';
import {StappsRatingComponent} from './elements/rating.component';
import {SimpleCardComponent} from './elements/simple-card.component';
import {SkeletonListItemComponent} from './elements/skeleton-list-item.component';
import {SkeletonSegmentComponent} from './elements/skeleton-segment-button.component';
import {SkeletonSimpleCardComponent} from './elements/skeleton-simple-card.component';
import {TitleCardComponent} from './elements/title-card.component';
import {DataListItemHostDefaultComponent} from './list/data-list-item-host-default.component';
import {DataListItemHostDirective} from './list/data-list-item-host.directive';
import {DataListItemComponent} from './list/data-list-item.component';
import {DataListComponent} from './list/data-list.component';
import {FoodDataListComponent} from './list/food-data-list.component';
import {SearchPageComponent} from './list/search-page.component';
import {SimpleDataListComponent} from './list/simple-data-list.component';
import {SkeletonListComponent} from './list/skeleton-list.component';
import {TreeListFragmentComponent} from './list/tree-list-fragment.component';
import {TreeListComponent} from './list/tree-list.component';
import {StAppsWebHttpClient} from './stapps-web-http-client.provider';
import {ArticleDetailContentComponent} from './types/article/article-detail-content.component';
import {ArticleListItemComponent} from './types/article/article-item.component';
import {BookDetailContentComponent} from './types/book/book-detail-content.component';
import {BookListItemComponent} from './types/book/book-list-item.component';
import {CatalogDetailContentComponent} from './types/catalog/catalog-detail-content.component';
import {CatalogListItemComponent} from './types/catalog/catalog-list-item.component';
import {DateSeriesDetailContentComponent} from './types/date-series/date-series-detail-content.component';
import {DateSeriesListItemComponent} from './types/date-series/date-series-list-item.component';
import {DishCharacteristicsComponent} from './types/dish/dish-characteristics.component';
import {DishDetailContentComponent} from './types/dish/dish-detail-content.component';
import {DishListItemComponent} from './types/dish/dish-list-item.component';
import {EventDetailContentComponent} from './types/event/event-detail-content.component';
import {EventListItemComponent} from './types/event/event-list-item.component';
import {EventRoutePathComponent} from './types/event/event-route-path.component';
import {FavoriteDetailContentComponent} from './types/favorite/favorite-detail-content.component';
import {FavoriteListItemComponent} from './types/favorite/favorite-list-item.component';
import {JobPostingDetailContentComponent} from './types/job-posting/job-posting-detail-content.component';
import {JobPostingListItemComponent} from './types/job-posting/job-posting-list-item.component';
import {MessageDetailContentComponent} from './types/message/message-detail-content.component';
import {MessageListItemComponent} from './types/message/message-list-item.component';
import {OrganizationDetailContentComponent} from './types/organization/organization-detail-content.component';
import {OrganizationListItemComponent} from './types/organization/organization-list-item.component';
import {PeriodicalDetailContentComponent} from './types/periodical/periodical-detail-content.component';
import {PeriodicalListItemComponent} from './types/periodical/periodical-list-item.component';
import {PersonDetailContentComponent} from './types/person/person-detail-content.component';
import {PersonListItemComponent} from './types/person/person-list-item.component';
import {PlaceDetailContentComponent} from './types/place/place-detail-content.component';
import {PlaceListItemComponent} from './types/place/place-list-item.component';
import {PlaceMensaDetailComponent} from './types/place/special/mensa/place-mensa-detail.component';
import {SemesterDetailContentComponent} from './types/semester/semester-detail-content.component';
import {SemesterListItemComponent} from './types/semester/semester-list-item.component';
import {VideoDetailContentComponent} from './types/video/video-detail-content.component';
import {VideoListItemComponent} from './types/video/video-list-item.component';
import {ShareButtonComponent} from './elements/share-button.component';

/**
 * Module for handling data
 */
@NgModule({
  declarations: [
    ActionChipListComponent,
    AddEventActionChipComponent,
    NavigateActionChipComponent,
    EditEventSelectionComponent,
    AddressDetailComponent,
    CatalogDetailContentComponent,
    CatalogListItemComponent,
    CertificationsInDetailComponent,
    DishCharacteristicsComponent,
    DataDetailComponent,
    DataDetailContentComponent,
    DataIconPipe,
    DataListComponent,
    DataListItemComponent,
    DataPathComponent,
    EventRoutePathComponent,
    DateSeriesDetailContentComponent,
    DateSeriesListItemComponent,
    DishDetailContentComponent,
    DishListItemComponent,
    EventDetailContentComponent,
    EventListItemComponent,
    FavoriteButtonComponent,
    FavoriteDetailContentComponent,
    SkeletonListComponent,
    FavoriteListItemComponent,
    FoodDataListComponent,
    LocateActionChipComponent,
    LongInlineTextComponent,
    MessageDetailContentComponent,
    MessageListItemComponent,
    JobPostingDetailContentComponent,
    JobPostingListItemComponent,
    OffersDetailComponent,
    OffersInListComponent,
    OrganizationDetailContentComponent,
    OrganizationListItemComponent,
    OriginDetailComponent,
    OriginInListComponent,
    PersonDetailContentComponent,
    PersonListItemComponent,
    PlaceDetailContentComponent,
    PlaceListItemComponent,
    PlaceMensaDetailComponent,
    SearchPageComponent,
    SemesterDetailContentComponent,
    SemesterListItemComponent,
    DataListItemHostDirective,
    DataListItemHostDefaultComponent,
    SimpleCardComponent,
    SkeletonListItemComponent,
    SkeletonSegmentComponent,
    StappsRatingComponent,
    SkeletonSimpleCardComponent,
    TreeListComponent,
    TreeListFragmentComponent,
    VideoDetailContentComponent,
    VideoListItemComponent,
    SimpleDataListComponent,
    TitleCardComponent,
    ExternalLinkComponent,
    ArticleListItemComponent,
    ArticleDetailContentComponent,
    BookListItemComponent,
    BookDetailContentComponent,
    PeriodicalListItemComponent,
    PeriodicalDetailContentComponent,
    ShareButtonComponent,
  ],
  imports: [
    CommonModule,
    DataRoutingModule,
    FormsModule,
    MapWidgetComponent,
    HttpClientModule,
    IonicModule.forRoot(),
    MarkdownModule.forRoot(),
    MenuModule,
    IonIconModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        m: 59,
      },
    }),
    ScrollingModule,
    StorageModule,
    TranslateModule.forChild(),
    ThingTranslateModule.forChild(),
    UtilModule,
    GeoNavigationDirective,
  ],
  providers: [
    CoordinatedSearchProvider,
    DataProvider,
    DataFacetsProvider,
    Geolocation,
    ScheduleProvider,
    StAppsWebHttpClient,
    CalendarService,
    RoutingStackService,
    SettingsProvider,
    {
      provide: SimpleBrowser,
      useFactory: browserFactory,
      deps: [Platform],
    },
  ],
  exports: [
    DataDetailComponent,
    DataDetailContentComponent,
    DataIconPipe,
    DataListComponent,
    DataListItemComponent,
    DateSeriesListItemComponent,
    PlaceListItemComponent,
    SimpleCardComponent,
    SkeletonListItemComponent,
    SkeletonSimpleCardComponent,
    SearchPageComponent,
    SimpleDataListComponent,
    OriginDetailComponent,
    FavoriteButtonComponent,
    TreeListComponent,
    ExternalLinkComponent,
    ArticleDetailContentComponent,
    BookDetailContentComponent,
    PeriodicalDetailContentComponent,
    TitleCardComponent,
  ],
})
export class DataModule {}
