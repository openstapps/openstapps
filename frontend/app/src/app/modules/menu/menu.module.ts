/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {LayoutModule} from '@angular/cdk/layout';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {SettingsModule} from '../settings/settings.module';
import {ContextMenuComponent} from './context/context-menu.component';
import {ContextMenuService} from './context/context-menu.service';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

/**
 * Menu module
 */
@NgModule({
  declarations: [ContextMenuComponent],
  exports: [ContextMenuComponent],
  imports: [
    CommonModule,
    IonIconModule,
    FormsModule,
    IonicModule.forRoot(),
    RouterModule,
    SettingsModule,
    TranslateModule.forChild(),
    LayoutModule,
  ],
  providers: [ContextMenuService],
})
export class MenuModule {}
