/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, ElementRef, HostBinding, ViewChild} from '@angular/core';
import {InternetConnectionService} from '../../../util/internet-connection.service';
import {Router} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';

@Component({
  selector: 'stapps-offline-notice',
  templateUrl: 'offline-notice.html',
  styleUrls: ['offline-notice.scss'],
})
export class OfflineNoticeComponent {
  @HostBinding('class.is-offline') isOffline = false;

  @HostBinding('class.has-error') hasError = false;

  @ViewChild('spinIcon', {read: ElementRef}) spinIcon: ElementRef;

  constructor(
    readonly offlineProvider: InternetConnectionService,
    readonly router: Router,
    readonly logger: NGXLogger,
  ) {
    this.offlineProvider.offline$.pipe(takeUntilDestroyed()).subscribe(isOffline => {
      this.isOffline = isOffline;
    });
    this.offlineProvider.error$.pipe(takeUntilDestroyed()).subscribe(hasError => {
      this.hasError = hasError;
    });
  }

  retry() {
    this.spinIcon.nativeElement.classList.remove('spin');
    this.spinIcon.nativeElement.offsetWidth;
    this.spinIcon.nativeElement.classList.add('spin');
    this.offlineProvider.retry();
  }
}
