/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {TabsComponent} from './tabs.component';
import {ConfigProvider} from '../../config/config.provider';
import {sampleAuthConfiguration} from '../../../_helpers/data/sample-configuration';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {NGXLogger} from 'ngx-logger';
import {Platform} from '@ionic/angular';
import {ThingTranslateService} from '../../../translation/thing-translate.service';
import {SettingsProvider} from '../../settings/settings.provider';
import {ScheduleSyncService} from '../../background/schedule/schedule-sync.service';
import {StorageProvider} from '../../storage/storage.provider';

describe('Tabs', () => {
  let platformReadySpy: any;
  let platformSpy: jasmine.SpyObj<Platform>;
  let translateServiceSpy: jasmine.SpyObj<TranslateService>;
  let thingTranslateServiceSpy: jasmine.SpyObj<ThingTranslateService>;
  let settingsProvider: jasmine.SpyObj<SettingsProvider>;
  let configProvider: jasmine.SpyObj<ConfigProvider>;
  let ngxLogger: jasmine.SpyObj<NGXLogger>;
  let scheduleSyncServiceSpy: jasmine.SpyObj<ScheduleSyncService>;
  let platformIsSpy;
  let storageProvider: jasmine.SpyObj<StorageProvider>;
  beforeEach(waitForAsync(() => {
    platformReadySpy = Promise.resolve();
    platformIsSpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', {
      ready: platformReadySpy,
      is: platformIsSpy,
    });
    translateServiceSpy = jasmine.createSpyObj('TranslateService', ['setDefaultLang', 'use']);
    thingTranslateServiceSpy = jasmine.createSpyObj('ThingTranslateService', ['init']);
    settingsProvider = jasmine.createSpyObj('SettingsProvider', [
      'getSettingValue',
      'provideSetting',
      'setCategoriesOrder',
    ]);
    scheduleSyncServiceSpy = jasmine.createSpyObj('ScheduleSyncService', [
      'getDifferences',
      'postDifferencesNotification',
    ]);
    configProvider = jasmine.createSpyObj('ConfigProvider', ['init', 'getAnyValue']);
    configProvider.getAnyValue = jasmine.createSpy().and.callFake(function () {
      return sampleAuthConfiguration;
    });
    ngxLogger = jasmine.createSpyObj('NGXLogger', ['log', 'error', 'warn']);
    storageProvider = jasmine.createSpyObj('StorageProvider', ['init', 'get', 'has', 'put']);

    TestBed.configureTestingModule({
      declarations: [TabsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        {provide: Platform, useValue: platformSpy},
        {provide: TranslateService, useValue: translateServiceSpy},
        {provide: ThingTranslateService, useValue: thingTranslateServiceSpy},
        {provide: ScheduleSyncService, useValue: scheduleSyncServiceSpy},
        {provide: SettingsProvider, useValue: settingsProvider},
        {provide: ConfigProvider, useValue: configProvider},
        {provide: NGXLogger, useValue: ngxLogger},
        {provide: StorageProvider, useValue: storageProvider},
      ],
    }).compileComponents();
  }));

  it('should create the tabs page', () => {
    const fixture = TestBed.createComponent(TabsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
