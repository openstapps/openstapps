/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import type {AnimationBuilder} from '@ionic/angular';
import {AnimationController} from '@ionic/angular';
import type {AnimationOptions} from '@ionic/angular/common/providers/nav-controller';
import {iosDuration, iosEasing, mdDuration, mdEasing} from 'src/app/animation/easings';

/**
 *
 */
export function tabsTransition(animationController: AnimationController): AnimationBuilder {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (_baseElement: HTMLElement, options: AnimationOptions | any) => {
    const rootTransition = animationController
      .create()
      .duration(options.duration ?? (options.mode === 'ios' ? iosDuration : mdDuration * 1.4))
      .easing(options.mode === 'ios' ? iosEasing : mdEasing);

    const exitZIndex = animationController.create().fromTo('opacity', '1', '0').addElement(options.leavingEl);
    const contentEnter = animationController
      .create()
      .fromTo('transform', 'scale(1.05)', 'scale(1)')
      .fromTo('opacity', '0', '1')
      .addElement(options.enteringEl);

    rootTransition.addAnimation([contentEnter, exitZIndex]);
    return rootTransition;
  };
}
