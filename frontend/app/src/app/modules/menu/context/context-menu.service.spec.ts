/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {TestBed} from '@angular/core/testing';

import {ContextMenuService} from './context-menu.service';
import {SCFacet} from '@openstapps/core';
import {FilterContext, SortContext} from './context-type';
import {ThingTranslateModule} from '../../../translation/thing-translate.module';
import {TranslateModule} from '@ngx-translate/core';

describe('ContextMenuService', () => {
  let service: ContextMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ThingTranslateModule.forRoot(), TranslateModule.forRoot()],
      providers: [ContextMenuService],
    });
    service = TestBed.inject(ContextMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update filterOptions', done => {
    service.filterContextChanged$.subscribe(result => {
      expect(result).toBeDefined();
      done();
    });
    service.updateContextFilter(facetsMock);
  });

  it('should update filterQuery', done => {
    service.filterContextChanged$.subscribe(result => {
      expect(result).toBeDefined();
      expect(service.contextFilter.options[0].buckets.length).toEqual(
        filterContext.options[0].buckets.length,
      );
      done();
    });
    service.updateContextFilter(facetsMock);
  });

  it('should update sortOptions', done => {
    service.sortContextChanged$.subscribe(result => {
      expect(result).toBeDefined();
      done();
    });
    service.setContextSort(sortContext);
  });

  it('should update sortQuery', done => {
    service.sortContextChanged$.subscribe(result => {
      expect(result).toBeDefined();
      done();
    });
    service.setContextSort(sortContext);
  });
});

const facetsMock: SCFacet[] = [
  {
    buckets: [
      {
        count: 60,
        key: 'academic event',
      },
      {
        count: 160,
        key: 'message',
      },
      {
        count: 151,
        key: 'date series',
      },
      {
        count: 106,
        key: 'dish',
      },
      {
        count: 20,
        key: 'building',
      },
      {
        count: 20,
        key: 'semester',
      },
    ],
    field: 'type',
  },
];

const filterContext: FilterContext = {
  name: 'filter',
  options: [
    {
      buckets: [
        {
          checked: true,
          count: 60,
          key: 'academic event',
        },
        {
          checked: false,
          count: 160,
          key: 'message',
        },
        {
          checked: false,
          count: 151,
          key: 'date series',
        },
        {
          checked: false,
          count: 106,
          key: 'dish',
        },
        {
          checked: false,
          count: 20,
          key: 'building',
        },
        {
          checked: false,
          count: 20,
          key: 'semester',
        },
      ],
      field: 'type',
      info: {
        field: 'type',
        sortOrder: 0,
      },
    },
  ],
};

const sortContext: SortContext = {
  name: 'sort',
  reversed: false,
  value: 'name',
  values: [
    {
      reversible: false,
      value: 'relevance',
    },
    {
      reversible: true,
      value: 'name',
    },
    {
      reversible: true,
      value: 'type',
    },
  ],
};
