/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ChangeDetectionStrategy, Component} from '@angular/core';
import {IdCardsProvider} from './id-cards.provider';
import {SCIdCard} from '@openstapps/core';
import {IonicModule} from '@ionic/angular';
import {AsyncPipe, TitleCasePipe} from '@angular/common';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {UtilModule} from '../../util/util.module';
import {IdCardComponent} from './id-card.component';
import {TranslateModule} from '@ngx-translate/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'stapps-id-cards',
  templateUrl: 'id-cards.html',
  styleUrls: ['id-cards.scss'],
  providers: [IdCardsProvider],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    IonicModule,
    AsyncPipe,
    ThingTranslateModule,
    UtilModule,
    IdCardComponent,
    TranslateModule,
    TitleCasePipe,
  ],
})
export class IdCardsComponent {
  idCards: Observable<SCIdCard[]> = this.idCardsProvider.getIdCards();

  constructor(readonly idCardsProvider: IdCardsProvider) {}
}
