/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {AuthHelperService} from '../../auth/auth-helper.service';
import {SCAuthorizationProviderType} from '@openstapps/core';
import {ActivatedRoute} from '@angular/router';
import {ScheduleProvider} from '../../calendar/schedule.provider';
import {profilePageSections} from '../../../../config/profile-page-sections';

@Component({
  selector: 'app-home',
  templateUrl: 'profile-page.html',
  styleUrls: ['profile-page.scss'],
})
export class ProfilePageComponent {
  sections = profilePageSections;

  constructor(
    readonly authHelper: AuthHelperService,
    readonly activatedRoute: ActivatedRoute,
    readonly scheduleProvider: ScheduleProvider,
  ) {}

  async signIn(providerType: SCAuthorizationProviderType) {
    const originPath = this.activatedRoute.snapshot.queryParamMap.get('origin_path');
    await (originPath ? this.authHelper.setOriginPath(originPath) : this.authHelper.deleteOriginPath());
    await this.authHelper.getProvider(providerType).signIn();
  }

  async signOut(providerType: SCAuthorizationProviderType) {
    await this.authHelper.getProvider(providerType).signOut();
  }

  ionViewWillEnter() {
    this.authHelper
      .getProvider('default')
      .getValidToken()
      .then(() => void this.authHelper.getProvider('default').loadUserInfo())
      .catch(() => {
        // noop
      });
    this.authHelper
      .getProvider('paia')
      .getValidToken()
      .catch(() => {
        // noop
      });
  }
}
