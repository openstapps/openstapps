/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {NgModule} from '@angular/core';
import {AddEventReviewModalComponent} from './add-event-review-modal.component';
import {Calendar} from '@awesome-cordova-plugins/calendar/ngx';
import {CalendarService} from './calendar.service';
import {ScheduleProvider} from './schedule.provider';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {MomentModule} from 'ngx-moment';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

@NgModule({
  declarations: [AddEventReviewModalComponent],
  imports: [
    IonicModule.forRoot(),
    TranslateModule.forChild(),
    ThingTranslateModule.forChild(),
    IonIconModule,
    FormsModule,
    CommonModule,
    MomentModule,
    UtilModule,
  ],
  exports: [],
  providers: [Calendar, CalendarService, ScheduleProvider],
})
export class CalendarModule {}
