/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCLanguageCode} from '@openstapps/core';
import type {Locale} from 'date-fns';

type LocalesMap = Record<SCLanguageCode, () => Promise<Locale>>;

const LOCALES = {
  en: () => import('date-fns/locale/en-GB').then(({enGB}) => enGB),
  de: () => import('date-fns/locale/de').then(({de}) => de),
} satisfies Partial<LocalesMap>;

/**
 * Get a Date Fns Locale
 */
export async function getDateFnsLocale(code: SCLanguageCode): Promise<Locale> {
  if (code in LOCALES) {
    return LOCALES[code as keyof typeof LOCALES]();
  } else {
    console.warn(`Unknown Locale "${code}" for Date Fns. Falling back to English.`);
    return LOCALES.en();
  }
}
