/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCTranslations} from '@openstapps/core';

export type RegExpString = string;
export type SCSearchFilterConfig = Record<
  RegExpString,
  Record<
    RegExpString,
    {
      name?: string;
      sortOrder: number;
      translations?: SCTranslations<{name: string}>;
    }
  >
>;

export const searchFilters: SCSearchFilterConfig = {
  '.*': {
    'type': {
      sortOrder: 0,
    },
    '[^.]*': {
      sortOrder: 10,
    },
    'academicTerms?\\.acronym': {
      name: 'semester',
      sortOrder: 2,
      translations: {
        de: {
          name: 'Semester',
        },
        en: {
          name: 'Semester',
        },
      },
    },
  },
};
