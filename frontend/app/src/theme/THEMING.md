# Theming

## Customizing the theme

- `colors.scss` contains the brand colors you can customize
- `color-processing.scss` contains some values you can use to adjust how the theme is generated.
  You might need to edit this in if your brand colors result in low contrast.
- `font.scss` contains the font face definitions
- `theme.scss` contains general variables you can customize
