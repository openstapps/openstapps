# GitLab API

Wraps common GitLab API calls in a TypeScript class.

## Installation

```shell
npm install @openstapps/gitlab-api
```

## CLI

Install it globally.

```shell
npm install -g @openstapps/gitlab-api
```

To see the usage of the CLI tool to make calls to the GitLab API from the command line use the following command.

```shell
openstapps-gitlab-api --help
```
