/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {
  deleteUndefinedProperties,
  isNodeEnvironment,
  isProductiveEnvironment,
  isProductiveNodeEnvironment,
} from '../src/index.js';

describe('common', function () {
  it('should should delete undefined properties (1)', function () {
    expect(
      deleteUndefinedProperties({
        a: 2,
        b: {
          c: 3,
          d: undefined,
        },
      }),
    ).to.deep.equal({
      a: 2,
      b: {
        c: 3,
      },
    });
  });

  it('should delete undefined properties (2)', function () {
    expect(
      deleteUndefinedProperties({
        a: undefined,
        b: undefined,
      }),
    ).to.deep.equal({});
  });

  it('should delete undefined properties (3)', function () {
    expect(
      deleteUndefinedProperties({
        a: 2,
        b: 'foo',
        c: 'bar',
      }),
    ).to.deep.equal({
      a: 2,
      b: 'foo',
      c: 'bar',
    });
  });

  it('should detect node environment', function () {
    expect(isNodeEnvironment()).to.be.equal(true);

    const savedProcess = process;

    // @ts-expect-error override this
    process = undefined;
    expect(isNodeEnvironment()).to.be.equal(false);

    process = savedProcess;
  });

  it('should detect production environment', function () {
    const nodeEnvironment = process.env.NODE_ENV;

    process.env.NODE_ENV = '';
    expect(isProductiveEnvironment()).to.be.equal(false);

    process.env.NODE_ENV = 'production';
    expect(isProductiveEnvironment()).to.be.equal(true);

    process.env.NODE_ENV = nodeEnvironment;
  });

  it('should detect node production environment', function () {
    const nodeEnvironment = process.env.NODE_ENV;

    process.env.NODE_ENV = '';
    expect(isProductiveNodeEnvironment()).to.be.equal(false);

    process.env.NODE_ENV = 'production';
    expect(isProductiveNodeEnvironment()).to.be.equal(true);

    process.env.NODE_ENV = nodeEnvironment;
  });
});
