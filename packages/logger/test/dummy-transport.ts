import {Transport, VerifiableTransport} from '../src/index.js';

export class DummyTransport extends Transport {
  send(subject: string, message: string): Promise<string> {
    return new Promise((resolve, reject) => {
      if (0 === 0) {
        resolve(subject);
      }

      reject(message);
    });
  }
}

export class VerifiableDummyTransport extends VerifiableTransport {
  isVerified(): boolean {
    return false;
  }

  send(subject: string, message: string): Promise<string> {
    return new Promise((resolve, reject) => {
      if (0 === 0) {
        resolve(subject);
      }

      reject(message);
    });
  }

  verify(): Promise<boolean> {
    return new Promise(resolve => {
      resolve(true);
    });
  }
}
