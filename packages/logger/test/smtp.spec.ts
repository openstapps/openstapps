/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {SMTP} from '../src/index.js';

const validEmails = [
  'stordeur@campus.tu-berlin.de',
  'foo@bar.com',
  'test@test.cz',
  'info@beispiel.to',
  'stördeur@campus.tu-berlin.de',
  'stordeur@campus.tu-berlin.de+a',
];

const invalidEmails = [
  'stordeurcampus.tu-berlin.de',
  '@campus.tu-berlin.de',
  '',
  '@',
  ' stordeur@campus.tu-berlin.de',
  'stord+eur@campus.tu-berlin.de ',
  'stordeur@campus..tu-berlin.de',
  'stordeur@campus',
];

describe('isValidEmailAddress', function () {
  describe('valid emails', function () {
    for (const email of validEmails) {
      it(`should detect "${email}" as valid`, function () {
        expect(SMTP.isValidEmailAddress(email)).to.be.true;
      });
    }
  });

  describe('invalid emails', function () {
    for (const email of invalidEmails) {
      it(`should detect "${email}" as invalid`, function () {
        expect(SMTP.isValidEmailAddress(email)).to.be.false;
      });
    }
  });
});
