/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {isTransportWithVerification} from '../src/index.js';
import {DummyTransport, VerifiableDummyTransport} from './dummy-transport.js';

describe('transport', function () {
  it('should not have transport with verification', function () {
    expect(isTransportWithVerification(new DummyTransport())).to.be.false;
  });

  it('should have transport with verification', function () {
    expect(isTransportWithVerification(new VerifiableDummyTransport())).to.be.true;
  });
});
