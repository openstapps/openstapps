/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {Colorize} from '../../src/index.js';
import chalk from 'chalk';

chalk.level = 2;

describe('colorize', function () {
  it('should transform', function () {
    const transformation = new Colorize();

    expect(transformation.transform('ERROR', 'Foobar')).to.be.equal(
      '\u001B[1m\u001B[31mFoobar\u001B[39m\u001B[22m',
    );
    expect(transformation.transform('LOG', 'Foobar')).to.be.equal('\u001B[37mFoobar\u001B[39m');
  });
});
