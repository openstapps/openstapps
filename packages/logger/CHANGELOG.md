# @openstapps/logger

## 3.0.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

- 64caebaf: Migrate away from `@krlwlfrt/async-pool`

  ```ts
  import {mapAsyncLimit} from '@openstapps/collection-utils';

  await mapAsyncLimit(
    [1, 2, 3],
    async it => {
      await someNetworkRequest(it);
    },
    5,
  );
  ```

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Migrate away from @openstapps/configuration
- 23481d0d: Update to TypeScript 5.1.6
- 98546a97: Fix an issue with chalk terminal colors in tests

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

- 64caebaf: Migrate away from `@krlwlfrt/async-pool`

  ```ts
  import {mapAsyncLimit} from '@openstapps/collection-utils';

  await mapAsyncLimit(
    [1, 2, 3],
    async it => {
      await someNetworkRequest(it);
    },
    5,
  );
  ```

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Migrate away from @openstapps/configuration
- 98546a97: Fix an issue with chalk terminal colors in tests

## [1.1.1](https://gitlab.com/openstapps/logger/compare/v1.1.0...v1.1.1) (2022-11-08)

### Bug Fixes

- removing transformations in production ([8d6ea04](https://gitlab.com/openstapps/logger/commit/8d6ea040c1e4ce177068e1a9fea5085e748f1110))

## [1.1.0](https://gitlab.com/openstapps/logger/compare/v1.0.1...v1.1.0) (2022-11-01)

### Bug Fixes

- compatibility with log aggregators ([8aef5b8](https://gitlab.com/openstapps/logger/commit/8aef5b8d5b5fe4ed4ff7f17fd679ebdf83381001))

## [1.0.1](https://gitlab.com/openstapps/logger/compare/v1.0.0...v1.0.1) (2022-10-12)

## [1.0.0](https://gitlab.com/openstapps/logger/compare/v0.8.1...v1.0.0) (2022-08-17)

## [0.8.1](https://gitlab.com/openstapps/logger/compare/v0.8.0...v0.8.1) (2022-05-27)

## [0.8.0](https://gitlab.com/openstapps/logger/compare/v0.7.0...v0.8.0) (2021-12-14)

## [0.7.0](https://gitlab.com/openstapps/logger/compare/v0.6.0...v0.7.0) (2021-05-18)

## [0.6.0](https://gitlab.com/openstapps/logger/compare/v0.4.0...v0.6.0) (2021-04-06)

### Bug Fixes

- log level exclusiveness calculation ([100b921](https://gitlab.com/openstapps/logger/commit/100b921488c3b8846678bf72befb787ca51bd1c7))

### Features

- add EXIT log level ([edc6e6f](https://gitlab.com/openstapps/logger/commit/edc6e6fad5ece784059e406dcedd6d76fe07f74b))

## [0.4.0](https://gitlab.com/openstapps/logger/compare/v0.3.1...v0.4.0) (2019-07-12)

### Features

- add transformations ([ddbe00d](https://gitlab.com/openstapps/logger/commit/ddbe00d2a51430348fcf4e4e29807cc43a37cf49)), closes [#9](https://gitlab.com/openstapps/logger/issues/9)

## [0.3.1](https://gitlab.com/openstapps/logger/compare/v0.3.0...v0.3.1) (2019-06-05)

### Bug Fixes

- correct meta information for imports ([d2cb99f](https://gitlab.com/openstapps/logger/commit/d2cb99f70f713029550d8e535abfa65961486006))

## [0.3.0](https://gitlab.com/openstapps/logger/compare/v0.2.1...v0.3.0) (2019-06-04)

## [0.2.1](https://gitlab.com/openstapps/logger/compare/v0.2.0...v0.2.1) (2019-05-27)

## [0.2.0](https://gitlab.com/openstapps/logger/compare/v0.1.0...v0.2.0) (2019-05-27)

### Features

- add automatic publishing ([d9c4ce4](https://gitlab.com/openstapps/logger/commit/d9c4ce48dc980f85d43be47a23b5eda73f9f21b1)), closes [#6](https://gitlab.com/openstapps/logger/issues/6)

## [0.1.0](https://gitlab.com/openstapps/logger/compare/v0.0.5...v0.1.0) (2019-05-07)

## [0.0.5](https://gitlab.com/openstapps/logger/compare/v0.0.4...v0.0.5) (2019-01-16)

### Features

- allow env variables to overwrite smtp config ([3d82c94](https://gitlab.com/openstapps/logger/commit/3d82c94577ad2013124fab64fc13ce24df579d21)), closes [#3](https://gitlab.com/openstapps/logger/issues/3)

## [0.0.4](https://gitlab.com/openstapps/logger/compare/v0.0.3...v0.0.4) (2018-11-28)

## [0.0.3](https://gitlab.com/openstapps/logger/compare/v0.0.2...v0.0.3) (2018-11-28)

## [0.0.2](https://gitlab.com/openstapps/logger/compare/v0.0.1...v0.0.2) (2018-11-28)

## [0.0.1](https://gitlab.com/openstapps/logger/compare/911d71cf3b453dfe302c33d275cb840086288e8e...v0.0.1) (2018-11-28)

### Features

- add logger ([911d71c](https://gitlab.com/openstapps/logger/commit/911d71cf3b453dfe302c33d275cb840086288e8e))
