/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * An abstract wrapper for a transport system like for example SMTP
 */
export abstract class Transport {
  /**
   * Send message with subject
   *
   * A message can be a mail or something completely different depending on what transport is implemented.
   * @param subject Subject of the message
   * @param message Message to send
   */
  abstract send(subject: string, message: string): Promise<string>;
}

/**
 * A transport wrapper of transport which can be verified
 */
export abstract class VerifiableTransport extends Transport {
  /**
   * Checks if the transport was verified at least once
   */
  abstract isVerified(): boolean;

  /**
   * Verifies transport
   *
   * Check connection, authentication, ...
   */
  abstract verify(): Promise<boolean>;
}
