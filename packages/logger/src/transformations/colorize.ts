/*
 * Copyright (C) 2019-2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import chalk from 'chalk';
// eslint-disable-next-line unicorn/import-style
import type {ChalkInstance} from 'chalk';
import {LogLevel} from '../logger.js';
import {Transformation} from '../transformation.js';

/**
 * Transformation that colorizes log output
 */
export class Colorize implements Transformation {
  /**
   * Skip this transformation in production environments
   */
  useInProduction = false;

  /**
   * Instantiate a new colorize transformation
   * @param logLevelToColor Map from log level to color transformation to apply
   */
  constructor(
    // not entirely sure why we can't just use the functions directly here
    private readonly logLevelToColor: {[k in LogLevel]: ChalkInstance} = {
      ERROR: chalk.bold.red,
      INFO: chalk.cyan,
      LOG: chalk.white,
      OK: chalk.bold.green,
      WARN: chalk.yellow,
    },
  ) {
    // noop
  }

  /**
   * Colorize log output
   * @param logLevel Log level to choose color for
   * @param output Output to colorize
   */
  transform(logLevel: LogLevel, output: string): string {
    return this.logLevelToColor[logLevel](output);
  }
}
