/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {LogLevel} from '../logger.js';
import {Transformation} from '../transformation.js';

/**
 * Transformation that adds the log level to output
 */
export class AddLogLevel implements Transformation {
  /**
   * Keep this transformation in production environments
   */
  useInProduction = true;

  /**
   * Add log level to output
   * @param logLevel Log level to add to output
   * @param output Output to colorize
   */
  // tslint:disable-next-line:prefer-function-over-method
  transform(logLevel: LogLevel, output: string): string {
    return `[${logLevel}] ${output}`;
  }
}
