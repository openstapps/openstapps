/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {TypeFlags} from 'typescript';

/**
 * Describes an easy to use type definition.
 */
export interface LightweightType {
  /**
   * Type Flags
   */
  flags: TypeFlags;

  /**
   * Contains all types inside of <> brackets
   */
  genericsTypes?: LightweightType[];

  /**
   * If it is an array(-like) type
   */
  isArray?: true;

  /**
   * If it is a type parameter
   */
  isTypeParameter?: true;

  /**
   * The name of the type that is referenced. Enum members have reference names that lead no where.
   */
  referenceName?: string;

  /**
   * Type specifications, if the type is combined by either an array, union or a typeOperator
   */
  specificationTypes?: LightweightType[];

  /**
   * Value of the type
   *
   * Literal types have their value here, non-literals their type name (for example 'string')
   */
  value?: string | number | boolean;
}
