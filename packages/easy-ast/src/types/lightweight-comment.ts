/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Represents a Comment
 */
export interface LightweightComment {
  /**
   * Description of the comment
   */
  description?: string;

  /**
   * Short summary of the comment
   */
  shortSummary?: string;

  /**
   * Tags of the comment
   */
  tags?: LightweightCommentTag[];
}

/**
 * Lightweight comment tag
 */
export interface LightweightCommentTag {
  /**
   * The name of the tag
   */
  name: string;

  /**
   * The parameters of the tag
   */
  parameters?: string[];
}
