/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {lightweightProjectFromPath} from '../src/index.js';
import {expect} from 'chai';
import {expandPathToFilesSync, toUnixPath} from '../src/util.js';
import type {EasyAstSpecType} from './easy-ast-spec-type.js';

const projectPath = './test/project';

const tests = await Promise.all(
  expandPathToFilesSync(projectPath, file => file.endsWith('ast-test.ts')).map(async it => ({
    path: toUnixPath(it),
    config: await import(`file://${it}`).then(it => it.testConfig as EasyAstSpecType),
  })),
);

describe('Easy AST', function () {
  this.timeout(15_000);
  this.slow(2000);

  it('should build the project', function () {
    const project = lightweightProjectFromPath(projectPath, true);
    expect(Object.keys(project).length).to.equal(tests.length);
  });

  const project = lightweightProjectFromPath(projectPath, true);

  for (const {path, config} of tests) {
    it(config.testName, function () {
      const projectAtPath = project[path];
      expect(projectAtPath).not.to.be.undefined;
      for (const key in projectAtPath) {
        if (key.startsWith('$')) delete projectAtPath[key];
      }

      expect(projectAtPath).to.be.deep.equal(config.expected);
    });
  }
});
