# What is Easy AST?

Easy [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree) is a "wrapper" around the TypeScript Compiler API. The TS Compiler API is built for many
much more complex things than just accessing type declarations, and thus comes with a ton of
bloat that makes it really difficult to work with.

This is why we built the Easy AST, which converts the TS representation to a really easy and
lightweight structure, that removes a lot of the stuff you wouldn't really need.
