# @openstapps/api

Node.js library to interact with the StApps backend service

## Use this as a standalone program

See `@openstapps/api-cli`
