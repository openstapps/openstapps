/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import chai from 'chai';
import {expect} from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiSpies from 'chai-spies';
import {ApiError} from '../src/index.js';

chai.should();
chai.use(chaiSpies);
chai.use(chaiAsPromised);

const sandbox = chai.spy.sandbox();

describe('Errors', function () {
  afterEach(function () {
    sandbox.restore();
  });

  it('should add additional data', function () {
    const error = new ApiError({
      additionalData: 'Lorem ipsum',
    });

    expect(error.toString()).to.contain('Lorem ipsum');
  });

  it('should add remote stack-trace', async function () {
    const error = new ApiError({
      stack: 'Lorem ipsum',
    });

    expect(error.toString()).to.contain('Lorem ipsum');
  });

  it('should set name', async function () {
    const error = new ApiError({
      name: 'Foo',
    });

    expect(error.name).to.be.equal('Foo');
  });
});
