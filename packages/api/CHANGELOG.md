# @openstapps/api

## 3.3.2

### Patch Changes

- @openstapps/core@3.3.2

## 3.3.0

### Minor Changes

- 688bc5f2: v3.3.0 changes

### Patch Changes

- Updated dependencies [688bc5f2]
  - @openstapps/core@3.3.0

## 3.2.0

### Patch Changes

- Updated dependencies [912ae422]
  - @openstapps/core@3.2.0

## 3.1.1

### Patch Changes

- Fix version history offered by backend
- Updated dependencies
  - @openstapps/core@3.1.1

## 3.1.0

### Patch Changes

- Updated dependencies [06b8ca10]
  - @openstapps/core@3.1.0

## 3.0.0

### Major Changes

- 64caebaf: Split API into API, API-CLI and API-Plugin

  Plugins are now required to use `api-plugin`.
  Consumers of `api` can benefit from a slimmer package with
  no NodeJS dependencies.

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

- 64caebaf: Migrate HttpClient to Node 18's native `fetch` API

  HttpClient can now be used both in NodeJS and browsers.

- 64caebaf: Migrate away from `@krlwlfrt/async-pool`

  ```ts
  import {mapAsyncLimit} from '@openstapps/collection-utils';

  await mapAsyncLimit(
    [1, 2, 3],
    async it => {
      await someNetworkRequest(it);
    },
    5,
  );
  ```

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Migrate away from @openstapps/configuration
- 23481d0d: Update to TypeScript 5.1.6
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [1f62b5c5]
- Updated dependencies [98546a97]
- Updated dependencies [23481d0d]
- Updated dependencies [0a7e6af1]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
  - @openstapps/core@3.0.0

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6
- Updated dependencies [23481d0d]
  - @openstapps/core@3.0.0-next.4

## 3.0.0-next.0

### Major Changes

- 64caebaf: Split API into API, API-CLI and API-Plugin

  Plugins are now required to use `api-plugin`.
  Consumers of `api` can benefit from a slimmer package with
  no NodeJS dependencies.

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

- 64caebaf: Migrate HttpClient to Node 18's native `fetch` API

  HttpClient can now be used both in NodeJS and browsers.

- 64caebaf: Migrate away from `@krlwlfrt/async-pool`

  ```ts
  import {mapAsyncLimit} from '@openstapps/collection-utils';

  await mapAsyncLimit(
    [1, 2, 3],
    async it => {
      await someNetworkRequest(it);
    },
    5,
  );
  ```

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Migrate away from @openstapps/configuration
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [98546a97]
- Updated dependencies [0a7e6af1]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
- Updated dependencies [64caebaf]
  - @openstapps/core@3.0.0-next.0

## [1.1.0](https://gitlab.com/openstapps/api/compare/v1.0.1...v1.1.0) (2023-05-19)

## [1.0.1](https://gitlab.com/openstapps/api/compare/v1.0.0...v1.0.1) (2023-05-08)

### Bug Fixes

- changelog generation on version tags ([a5291af](https://gitlab.com/openstapps/api/commit/a5291af83831d542a60d8df119dc10fc44233ae6))
- rename deprecated Gitlab CI variables ([10f7cb8](https://gitlab.com/openstapps/api/commit/10f7cb802883b51fcee52b05d2751cbac27130cd))

## [1.0.0](https://gitlab.com/openstapps/api/compare/v0.46.0...v1.0.0) (2023-05-04)

## [0.46.0](https://gitlab.com/openstapps/api/compare/v0.45.0...v0.46.0) (2023-05-04)

## [0.45.0](https://gitlab.com/openstapps/api/compare/v0.44.0...v0.45.0) (2022-12-06)

## [0.44.0](https://gitlab.com/openstapps/api/compare/v0.43.0...v0.44.0) (2022-10-18)

## [0.43.0](https://gitlab.com/openstapps/api/compare/v0.42.0...v0.43.0) (2022-08-23)

## [0.42.0](https://gitlab.com/openstapps/api/compare/v0.41.1...v0.42.0) (2022-07-04)

## [0.41.1](https://gitlab.com/openstapps/api/compare/v0.41.0...v0.41.1) (2022-05-27)

## [0.41.0](https://gitlab.com/openstapps/api/compare/v0.40.0...v0.41.0) (2022-05-17)

## [0.40.0](https://gitlab.com/openstapps/api/compare/v0.39.0...v0.40.0) (2022-04-04)

## [0.39.0](https://gitlab.com/openstapps/api/compare/v0.38.0...v0.39.0) (2022-03-17)

## [0.38.0](https://gitlab.com/openstapps/api/compare/v0.37.0...v0.38.0) (2022-01-25)

### Features

- add client method for invoking plugin routes ([83120a6](https://gitlab.com/openstapps/api/commit/83120a67345d2fee672f6a44ab91708ecce65e58))

## [0.37.0](https://gitlab.com/openstapps/api/compare/v0.36.0...v0.37.0) (2021-12-17)

## [0.36.0](https://gitlab.com/openstapps/api/compare/v0.35.0...v0.36.0) (2021-12-15)

## [0.35.0](https://gitlab.com/openstapps/api/compare/v0.34.0...v0.35.0) (2021-10-20)

## [0.34.0](https://gitlab.com/openstapps/api/compare/v0.33.0...v0.34.0) (2021-10-04)

## [0.33.0](https://gitlab.com/openstapps/api/compare/v0.32.0...v0.33.0) (2021-09-16)

## [0.32.0](https://gitlab.com/openstapps/api/compare/v0.31.1...v0.32.0) (2021-09-01)

## [0.31.1](https://gitlab.com/openstapps/api/compare/v0.31.0...v0.31.1) (2021-08-27)

### Bug Fixes

- plugin related tests ([51c157d](https://gitlab.com/openstapps/api/commit/51c157d577b9ae70e8c4683e653cd65aacb4f248))

## [0.31.0](https://gitlab.com/openstapps/api/compare/v0.30.0...v0.31.0) (2021-08-17)

## [0.30.0](https://gitlab.com/openstapps/api/compare/v0.29.0...v0.30.0) (2021-08-04)

## [0.29.0](https://gitlab.com/openstapps/api/compare/v0.28.0...v0.29.0) (2021-06-08)

## [0.28.0](https://gitlab.com/openstapps/api/compare/v0.27.0...v0.28.0) (2021-04-27)

## [0.27.0](https://gitlab.com/openstapps/api/compare/v0.26.0...v0.27.0) (2021-03-08)

### Bug Fixes

- send valid JSON on bulk/done ([29cd22f](https://gitlab.com/openstapps/api/commit/29cd22f2d11cebbe2de63cc2e614734e286821f2)), closes [#38](https://gitlab.com/openstapps/api/issues/38)

## [0.26.0](https://gitlab.com/openstapps/api/compare/v0.25.0...v0.26.0) (2021-02-23)

## [0.25.0](https://gitlab.com/openstapps/api/compare/v0.23.0...v0.25.0) (2020-11-09)

## [0.23.0](https://gitlab.com/openstapps/api/compare/v0.22.0...v0.23.0) (2020-10-23)

### Bug Fixes

- make express parse body ([b3e1075](https://gitlab.com/openstapps/api/commit/b3e107528b5d6ac5ec86786a9f9bf8fb1645e874))

## [0.22.0](https://gitlab.com/openstapps/api/compare/v0.21.0...v0.22.0) (2020-05-15)

### Features

- add content-type header ([d30de89](https://gitlab.com/openstapps/api/commit/d30de896f0d1a21c039e1905c0766cc37df06b23))

## [0.21.0](https://gitlab.com/openstapps/api/compare/v0.19.0...v0.21.0) (2020-05-06)

## [0.19.0](https://gitlab.com/openstapps/api/compare/v0.18.0...v0.19.0) (2020-03-11)

### Bug Fixes

- repair and unify subcommand help outputs ([4ae968f](https://gitlab.com/openstapps/api/commit/4ae968ff0f52c940cb11a97876f72d57feb61597))

### Features

- add option to wait on resource to e2e script ([e18858f](https://gitlab.com/openstapps/api/commit/e18858fc58ccfb8b3bdec92260eba12cd1a25214))

## [0.18.0](https://gitlab.com/openstapps/api/compare/v0.17.0...v0.18.0) (2020-02-04)

### Features

- extend e2e procedure ([dc79dc8](https://gitlab.com/openstapps/api/commit/dc79dc8feb10a29cff0f6d1c320658ad13cf7e4e))

## [0.17.0](https://gitlab.com/openstapps/api/compare/v0.16.0...v0.17.0) (2019-11-15)

## [0.16.0](https://gitlab.com/openstapps/api/compare/v0.15.0...v0.16.0) (2019-09-03)

### Features

- add plugin class ([c2848fc](https://gitlab.com/openstapps/api/commit/c2848fc7a528288ef4c3ab96728d4e16add6fe75)), closes [#12](https://gitlab.com/openstapps/api/issues/12)

## [0.15.0](https://gitlab.com/openstapps/api/compare/v0.14.0...v0.15.0) (2019-08-19)

## [0.14.0](https://gitlab.com/openstapps/api/compare/v0.13.0...v0.14.0) (2019-07-23)

## [0.13.0](https://gitlab.com/openstapps/api/compare/v0.12.0...v0.13.0) (2019-07-16)

### Bug Fixes

- change removeReferences() to also remove origin ([ded221c](https://gitlab.com/openstapps/api/commit/ded221c175368d315fdbb79d561c86c3ee8bf158))

## [0.12.0](https://gitlab.com/openstapps/api/compare/v0.11.0...v0.12.0) (2019-07-15)

### Features

- add function to remove undefined properties ([f95fd09](https://gitlab.com/openstapps/api/commit/f95fd09112e81eab42591a5aaff3568ab06e9441))

## [0.11.0](https://gitlab.com/openstapps/api/compare/v0.10.0...v0.11.0) (2019-07-12)

## [0.10.0](https://gitlab.com/openstapps/api/compare/v0.9.0...v0.10.0) (2019-07-03)

## [0.9.0](https://gitlab.com/openstapps/api/compare/v0.8.0...v0.9.0) (2019-06-20)

## [0.8.0](https://gitlab.com/openstapps/api/compare/v0.7.0...v0.8.0) (2019-05-14)

### Features

- add e2e command. Indexes core test files. ([e1313b5](https://gitlab.com/openstapps/api/commit/e1313b55ff3bc17e96b09639f4949b2179991679))

## [0.7.0](https://gitlab.com/openstapps/api/compare/v0.6.0...v0.7.0) (2019-04-16)

## [0.6.0](https://gitlab.com/openstapps/api/compare/v0.5.0...v0.6.0) (2019-04-09)

## [0.5.0](https://gitlab.com/openstapps/api/compare/v0.4.1...v0.5.0) (2019-04-04)

### Features

- add method to remove references from a thing ([9cf6fde](https://gitlab.com/openstapps/api/commit/9cf6fde050c665b63026b8c08502c7836e23c2d5)), closes [#6](https://gitlab.com/openstapps/api/issues/6)

## [0.4.1](https://gitlab.com/openstapps/api/compare/v0.4.0...v0.4.1) (2019-03-15)

### Bug Fixes

- adjust compile npm script to exact configuration guideline ([7f8c9ed](https://gitlab.com/openstapps/api/commit/7f8c9eda2fdcef0831af23806b846baa0d236a1c))

## [0.4.0](https://gitlab.com/openstapps/api/compare/v0.3.0...v0.4.0) (2019-03-15)

### Bug Fixes

- apply changes suggested by @openstapps/configuration ([782fdbe](https://gitlab.com/openstapps/api/commit/782fdbebba3599323a98244fcc28e7605ed95cc2))

## [0.3.0](https://gitlab.com/openstapps/api/compare/v0.2.0...v0.3.0) (2019-02-21)

## [0.2.0](https://gitlab.com/openstapps/api/compare/v0.1.1...v0.2.0) (2019-02-18)

## [0.1.1](https://gitlab.com/openstapps/api/compare/v0.1.0...v0.1.1) (2019-02-07)

## [0.1.0](https://gitlab.com/openstapps/api/compare/v0.0.3...v0.1.0) (2019-01-28)

## [0.0.3](https://gitlab.com/openstapps/api/compare/v0.0.2...v0.0.3) (2019-01-21)

## [0.0.2](https://gitlab.com/openstapps/api/compare/v0.0.1...v0.0.2) (2018-12-14)

### Bug Fixes

- add type and batchSize to required parameters ([8541b00](https://gitlab.com/openstapps/api/commit/8541b00dbbf4c683fddaf3d003cf13fabfbc979b)), closes [#1](https://gitlab.com/openstapps/api/issues/1)
- remove trailing slash from url if needed ([eb7c334](https://gitlab.com/openstapps/api/commit/eb7c334bb8878cffd96cd9d52b5b99e2338e3a65))

## [0.0.1](https://gitlab.com/openstapps/api/compare/4839f941c617681c78ef9959f3df86013459c332...v0.0.1) (2018-11-29)

### Features

- add api ([4839f94](https://gitlab.com/openstapps/api/commit/4839f941c617681c78ef9959f3df86013459c332))
