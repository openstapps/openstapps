/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Filters only defined elements
 */
export function rejectNil<T>(array: Array<T | undefined | null>): T[] {
  // eslint-disable-next-line unicorn/no-null
  return array.filter(it => it != null) as T[];
}

/**
 * Map elements that are not null
 */
export function mapNotNil<T, S>(array: readonly T[], transform: (element: T) => S | undefined | null): S[] {
  return rejectNil(array.map(transform));
}

/**
 * Deletes all properties with the value 'undefined', [] or {}
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export function cleanupEmpty<T extends object>(object: T): T {
  const out = {} as T;
  for (const key in object) {
    const value = object[key];
    // eslint-disable-next-line unicorn/no-null
    if (value != null && (typeof value !== 'object' || Object.values(value).length > 0)) {
      out[key] = value;
    }
  }
  return out;
}
