/*
 * Copyright (C) 2018-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type SCRoute = any;

export interface RouteInstanceWithMeta extends SCRoute {
  /**
   * Possible errors on a route
   */
  errors: SCErrorResponse[];

  /**
   * Description of the request body
   */
  requestBodyDescription: string;

  /**
   * Description of the response body
   */
  responseBodyDescription: string;
}

/**
 * A route instance with its relevant meta information
 */
export interface RouteWithMetaInformation {
  /**
   * Description of the route
   */
  description: {
    /**
     * Short text of the description - title
     */
    shortText?: string;
    /**
     * Text of the description
     */
    text?: string;
  };
  /**
   * Name of the route
   */
  name: string;

  /**
   * Instance of the route
   */
  route: RouteInstanceWithMeta;

  /**
   * Possible tags/keywords the route can be associated with
   */
  tags?: [string];
}

/**
 * A node with its relevant meta information
 */
export interface NodeWithMetaInformation {
  /**
   * Module the node belongs to
   */
  module: string;

  /**
   * Type of the node
   */
  type: string;
}

/**
 * A generic error that can be returned by the backend if somethings fails during the processing of a request
 */
export interface SCErrorResponse extends Error {
  /**
   * Additional data that describes the error
   */
  additionalData?: unknown;

  /**
   * HTTP status code to return this error with
   */
  statusCode: number;
}
