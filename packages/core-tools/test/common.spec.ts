/* eslint-disable unicorn/prefer-module */
/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {getTsconfigPath} from '../src/common.js';
import path from 'path';
import {fileURLToPath} from 'url';

process.on('unhandledRejection', (reason: unknown): void => {
  if (reason instanceof Error) {
    void Logger.error('UNHANDLED REJECTION', reason.stack);
  }
  process.exit(1);
});

describe('common', function () {
  describe('getTsconfigPath', function () {
    it('should get tsconfig path', function () {
      expect(getTsconfigPath(path.dirname(fileURLToPath(import.meta.url)))).to.be.equal(process.cwd());
    });
  });
});
