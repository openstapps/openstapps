/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * User configuration keys mapped to string type while including their requiredness
 */
export type SCUserConfigurationMap = {[K in keyof SCUserConfigurationOptional]?: string} & {
  [K in keyof SCUserConfigurationRequired]: string;
};

/**
 * A user configuration
 */
export type SCUserConfiguration = SCUserConfigurationRequired & SCUserConfigurationOptional;

/**
 * A user configurations required properties
 */
interface SCUserConfigurationRequired {
  /**
   * ID given to the user
   */
  id: string;
  /**
   * The complete name of the user combining all the parts of the name into one
   */
  name: string;
}

/**
 * A user configurations optional properties
 */
interface SCUserConfigurationOptional {
  /**
   * User's e-mail
   */
  email?: string;
  /**
   * User's family name
   */
  familyName?: string;
  /**
   * User's given name
   */
  givenName?: string;
  /**
   * Role assigned to the user
   */
  role?: string;
  /**
   * Student ID given to the user
   */
  studentId?: string;
}
