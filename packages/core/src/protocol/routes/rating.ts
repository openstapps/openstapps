/*
 * Copyright (C) 2019-2023 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCInternalServerErrorResponse} from '../errors/internal-server-error.js';
import {SCMethodNotAllowedErrorResponse} from '../errors/method-not-allowed.js';
import {SCRequestBodyTooLargeErrorResponse} from '../errors/request-body-too-large.js';
import {SCSyntaxErrorResponse} from '../errors/syntax-error.js';
import {SCUnsupportedMediaTypeErrorResponse} from '../errors/unsupported-media-type.js';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../route.js';
import {SCThing} from '../../things/abstract/thing.js';
import {SCUserGroupSetting} from '../../things/setting.js';
import {SCValidationErrorResponse} from '../errors/validation.js';

/**
 * User rating from the app
 * Plugin needs to define its own rating request to hit the target rating system.
 * That request should extend this one and contain timestamp and other needed data.
 * @validatable
 */
export interface SCRatingRequest {
  /**
   * Number of rating stars
   */
  rating: 1 | 2 | 3 | 4 | 5;

  /**
   * User's group in the app
   */
  userGroup: SCUserGroupSetting['value'];

  /**
   * UID of the thing that is rated
   */
  uid: SCThing['uid'];
}

/**
 * A response to a rating request
 * @validatable
 */
export interface SCRatingResponse {}

/**
 * Route for rating submission
 */
export class SCRatingRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCRatingRequest';
    this.responseBodyName = 'SCRatingResponse';
    this.statusCodeSuccess = StatusCodes.OK;
    this.urlPath = '/rating';
  }
}
