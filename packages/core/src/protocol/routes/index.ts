/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCAppConfiguration} from '../../config/app.js';
import {SCAuthorizationProvider, SCAuthorizationProviderType} from '../../config/authorization.js';
import {SCBackendConfiguration} from '../../config/backend.js';
import {SCInternalServerErrorResponse} from '../errors/internal-server-error.js';
import {SCMethodNotAllowedErrorResponse} from '../errors/method-not-allowed.js';
import {SCRequestBodyTooLargeErrorResponse} from '../errors/request-body-too-large.js';
import {SCSyntaxErrorResponse} from '../errors/syntax-error.js';
import {SCUnsupportedMediaTypeErrorResponse} from '../errors/unsupported-media-type.js';
import {SCValidationErrorResponse} from '../errors/validation.js';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../route.js';

/**
 * Index request
 * @validatable
 */
export interface SCIndexRequest {}

/**
 * A response to an index request
 * @validatable
 */
export interface SCIndexResponse {
  /**
   * @see SCAppConfiguration
   */
  app: SCAppConfiguration;

  /**
   * @see SCAuthorizationProvider
   */
  auth: {[key in SCAuthorizationProviderType]?: SCAuthorizationProvider};

  /**
   * @see SCBackendConfiguration
   */
  backend: SCBackendConfiguration;
}

/**
 * Route to request meta information about the deployment
 */
export class SCIndexRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCIndexRequest';
    this.responseBodyName = 'SCIndexResponse';
    this.statusCodeSuccess = StatusCodes.OK;
    this.urlPath = '/';
  }
}
