/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n.js';
import {
  SCThing,
  SCThingMeta,
  SCThingTranslatableProperties,
  SCThingType,
  SCThingWithoutReferences,
} from './abstract/thing.js';
import {SCISO8601DateRange} from './abstract/range.js';

/**
 * An ID-Card without references
 */
export interface SCIdCardWithoutReferences extends SCThingWithoutReferences {
  /**
   * Validity range
   */
  validity?: SCISO8601DateRange;

  /**
   * Type
   */
  type: SCThingType.IdCard;
}

/**
 * A message
 * @validatable
 * @indexable
 */
export interface SCIdCard extends SCIdCardWithoutReferences, SCThing {
  /**
   * Translated fields of a message
   */
  translations?: SCTranslations<SCIdCardTranslatableProperties>;

  /**
   * Type
   */
  type: SCThingType.IdCard;
}

/**
 * Translatable properties of a message
 */
export interface SCIdCardTranslatableProperties extends SCThingTranslatableProperties {}

/**
 * Meta information about messages
 */
export class SCIdCardMeta extends SCThingMeta implements SCMetaTranslations<SCIdCard> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      validity: 'Gültigkeit',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      validity: 'validity',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      type: 'Ausweis',
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
      type: SCThingType.Message,
    },
  };
}
