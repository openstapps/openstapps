import {SCInPlace} from './abstract/place.js';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories.js';
import {SCThingMeta, SCThingType} from './abstract/thing.js';
import {SCOrganizationWithoutReferences} from './organization.js';

/**
 * categories of a job posting
 */
export type SCJobCategories =
  | 'law'
  | 'business'
  | 'natural'
  | 'it'
  | 'education'
  | 'other'
  | 'arts'
  | 'social'
  | 'engineering'
  | 'communication'
  | 'medical';

export interface SCJobPostingWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCJobCategories, SCThingWithCategoriesSpecificValues>,
    SCInPlace {
  /**
   * Type of a job posting
   */
  type: SCThingType.JobPosting;
}

/**
 * A JobPosting
 * @validatable
 * @indexable
 */
export interface SCJobPosting
  extends SCThingWithCategories<SCJobCategories, SCThingWithCategoriesSpecificValues>,
    SCJobPostingWithoutReferences {
  /**
   * A description of the employer
   * @text
   */
  employerOverview?: SCOrganizationWithoutReferences;

  /**
   * A JobPosting
   */
  type: SCThingType.JobPosting;
}

export class SCJobPostingMeta extends SCThingMeta {
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta().fieldTranslations.de,
      employerOverview: 'Arbeitgeberübersicht',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta().fieldTranslations.en,
      employerOverview: 'employer overview',
    },
  };

  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta().fieldValueTranslations.de,
      type: 'Job Angebot',
      categories: {
        law: 'Recht',
        business: 'Business',
        natural: 'Ökologisch',
        it: 'IT',
        education: 'Bildung',
        other: 'Andere',
        arts: 'Künste',
        social: 'Sozial',
        engineering: 'Ingenieurswesen',
        communication: 'Kommunikation',
        medical: 'Medizin',
      },
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta().fieldValueTranslations.en,
      type: SCThingType.JobPosting,
      categories: {
        law: 'law',
        business: 'business',
        natural: 'natural',
        it: 'it',
        education: 'education',
        other: 'other',
        arts: 'arts',
        social: 'social',
        engineering: 'engineering',
        communication: 'communication',
        medical: 'medical',
      },
    },
  };
}
