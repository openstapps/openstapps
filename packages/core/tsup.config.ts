import {defineConfig} from 'tsup';
import packageJson from './package.json' assert {type: 'json'};

export default defineConfig({
  entry: ['src/index.ts'],
  sourcemap: true,
  clean: true,
  format: 'esm',
  outDir: 'lib',
  env: {
    CORE_VERSION: packageJson.version,
  },
});
