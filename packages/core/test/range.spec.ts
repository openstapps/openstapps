import {expect} from 'chai';
import {formatRange, isInRange, SCRange} from '../src/index.js';

const cases: Record<'accept' | 'reject', [number, SCRange<number>][]> = {
  accept: [
    [4, {gt: 3, lt: 5}],
    [4, {gte: 4, lte: 4}],
    [3, {gt: 2, lt: 4}],
    [5, {gte: 3, lte: 5}],
    [10, {gt: 5, lt: 15}],
    [0, {gte: 0, lte: 10}],
  ],
  reject: [
    [4, {gt: 3, lt: 4}],
    [4, {gte: 5, lte: 6}],
    [2, {gt: 5, lt: 10}],
    [6, {gte: 7, lte: 8}],
    [-1, {gt: 0, lt: 5}],
    [20, {gte: 10, lte: 15}],
  ],
};

describe('Range', () => {
  for (const constructor of ['Number', 'Date'] as const) {
    describe(`${constructor} range`, () => {
      for (const [accept, [value, range]] of Object.entries(cases).flatMap(([accept, cases]) =>
        cases.map(it => [accept, it] as const),
      )) {
        it(`should ${accept} ${value} in the range ${formatRange(range)}`, () => {
          const result = isInRange(constructor === 'Number' ? value : new Date(value), range);
          if (accept === 'accept') {
            expect(result).to.be.true;
          } else {
            expect(result).to.be.false;
          }
        });
      }
    });
  }
});
