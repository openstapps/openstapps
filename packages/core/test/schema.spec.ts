import {validateFiles, writeReport} from '@openstapps/core-tools';
import {expect} from 'chai';
import {mkdir} from 'fs/promises';
import path from 'path';

describe('Schema', function () {
  this.timeout(15_000);
  this.slow(10_000);

  it('should validate against test files', async function () {
    const errorsPerFile = await validateFiles(
      path.resolve('lib', 'schema'),
      path.resolve('test', 'resources'),
    );

    await mkdir('report', {recursive: true});
    await writeReport(path.join('report', 'index.html'), errorsPerFile);

    for (const file of Object.keys(errorsPerFile)) {
      for (const error of errorsPerFile[file]) {
        expect(error.expected).to.be.true;
      }
    }
  });
});
