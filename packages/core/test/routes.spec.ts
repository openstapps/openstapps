/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {SCBulkRoute} from '../src/index.js';
import {SCBulkAddRoute} from '../src/index.js';
import {SCThingUpdateRoute} from '../src/index.js';

describe('Routes', function () {
  this.timeout(10_000);
  this.slow(5000);

  it('should produce correct BulkAddRoute url path', function () {
    expect(
      new SCBulkAddRoute().getUrlPath({
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      }),
    ).to.equal('/bulk/540862f3-ea30-5b8f-8678-56b4dc217140');
  });

  it('should produce correct BlukRoute url path', function () {
    expect(new SCBulkRoute().getUrlPath()).to.equal('/bulk');
  });

  it('should produce correct ThingUpdateRoute url path', function () {
    expect(
      new SCThingUpdateRoute().getUrlPath({
        TYPE: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      }),
    ).to.equal('/dish/540862f3-ea30-5b8f-8678-56b4dc217140');
  });

  it('should throw an error if too many parameters are provided', function () {
    expect(() =>
      new SCThingUpdateRoute().getUrlPath({
        FOO: 'bar',
        TYPE: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      }),
    ).to.throw('Extraneous parameters provided.');
  });

  it('should throw an error if wrong parameters are provided', function () {
    expect(() =>
      new SCThingUpdateRoute().getUrlPath({
        TYPO: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      }),
    ).to.throw("Parameter 'TYPE' not provided.");
  });
});
