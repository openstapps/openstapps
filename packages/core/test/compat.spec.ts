/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {lightweightProjectFromPath, LightweightProject} from '@openstapps/easy-ast';
import {expect} from 'chai';

process.on('unhandledRejection', error => {
  throw error;
});

describe('Mapping Compatibility', () => {
  let project: LightweightProject;

  before(function () {
    this.timeout(15_000);
    this.slow(10_000);

    project = lightweightProjectFromPath('src');
  });

  it('non-exported definitions should not have duplicate names across files', () => {
    const names = new Set<string>();

    for (const file in project) {
      for (const definition in project[file]) {
        expect(names).not.to.include(definition);
        names.add(definition);
      }
    }
  });
});
