import {SCSearchResponse} from '../../src/index.js';
import {dishWithTranslation} from './dish-with-translation.js';

export const dishWithTranslationSearchResponse: SCSearchResponse = {
  data: [dishWithTranslation],
  facets: [
    {
      buckets: [
        {
          count: 1,
          key: 'key',
        },
      ],
      field: 'field',
    },
  ],
  pagination: {
    count: 1,
    offset: 0,
    total: 1,
  },
  stats: {
    time: 1,
  },
};
