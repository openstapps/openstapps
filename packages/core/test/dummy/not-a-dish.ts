import {SCThingOriginType, SCDish} from '../../src/index.js';

export const notADish: Omit<SCDish, 'type'> & {type: 'foobar'} = {
  categories: ['appetizer'],
  name: 'foo',
  origin: {
    created: '',
    type: SCThingOriginType.User,
  },
  type: 'foobar',
  uid: 'bar',
};
