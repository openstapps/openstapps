import {SCDish, SCThingOriginType, SCThingType} from '../../src/index.js';

export const dishWithTranslation: SCDish = {
  categories: ['appetizer'],
  name: 'foo',
  origin: {
    created: '',
    type: SCThingOriginType.User,
  },
  translations: {
    de: {
      name: 'Foo',
    },
  },
  type: SCThingType.Dish,
  uid: 'bar',
};
