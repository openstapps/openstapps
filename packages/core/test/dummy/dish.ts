import {SCDish, SCThingOriginType, SCThingType} from '../../src/index.js';
import {building} from './building.js';

export const dish: SCDish = {
  categories: ['main dish', 'dessert'],
  characteristics: [{name: 'base-characteristic0'}, {name: 'base-characteristic1'}],
  name: 'base-dish-name',
  offers: [
    {
      availability: 'in stock',
      inPlace: building,
      prices: {
        default: 23.42,
      },
      provider: {
        name: 'base-provider',
        type: SCThingType.Organization,
        uid: '540862f3-ea30-5b8f-8678-56b4dc217141',
      },
    },
  ],
  origin: {
    indexed: '1970-01-01T00:00:00.000Z',
    name: 'dish-connector',
    type: SCThingOriginType.Remote,
  },
  translations: {
    de: {
      characteristics: [{name: 'de-characteristic0'}, {name: 'de-characteristic1'}],
      name: 'de-dish-name',
    },
  },
  type: SCThingType.Dish,
  uid: '540862f3-ea30-5b8f-8678-56b4dc217140',
};
