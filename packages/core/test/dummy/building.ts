import {SCBuildingWithoutReferences, SCThingType} from '../../src/index.js';

export const building: SCBuildingWithoutReferences = {
  address: {
    addressCountry: 'base-address.addressCountry',
    addressLocality: 'base-address.addressLocality',
    postalCode: 'base-address.postalCode',
    streetAddress: 'base-address.streetAddress',
  },
  categories: ['office', 'education'],
  floors: ['base-floor0', 'base-floor1'],
  geo: {
    point: {
      coordinates: [12, 13],
      type: 'Point',
    },
  },
  name: 'base-space-name',
  translations: {
    de: {
      address: {
        addressCountry: 'de-address.addressCountry',
        addressLocality: 'de-address.addressLocality',
        postalCode: 'de-address.postalCode',
        streetAddress: 'de-address.streetAddress',
      },
      floors: ['de-floor0', 'de-floor1'],
      name: 'de-space-name',
    },
  },
  type: SCThingType.Building,
  uid: '540862f3-ea30-5b8f-8678-56b4dc217140',
};
