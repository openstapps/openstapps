import {SCSetting, SCSettingInputType, SCThingOriginType, SCThingType} from '../../src/index.js';

export const setting: SCSetting = {
  categories: ['profile'],
  defaultValue: 'student',
  description: 'base-description',
  inputType: SCSettingInputType.SingleChoice,
  name: 'group',
  order: 1,
  origin: {
    indexed: '2018-11-11T14:30:00Z',
    name: 'Dummy',
    type: SCThingOriginType.Remote,
  },
  type: SCThingType.Setting,
  uid: '2c97aa36-4aa2-43de-bc5d-a2b2cb3a530e',
  values: ['student', 'employee', true, 42],
};
