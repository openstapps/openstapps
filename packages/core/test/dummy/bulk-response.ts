import {SCBulkResponse, SCThingType} from '../../src/index.js';

export const bulkResponse: SCBulkResponse = {
  expiration: '2009-06-30T18:30:00+02:00 ',
  source: 'bar',
  state: 'done',
  type: SCThingType.Dish,
  uid: 'foo',
};
