/* eslint-disable unicorn/no-null */
/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {
  isBulkResponse,
  isMultiSearchResponse,
  isSearchResponse,
  isThing,
  isThingWithTranslations,
} from '../src/guards.js';
import {bulkResponse} from './dummy/bulk-response.js';
import {dishWithTranslation} from './dummy/dish-with-translation.js';
import {dishWithTranslationSearchResponse} from './dummy/dish-with-translation-search-response.js';
import {notADish} from './dummy/not-a-dish.js';
import {SCMultiSearchResponse} from '../src/index.js';

describe('Guards', function () {
  this.timeout(10_000);
  this.slow(5000);

  describe('isBulkResponse', function () {
    it(`should not accept nullish values`, function () {
      expect(isBulkResponse(null)).to.be.false;
    });

    it('should not accept a dish', function () {
      expect(isBulkResponse(dishWithTranslation)).to.be.false;
    });

    it('should accept a bulk', function () {
      expect(isBulkResponse(bulkResponse)).to.be.true;
    });
  });

  describe('isMultiSearchResponse', function () {
    const multiSearchResponse: SCMultiSearchResponse = {
      foo: dishWithTranslationSearchResponse,
    };

    it('should accept a multi search response', function () {
      expect(isMultiSearchResponse(multiSearchResponse)).to.be.true;
    });

    it('should not accept a multi search response with invalid search requests', function () {
      const notAMultiSearchResponse = {...multiSearchResponse, bar: 'baz'};
      expect(isMultiSearchResponse(notAMultiSearchResponse)).to.be.false;
    });

    it('should not accept empty responses', function () {
      expect(isMultiSearchResponse({})).to.be.false;
    });
  });

  describe('isSearchResponse', function () {
    it('should accept a search response', function () {
      expect(isSearchResponse(dishWithTranslationSearchResponse)).to.be.true;
    });

    it('should not accept nullish values', function () {
      expect(isSearchResponse(null)).to.be.false;
    });

    it('should not accept a response without pagination', function () {
      const response = {...dishWithTranslationSearchResponse};
      // @ts-expect-error this is on purpose of course
      delete response.pagination;
      expect(isSearchResponse(response)).to.be.false;
    });

    it('should not accept a response without data', function () {
      const response = {...dishWithTranslationSearchResponse};
      // @ts-expect-error this is on purpose of course
      delete response.data;
      expect(isSearchResponse(response)).to.be.false;
    });
  });

  describe('isThing', function () {
    it('should not accept strings', function () {
      expect(isThing('foo')).to.be.false;
    });

    it('should not accept objects with arbitrary type values', function () {
      expect(isThing({type: 'foo'})).to.be.false;
    });

    it('should not accept things with missing props', function () {
      expect(isThing(notADish)).to.be.false;
    });

    it('should accept valid things', function () {
      expect(isThing(dishWithTranslation)).to.be.true;
    });
  });

  describe('isThingWithTranslations', function () {
    it('should not accept things without translations', function () {
      const dishWithoutTranslation = {...dishWithTranslation};
      delete dishWithoutTranslation.translations;
      expect(isThingWithTranslations(dishWithoutTranslation)).to.be.false;
    });

    it('should accept things with translations', function () {
      expect(isThingWithTranslations(dishWithTranslation)).to.be.true;
    });
  });
});
