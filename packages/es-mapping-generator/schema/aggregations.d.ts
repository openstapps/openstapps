/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
declare module 'aggregations.json' {
  const value: AggregationSchema;
  export default value;
}

/**
 * An elasticsearch bucket aggregation
 * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/search-aggregations-bucket.html
 */
export interface AggregationSchema {
  [aggregationName: string]: ESTermsFilter | ESNestedAggregation;
}

/**
 * An elasticsearch terms filter
 */
export interface ESTermsFilter {
  /**
   * Terms filter definition
   */
  terms: {
    /**
     * Field to apply filter to
     */
    field: string;

    /**
     * Number of results
     */
    size?: number;
  };
}

/**
 * Filter that filters by name of the the field type
 */
export interface ESAggTypeFilter {
  /**
   * The type of the object to find
   */
  term: {
    /**
     * The name of the type
     */
    type: string;
  };
}

/**
 * Filter that matches everything
 */
export interface ESAggMatchAllFilter {
  /**
   * Filter that matches everything
   */
  match_all: object;
}

/**
 * For nested aggregations
 */
export interface ESNestedAggregation {
  /**
   * Possible nested Aggregations
   */
  aggs: AggregationSchema;
  /**
   * Possible filter for types
   */
  filter: ESAggTypeFilter | ESAggMatchAllFilter;
}
