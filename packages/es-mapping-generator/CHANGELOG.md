# @openstapps/es-mapping-generator

## 3.0.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 0a7e6af1: Remove put-es-templates

  The `put-es-templates` functionality has been removed.

- 0a7e6af1: Migrate es mapping types from es-mapping-generator to .d.ts next to generated mappings
- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Remove @openstapps/logger dependency
- 98546a97: Migrate away from @openstapps/configuration
- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.4

### Patch Changes

- 23481d0d: Update to TypeScript 5.1.6

## 3.0.0-next.0

### Major Changes

- 64caebaf: Move project to a turbo monorepo & pnpm

  Internal dependencies are now defined using `"@openstapps/package": "workspace:*"`

  - Removed extraneous files from packages
    - `.npmrc`
    - `.npmignore`
    - `.mailmap`
    - `.gitignore`
    - `CONTRIBUTING.md`
    - `LICENSE` (Project license file is added upon publishing, see [pnpm.io](https://pnpm.io/cli/publish))
    - `package-lock.json`
    - `.editorconfig`
    - `.eslintrc.json` (moved eslint config to `package.json`)
    - `.eslintignore`
    - `.gitlab-ci.yml` (Most workflows are workspace-level)
    - `.gitlab/**` (issue templates etc. are now workspace-level)
    - `.dockerignore` (Docker files are determined by which files are deployed with `pnpm deploy`, as per `package.json/files`)
  - TSConfig has been moved to its own package (You can now use `"extends": "@openstapps/tsconfig"`)
  - Removed ESLint and Prettier peer dependency hell by injecting them through the `.pnpmfile.cjs`
  - Added syncpack for keeping dependency versions in sync (and consistent key ordering in `package.json`)
  - Replaced conventional changelog with changesets
  - Apps with binaries now use a top level `app.js`

  ```js
  #!/usr/bin/env node
  import './lib/app.js';
  ```

- 64caebaf: Migrate to ESM

  CommonJS is no longer supported in any capacity. To use the new
  version, you will need to migrate your package to ESM.
  We recommend using `tsup` and `Node 18`.

  ```json
  {
    "type": "module"
  }
  ```

- 64caebaf: Migrate package to Node 18

  - Consumers of this package will need to migrate to Node 18 or
    higher.
  - Packages have been migrated from promisified `readFile` or
    `readFileSync` towards `fs/promises`
  - Packages use native `flatMap` now

- 0a7e6af1: Remove put-es-templates

  The `put-es-templates` functionality has been removed.

- 0a7e6af1: Migrate es mapping types from es-mapping-generator to .d.ts next to generated mappings
- 64caebaf: Migrate build system to `tsup`

  All packages now use an `index.ts` file to expose contents.

  You will need to migrate paths from `import foo from '@scope/package/lib/foo` to `import foo from '@scope/package'`

### Minor Changes

- 64caebaf: Migrate tests to C8/Chai/Mocha

  - `@testdeck` OOP testing has been removed.
  - Tests have been unified
  - CommonJS module mocking has been replaced through
    refactoring of tests, as ES Modules cannot be mocked
    (do yourself a favor and don't try to mock them)
  - C8 now replaces NYC as a native coverage tool

### Patch Changes

- 64caebaf: Migrated changelogs to changeset format

  ```js
  import fs from 'fs';

  const path = 'packages/logger/CHANGELOG.md';

  fs.writeFileSync(path, fs.readFileSync(path, 'utf8').replace(/^#+\s+\[/gm, '## ['));
  ```

- 98546a97: Remove @openstapps/logger dependency
- 98546a97: Migrate away from @openstapps/configuration

## [0.6.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.5.0...v0.6.0) (2023-04-28)

## [0.5.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.4.0...v0.5.0) (2023-04-27)

## [0.4.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.3.0...v0.4.0) (2023-01-12)

## [0.3.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.2.0...v0.3.0) (2022-08-17)

## [0.2.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.1.0...v0.2.0) (2022-06-27)

### Bug Fixes

- marked version with audit issue ([7534744](https://gitlab.com/openstapps/es-mapping-generator/commit/7534744294ec27f1ce3f4bf65424d717b33dd889))

## [0.1.0](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.0.4...v0.1.0) (2022-05-27)

## [0.0.4](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.0.3...v0.0.4) (2021-12-16)

### Bug Fixes

- filter tags override inherited type tags ([f0401e1](https://gitlab.com/openstapps/es-mapping-generator/commit/f0401e1889f12860d4eaa2dc5a1ee1b08f20ba31))

## [0.0.3](https://gitlab.com/openstapps/es-mapping-generator/compare/v0.0.2...v0.0.3) (2021-08-05)

### Bug Fixes

- put es template parsing ([9a88129](https://gitlab.com/openstapps/es-mapping-generator/commit/9a881299dc9d0a6d59b370340880a10bd8b1e2c5))

## [0.0.2](https://gitlab.com/openstapps/es-mapping-generator/compare/28334b800c014a9e0b6e980b5365af6fdc1d8950...v0.0.2) (2021-08-05)

### Bug Fixes

- fix unwanted crash when not supplying an error path ([28334b8](https://gitlab.com/openstapps/es-mapping-generator/commit/28334b800c014a9e0b6e980b5365af6fdc1d8950))
