/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MappingDynamicTemplate, MappingProperty} from '@elastic/elasticsearch/lib/api/types';

export interface MapAggTestOptions {
  testName: string;
  name: string;
  agg?: {
    fields?: string[];
    globals?: string[];
  };
  map?: MinimalMappingDescription;
  err?: string[];
  ignoredTags?: string[];
}

export interface MinimalMappingDescription {
  maps?: {
    [name: string]: MappingProperty;
  };
  dynamicTemplates?: Record<string, MappingDynamicTemplate>[];
}
