/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

export type FilterableEnumType = 'a' | 'b' | 'c';

/**
 * @indexable
 */
export interface FilterableTag {
  /**
   * @text
   * @filterable
   */
  foo: string;

  /**
   * @keyword
   * @filterable
   */
  bar: string;

  /**
   * @filterable
   */
  baz: 'some literal';

  /**
   * @filterable
   */
  buz: FilterableEnumType;

  type: ThingType.FilterableTag;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Filterable tag should add raw field to strings',
  name: ThingType.FilterableTag,
  map: {
    maps: {
      foo: {
        type: 'text',
        fields: {
          raw: {
            type: 'keyword',
          },
        },
      },
      bar: {
        type: 'keyword',
        fields: {
          raw: {
            type: 'keyword',
          },
        },
      },
      baz: {
        type: 'keyword',
        fields: {
          raw: {
            type: 'keyword',
          },
        },
      },
      buz: {
        type: 'keyword',
        fields: {
          raw: {
            type: 'keyword',
          },
        },
      },
    },
  },
};
