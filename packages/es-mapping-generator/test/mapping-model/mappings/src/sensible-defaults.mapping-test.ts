/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface SensibleDefaults {
  numberDefault: number;
  stringDefault: string;
  booleanDefault: boolean;
  stringLiteralDefault: 'Hey there!';
  booleanTrueLiteralDefault: true;
  booleanFalseLiteralDefault: false;

  type: ThingType.SensibleDefaultType;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Primitive types should have sensible defaults',
  name: ThingType.SensibleDefaultType,
  map: {
    maps: {
      numberDefault: {
        type: 'integer',
      },
      stringDefault: {
        type: 'text',
      },
      booleanDefault: {
        type: 'boolean',
      },
      stringLiteralDefault: {
        type: 'keyword',
      },
      booleanTrueLiteralDefault: {
        type: 'boolean',
      },
      booleanFalseLiteralDefault: {
        type: 'boolean',
      },
    },
  },
};
