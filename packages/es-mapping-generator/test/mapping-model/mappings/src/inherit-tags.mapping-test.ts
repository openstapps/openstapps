/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MapAggTestOptions} from '../../map-agg-test-options';
import {ThingType} from './types';

/**
 * @indexable
 */
export interface InheritTags {
  /**
   * @inheritTags inherit tags::bar.baz
   */
  foo: number;

  bar: {
    /**
     * @float
     */
    baz: number;
  };

  type: ThingType.InheritTags;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Inherit tags tag should work',
  name: ThingType.InheritTags,
  map: {
    maps: {
      foo: {
        type: 'float',
      },
      bar: {
        dynamic: 'strict',
        properties: {
          baz: {
            type: 'float',
          },
        },
      },
    },
  },
};
