/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface MapExplicitTypes {
  /**
   * @integer
   */
  esInteger: number;

  /**
   * @float
   */
  esFloat: number;

  /**
   * @keyword
   */
  esKeyword: string;

  /**
   * @text
   */
  esText: string;

  /**
   * @date
   */
  esEpochMsDate: number;

  /**
   * @date
   */
  esStringDate: string;

  type: ThingType.MapExplicitTypes;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Explicit type annotations should work',
  name: ThingType.MapExplicitTypes,
  map: {
    maps: {
      esInteger: {
        type: 'integer',
      },
      esFloat: {
        type: 'float',
      },
      esKeyword: {
        type: 'keyword',
      },
      esText: {
        type: 'text',
      },
      esEpochMsDate: {
        type: 'date',
      },
      esStringDate: {
        type: 'date',
      },
    },
  },
};
