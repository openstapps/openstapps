/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface TypeQuery {
  foo: typeof Bar;

  type: ThingType.TypeQuery;
}

enum Bar {
  'a',
  'b',
  'c',
}

// https://gitlab.com/openstapps/core-tools/-/issues/47
export const testConfig: MapAggTestOptions = {
  testName: 'Type queries should work',
  name: ThingType.TypeQuery,
  map: {
    maps: {
      foo: {
        type: 'text',
      },
    },
  },
};
