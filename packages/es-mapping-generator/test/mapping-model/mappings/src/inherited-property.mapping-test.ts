/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface InheritedProperty extends Bar {
  foo: number;

  type: ThingType.InheritedProperty;
}

interface Bar {
  /**
   * @keyword
   */
  bar: string;

  /**
   * @float
   */
  baz: number;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Inherited properties should inherit tags',
  name: ThingType.InheritedProperty,
  map: {
    maps: {
      foo: {
        type: 'integer',
      },
      bar: {
        type: 'keyword',
      },
      baz: {
        type: 'float',
      },
    },
  },
};
