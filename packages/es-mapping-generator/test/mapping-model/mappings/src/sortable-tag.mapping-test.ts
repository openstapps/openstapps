/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface SortableTag {
  /**
   * @sortable
   */
  foo: string;

  /**
   * @sortable ducet
   */
  bar: string;

  /**
   * @sortable
   */
  baz: number;

  type: ThingType.SortableTag;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Sortable tag should work',
  name: ThingType.SortableTag,
  map: {
    maps: {
      foo: {
        type: 'text',
        fields: {
          sort: {
            country: 'DE',
            language: 'de',
            type: 'icu_collation_keyword',
            variant: '@collation=phonebook',
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
          } as any,
        },
      },
      bar: {
        type: 'text',
        fields: {
          sort: {
            country: 'DE',
            language: 'de',
            type: 'icu_collation_keyword',
            variant: '@collation=phonebook',
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
          } as any,
        },
      },
      baz: {
        type: 'integer',
        fields: {
          sort: {
            country: 'DE',
            language: 'de',
            type: 'icu_collation_keyword',
            variant: '@collation=phonebook',
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
          } as any,
        },
      },
    },
  },
};
