/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MapAggTest} from './mapping-model/map-agg-test';
import {MapAggTestOptions} from './mapping-model/map-agg-test-options';
import {expandPathToFilesSync} from './common';

describe('ES Aggregation Gen', async () => {
  const magAppInstance = new MapAggTest('aggregations');

  for (const file of expandPathToFilesSync('./test/mapping-model/aggregations/', file =>
    file.endsWith('agg-test.ts'),
  )) {
    // eslint-disable-next-line unicorn/no-await-expression-member
    const test = (await import(file))['testConfig'] as MapAggTestOptions;

    it(test.testName, function () {
      magAppInstance.testInterfaceAgainstPath(test);
    });
  }
})
  .timeout(20_000)
  .slow(10_000);
