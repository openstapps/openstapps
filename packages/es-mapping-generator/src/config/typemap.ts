/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MappingFloatNumberProperty} from '@elastic/elasticsearch/lib/api/types';
import type {ElasticsearchTypemap} from '../../schema/mappings';

export const PARSE_ERROR = 'PARSE_ERROR' as MappingFloatNumberProperty['type'];
export const MISSING_PREMAP = 'MISSING_PREMAP' as MappingFloatNumberProperty['type'];
export const TYPE_CONFLICT = 'TYPE_CONFLICT' as MappingFloatNumberProperty['type'];

export const typemap: ElasticsearchTypemap = {
  boolean: {
    default: 'boolean',
  },
  false: {
    default: 'boolean',
  },
  number: {
    default: 'integer',
    float: 'float',
    integer: 'integer',
    date: 'date',
  },
  string: {
    default: 'text',
    keyword: 'keyword',
    text: 'text',
    date: 'date',
  },
  stringLiteral: {
    default: 'keyword',
  },
  true: {
    default: 'boolean',
  },
};

/**
 * If the string is a tag type
 */
export function isTagType(string_: string): boolean {
  for (const key in typemap) {
    if (typemap.hasOwnProperty(key) && typemap[key][string_] !== undefined) {
      return true;
    }
  }

  return false;
}

export const dynamicTypes = ['any', 'unknown'];
