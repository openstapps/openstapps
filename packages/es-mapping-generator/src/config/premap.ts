/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MappingProperty} from '@elastic/elasticsearch/lib/api/types';

export const premaps: Record<string, MappingProperty> = {
  'CoordinateReferenceSystem': {
    dynamic: true,
    properties: {
      type: {
        type: 'keyword',
      },
    },
  },
  'LineString': {
    type: 'geo_shape',
  },
  'Point': {
    type: 'geo_point',
  },
  'Polygon': {
    type: 'geo_shape',
  },
  'SCISO8601DateRange': {
    type: 'date_range',
  },
  'jsonpatch.OpPatch': {
    dynamic: 'strict',
    properties: {
      from: {
        type: 'keyword',
      },
      op: {
        type: 'keyword',
      },
      path: {
        type: 'keyword',
      },
      value: {
        // this is actually an 'any' type; however, ES does not really support that.
        type: 'keyword',
      },
    },
  },
};
