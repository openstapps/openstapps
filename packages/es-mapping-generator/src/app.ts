/*
 * Copyright (C) 2018-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Command} from 'commander';
import {copyFileSync, mkdirSync, readFileSync, writeFileSync} from 'fs';
import path from 'path';
import {generateTemplate} from './mapping';
import {getProjectReflection} from './project-reflection';

// handle unhandled promise rejections
process.on('unhandledRejection', async (reason: unknown) => {
  if (reason instanceof Error) {
    await console.error(reason.message);
    console.info(reason.stack);
  }
  process.exit(1);
});

const commander = new Command('openstapps-core-tools');

// eslint-disable-next-line unicorn/prefer-module
commander.version(JSON.parse(readFileSync(path.resolve(__dirname, '..', 'package.json')).toString()).version);

commander
  .command('mapping <relativeSrcPath>')
  .option('-m, --mappingPath <relativeMappingPath>', 'Mapping Path')
  .option('-i, --ignoredTags <ignoredTags>', 'Ignored Tags (comma-separated)')
  .option('-a, --aggPath <relativeAggregationPath>', 'Aggregations Path')
  .option('-e, --errorPath <relativeErrorPath>', 'Error Path')
  .action(async (relativeSourcePath, options) => {
    // get absolute paths
    const sourcePath = path.resolve(relativeSourcePath);

    let ignoredTagsList: string[] = [];
    if (typeof options.ignoredTags === 'string') {
      ignoredTagsList = options.ignoredTags.split(',');
    }

    // get project reflection
    const projectReflection = getProjectReflection(sourcePath);

    const result = generateTemplate(projectReflection, ignoredTagsList, true);
    if (result.errors.length > 0) {
      await console.error('Mapping generated with errors!');
    } else {
      console.log('Mapping generated without errors!');
    }

    // write documentation to file
    if (options.aggPath !== undefined) {
      const aggPath = path.resolve(options.aggPath);
      mkdirSync(path.dirname(aggPath), {recursive: true});
      // tslint:disable-next-line:no-magic-numbers
      writeFileSync(aggPath, JSON.stringify(result.aggregations, null, 2));
      copyFileSync(
        // eslint-disable-next-line unicorn/prefer-module
        require.resolve('../schema/aggregations.d.ts'),
        path.join(path.dirname(aggPath), 'aggregations.json.d.ts'),
      );
      console.log(`Elasticsearch aggregations written to ${aggPath}.`);
    }
    if (options.mappingPath !== undefined) {
      const mappingPath = path.resolve(options.mappingPath);
      mkdirSync(path.dirname(mappingPath), {recursive: true});
      writeFileSync(mappingPath, JSON.stringify(result.mappings, null, 2));
      copyFileSync(
        // eslint-disable-next-line unicorn/prefer-module
        require.resolve('../schema/mappings.d.ts'),
        path.join(path.dirname(mappingPath), 'mappings.json.d.ts'),
      );
      console.log(`Elasticsearch mappings written to ${mappingPath}.`);
    }
    if (options.errorPath !== undefined) {
      const errorPath = path.resolve(options.errorPath);
      mkdirSync(path.dirname(errorPath), {recursive: true});
      // tslint:disable-next-line:no-magic-numbers
      writeFileSync(errorPath, JSON.stringify(result.errors, null, 2));
      console.log(`Mapping errors written to ${errorPath}.`);
    } else if (result.errors.length > 0) {
      for (const error of result.errors) {
        await console.error(error);
      }

      throw new Error('Mapping generation failed');
    }
  });

commander.parse(process.argv);
