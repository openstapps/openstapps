/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {pick} from '../src/index.js';
import {expect} from 'chai';

describe('pick', function () {
  it('should pick properties', function () {
    const object = {a: 1, b: 2, c: 3};
    const result = pick(object, ['a', 'c']);
    expect(result).to.deep.equal({a: 1, c: 3});
  });
});
