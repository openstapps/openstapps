/**
 * Copyright (C) 2023 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Runs async tasks in parallel, with a limit
 *
 * Note: JavaScript is not multithreaded.
 * This will not let you run tasks in parallel, use it only to limit how many network requests should be
 * running at once.
 * @param items the items to iterate through
 * @param task the task to be run
 * @param limit the maximum number of tasks that should be run asynchronously
 */
export async function mapAsyncLimit<T, U = void>(
  items: T[],
  task: (item: T, index: number) => Promise<U>,
  limit = 5,
): Promise<U[]> {
  return Promise.all(
    Array.from({length: limit}).map(async () => {
      let i = 0;
      const results: U[] = [];
      for (let item = items.shift(); item !== undefined; item = items.shift()) {
        results.push(await task(item, i));
        i++;
      }
      return results;
    }),
  ).then(it => it.flat());
}
