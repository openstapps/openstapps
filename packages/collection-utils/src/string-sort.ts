/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * sort function for two strings
 */
export function stringSort(a = '', b = ''): number {
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
}

/**
 * sort function for two strings that allows for a custom transform
 */
export function stringSortBy<T>(map: (item: T) => string | undefined): (a: T, b: T) => number {
  return (a: T, b: T): number => {
    const aValue = map(a) || '';
    const bValue = map(b) || '';
    if (aValue < bValue) return -1;
    if (aValue > bValue) return 1;
    return 0;
  };
}
