/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Gets a value from a nested object.
 * The path must be key names separated by dots.
 * If the path doesn't exist, undefined is returned.
 */
export function get<U = unknown>(object: object, path: string): U {
  return path.split('.').reduce(
    (accumulator, current) =>
      accumulator?.hasOwnProperty(current)
        ? // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (accumulator as any)[current]
        : undefined,
    object,
  ) as unknown as U;
}
