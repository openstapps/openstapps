/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Group an array by a function
 */
export function groupBy<T>(collection: T[], group: (item: T) => string | undefined): Record<string, T[]> {
  return collection.reduce((accumulator: Record<string, T[]>, item) => {
    const key = group(item) ?? '';
    accumulator[key] = accumulator[key] ?? [];
    accumulator[key].push(item);
    return accumulator;
  }, {});
}

/**
 * Group an array by a function (returns a Map, whose keys keep order info of items entry)
 */
export function groupByStable<T>(collection: T[], group: (item: T) => string | undefined): Map<string, T[]> {
  return collection.reduce((accumulator: Map<string, T[]>, item) => {
    const key = group(item) ?? '';
    accumulator.set(key, accumulator.get(key) ?? []);
    accumulator.get(key)?.push(item);
    return accumulator;
  }, new Map<string, T[]>());
}

/**
 *
 */
export function groupByProperty<T extends object>(collection: T[], property: keyof T): Record<string, T[]> {
  return groupBy(collection, item => item[property] as unknown as string);
}
