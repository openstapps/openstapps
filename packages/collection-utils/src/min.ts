/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Returns the minimum value of a collection.
 */
export function minBy<T>(array: T[], transform: (item: T) => number | undefined): T {
  const transforms = array.map(transform);
  const min = Math.min(...(transforms.filter(it => !!it) as number[]));
  return array.find((_, i) => transforms[i] === min) as T;
}
