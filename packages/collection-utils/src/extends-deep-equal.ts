/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Checks if an item 'b' extends an item 'a' deeply. That means
 *
 * 1. Every property that is in 'a' must be in 'b'
 * 2. 'b' may have additional properties anywhere
 * 3. arrays may be in a different order
 * 4. a property with the name '*' in 'a' is treated as a template
 *    for another property in 'b', meaning a property in 'b' must match
 *    the property '*' in 'a' according to the matching rules
 * 4. Properties in both 'a' and 'b' must be equal or follow the above rules
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function extendsDeepEqual(a: any, b: any): boolean {
  if (typeof a !== typeof b) return false;

  if (Array.isArray(a)) {
    if (!Array.isArray(b)) return false;
    // this has *horrible* runtime complexity so keep an eye out...
    return a.every(element => b.find(it => extendsDeepEqual(element, it)));
  } else if (typeof a === 'object') {
    for (const key in a) {
      if (key === '*') {
        for (const otherKey in b) {
          if (extendsDeepEqual(a[key], b[otherKey])) return true;
        }
        return false;
      } else {
        if (!extendsDeepEqual(a[key], b[key])) return false;
      }
    }
    return true;
  } else {
    return a === b;
  }
}
