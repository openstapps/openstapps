/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Sum an an array
 */
export function sumBy<T extends object>(
  collection: T[],
  transform: (value: T) => number | undefined,
): number {
  return collection.reduce((accumulator, item) => accumulator + (transform(item) || 0), 0);
}

/**
 * Sum an array of numbers
 */
export function sum(collection: Array<number | undefined>): number {
  return collection.reduce<number>((accumulator, item) => accumulator + (item || 0), 0);
}
