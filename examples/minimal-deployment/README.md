# Minimal deployment
This is an example for how a deployment for StApps can look like. The project also shows why we decided to use Docker
for the deployment on all universities. With help of Docker's virtualization you don't have to install and configure
anything beyond `docker` and `docker compose` itself.

A complete StApps deployment consists of following projects:
```
|*****|        |*******|        |*********|        |**********|
| app | <----> | proxy | <----> | backend | <----> | database |
|*****|        |*******|        |*********|        |**********|
                                     ^
                                     |
                               |************|
                               | connectors |
                               |************|
```

This project shows a very fast way to deploy [backend](https://gitlab.com/openstapps/backend), 
[database](https://gitlab.com/openstapps/database) and [api](https://gitlab.com/openstapps/api) (to copy some data from
an existing deployment of the TU Berlin).


With a running backend and database you should be able to run the [app](https://gitlab.com/openstapps/app)
with your deployment.

# Step by step to your own deployment
At first you need to install [docker](https://docs.docker.com/install/) and 
[docker-compose](https://docs.docker.com/compose/install/). Then clone or download this repository. To download the 
docker images from GitLab you have to be authenticated against GitLabs container registry. Please execute
`docker login registry.gitlab.com`. The requested credentials are the same as for GitLab. 
`docker login` stores the credentials the users home directory. If you plan to execute docker commands only from root
or via sudo you should run `docker login` with root or sudo as well.


On Linux machines you should be able to proceed on [Execution on Linux](#execution-on-linux) otherwise follow me step 
by step.


Be sure to execute all the following commands in the repository and with root privileges or access to the Docker daemon.

## Start the database
Our current database is Elasticsearch and most configuration is contained in the Docker image. Since Docker is not a
full virtualization it uses the hosts kernel. That's why on some systems you may need to increase your virtual memory to
get Elasticsearch to work.
([documentation for increasing your virtual memory](https://www.elastic.co/guide/en/elasticsearch/reference/5.6/vm-max-map-count.html))


To start the database
```sh
# let the elasticsearch user and group be the owner of the folder
chown 100:101 database

# start the database container
docker-compose up -d database

# have a look at the logs of the database
docker-compose logs database
```

As stated in the `docker-compose.yml` the database will expose port `9200` to other services in the same deployment
(all services in the file). 

## Start the backend
The backend is providing an interface to search and bulk index data into the database. Simply run:

```sh
# start the backend container
docker-compose up -d backend

# have a look at the logs of the backend
docker-compose logs backend
```


The backend will find the database on `http://database:9200` inside the deployment network. As stated in the
`docker-compose.yml` the backend will expose port 3000 on the host. You should be able to request the backend for it's
configuration on http://localhost:3000 (or http://MY-IP-ADDRESS:3000).


Now you have a running backend that can be queried by the app. You could already try to the install the
[app](https://gitlab.com/openstapps/app) and use it with your local deployment. That is pretty boring, because the
backend doesn't provide any data by now.

There is no public deployment for the app version 2.0.0 by now. But you could copy some data from there.

To import some `SCPlace`'s you could run:
```shell
docker-compose run --rm api copy --appVersion 2.0.0 place <url-of-public-deployment> http://backend:3000 20
```
This will copy data using the [api](https://gitlab.com/openstapps/api) from the deployment of the TU Berlin to your
deployment.

# Execution on Linux
On Linux you can execute a simple shell script that will run you through 
steps above and copy some data in your deployment:

```shell
sh start.sh
```

# Where to go from here
## Writing your own connector
You now have your own deployment and can import some data from your university into the backend. To write your own
program to import data you should checkout the [minimal-connector](https://gitlab.com/openstapps/minimal-connector/)
This is a small program that shows the usage of the [api](https://gitlab.com/openstapps/api/) and imports some example
data from TypeScript code. Feel free to fork the [minimal-connector](https://gitlab.com/openstapps/minimal-connector/)
and write your own connector with it.

## Add your connector
You may want to integrate your connectors in the `docker-compose.yml` to
deploy your whole StApps-backend-environment in a single command. Use the 
`systemd` directory in this project as an example of how to deploy
your connector(s) and configure `systemd` services and timers.

The service will try to restart a named container. That means for successful 
execution the container needs to be started once before with the appropriate 
command. 
```shell
docker-compose up minimal-connector
```

To enable the service that runs the connector periodically execute:
```shell
systemctl --now enable /absolute/path/minimal-deployment/minimal-connector/minimal-connector.service /absolute/path/minimal-deployment/minimal-connector/minimal-connector.timer
```
This command will immediately start the service on execution.

## Round off your deployment with the [proxy](https://gitlab.com/openstapps/proxy)
The backend is exposed on the port 3000 now. This means anyone can import data into your backend and you can only run
one version of the backend at a time. Have a look at the [proxy](https://gitlab.com/openstapps/proxy/) to secure and
round off your deployment. The proxy will add a layer of security, allowing you to run multiple deployments for
different app version on one server and provide some static data like images for your university.


## Explore docker capabilities
Docker is a great tool with many great features. Have a look at the 
[docker cli documentation](https://docs.docker.com/engine/reference/commandline/cli/) or our 
[docker cheat sheet](https://gitlab.com/openstapps/projectmanagement/blob/master/project-docs/DOCKER_CHEAT_SHEET.md) to learn
more about it.
