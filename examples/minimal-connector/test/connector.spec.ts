/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingOriginType} from '@openstapps/core';
import {expect} from 'chai';
import {MinimalConnector} from '../src/minimal-connector.js';

describe('connector', function () {
  let connector: MinimalConnector;

  beforeEach(function () {
    connector = new MinimalConnector('f-u', 'minimal-connector');
  });

  it('should create remote origin', function () {
    const remoteOrigin = connector.createRemoteOrigin();
    expect(remoteOrigin.name).to.equal(connector.origin);
    expect(remoteOrigin.type).to.equal(SCThingOriginType.Remote);
    expect(Date.now()).to.be.at.least(Date.parse(remoteOrigin.indexed).valueOf());
  });

  it('should automatically generate missing UIDs', async function () {
    const uuidRegExp = new RegExp(
      '^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
    );
    const messages = await connector.getItems();
    for (const message of messages) {
      expect(message.uid).to.match(uuidRegExp);
    }
  });
});
