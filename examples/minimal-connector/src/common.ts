/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ConnectorClient, HttpClient} from '@openstapps/api';
import {SCLicensePlate, SCNamespaces, SCThings} from '@openstapps/core';
import {Connector} from './connector.js';

/**
 * Checks if the input is a valid SCNamespace
 * @param input Name of the potential SCNamespace
 */
export function isValidSCNamespace(input: string): input is SCLicensePlate {
  return Object.keys(SCNamespaces).indexOf(input) > 0;
}

/**
 * Creates a uuid from a JSON stringified item identifier
 *
 * You may create custom itemIdentifier-Interfaces to generate UIDs consistently
 * @param itemIdentifier Identifying representation of the item
 * @param licensePlate License plate of the school
 */
export function createUUID(itemIdentifier: unknown, licensePlate: SCLicensePlate): string {
  return ConnectorClient.makeUUID(JSON.stringify(itemIdentifier), licensePlate);
}

/**
 * Fetches items specified by the connector and pushs them to the backend,
 * by overwriting the bulk indexed with the `origin`.
 * @param backend URL of the StApps backend eployment
 * @param connector Connector to be executed
 */
export async function executeConnector<T extends SCThings>(backend: string, connector: Connector<T>) {
  const items: T[] = await connector.getItems();
  const client: ConnectorClient = new ConnectorClient(new HttpClient(), backend);
  // this might throw an error!
  await client.index<T>(items, connector.origin);
}
