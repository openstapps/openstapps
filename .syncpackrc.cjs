// @ts-check

/** @type {import('syncpack').RcFile} */
const config = {
  semverGroups: [
    {
      range: '',
      dependencies: ['**'],
      packages: ['**'],
    }
  ],
  source: ['package.json', '**/package.json'],
  indent: '  ',
  sortFirst: [
    'name',
    'description',
    'version',
    'private',
    'type',
    'license',
    'repository',
    'author',
    'contributors',
    'keywords',
    'main',
    'types',
    'bin',
    'files',
    'engines',
    'scripts',
    'dependencies',
    'devDependencies',
    'peerDependencies',
    'pnpm',
    'tsup',
    'prettier',
    'eslintConfig',
    'eslintIgnore',
    'nyc',
  ],
  versionGroups: [
    {
      label: 'Package version is controlled by changesets',
      dependencies: ['**'],
      dependencyTypes: ['workspace'],
      packages: ['**'],
      isIgnored: true,
    },
    {
      label: 'ES Mapping Generator Special Dependencies',
      dependencies: ['typescript', 'typedoc', 'ts-node', '@types/node', 'got'],
      packages: ['@openstapps/es-mapping-generator'],
      isIgnored: true,
    },
    {
      label: 'Packages should use workspace version',
      dependencies: ['@openstapps/**'],
      dependencyTypes: ['prod', 'dev', 'peer'],
      packages: ['**'],
      pinVersion: 'workspace:*',
    },
  ],
};

module.exports = config;
